package game;

import javax.naming.CannotProceedException;

public class Game {
    public static void main(String[] args) {
        BackPack backPack = new BackPack(15);
        Coin coin = new Coin();

        try {
            backPack.addItem(coin);
        } catch (CannotProceedException e) {
            System.out.println(e.getMessage());
        }

        Item arrow=()->5;

        Item sword = new Item(){
            @Override
            public int getWeight(){
                return 7;
            }
        };

        try {
            backPack.addItem(arrow);
        } catch (CannotProceedException e) {
            e.printStackTrace();
        }
        try {
            backPack.addItem(sword);
        } catch (CannotProceedException e) {
            e.printStackTrace();
        }



    }
}
