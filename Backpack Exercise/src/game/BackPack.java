package game;

import javax.naming.CannotProceedException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class BackPack {

    private List<Item> items;
    private int maxWeight;
    private int weight = 0;

    public BackPack(int maxWeight) {
        this.items = new ArrayList<>();
        this.maxWeight = maxWeight;
    }


    public void addItem(Item item) throws CannotProceedException {

        if (canAddItem(item)) {
            items.add(item);
        }
        else {
            throw new CannotProceedException("Cannot add item");
        }
    }

    private boolean canAddItem(Item item){
        int totalItemsWeight = items.stream().map(Item::getWeight).reduce(0,Integer::sum);
        return totalItemsWeight+item.getWeight()<=maxWeight;
    }



    public void removeItem(Item item) {
        items.remove(item);
    }

}
