package patterns.builder;

public enum TransmissionType {
    AUTOMATIC, MANUAL
}
