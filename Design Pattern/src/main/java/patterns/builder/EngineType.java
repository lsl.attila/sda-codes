package patterns.builder;

public enum EngineType {
    DIESEL, PETROL, ELECTRIC, HYBRID, PLUG_IN_HYBRID
}
