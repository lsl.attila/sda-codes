package patterns.builder;

import java.util.Optional;

public class Engine {
    private final EngineType engineType;
    private final int engineDisplacement;
    private final boolean isTurbo;
    private final int numberOfCylinders;
    private final int horsePower;
    private final int torque;
    private final TransmissionType transmissionType;
    private final CarType carType;

    public Engine(EngineBuilder engineBuilder) {
//        if(engineBuilder.engineType==null){
//            throw new IllegalArgumentException();
//        }
        engineType = Optional.ofNullable(engineBuilder.engineType)
                .orElseThrow(() -> new IllegalArgumentException("Engine type cannot be null"));
        engineDisplacement=Optional.ofNullable(engineBuilder.engineDisplacement)
                .orElseThrow(() -> new IllegalArgumentException("Engine displacement cannot be null"));
        numberOfCylinders=Optional.ofNullable(engineBuilder.numberOfCylinders)
                .orElse(4);
        horsePower=Optional.ofNullable(engineBuilder.horsePower)
                .orElseThrow(() -> new IllegalArgumentException("Horsepower cannot be null"));
        torque=Optional.ofNullable(engineBuilder.torque)
                .orElseThrow(() -> new IllegalArgumentException("Torque cannot be null"));
        transmissionType=Optional.ofNullable(engineBuilder.transmissionType)
                .orElse(TransmissionType.MANUAL);
        carType=Optional.ofNullable(engineBuilder.carType)
                .orElse(CarType.CROSSOVER);
        isTurbo = engineBuilder.isTurbo;

    }

    public static EngineBuilder builder() {
        return new EngineBuilder();
    }

    public EngineType getEngineType() {
        return engineType;
    }

    public int getEngineDisplacement() {
        return engineDisplacement;
    }

    public boolean isTurbo() {
        return isTurbo;
    }

    public int getNumberOfCylinders() {
        return numberOfCylinders;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public int getTorque() {
        return torque;
    }

    public TransmissionType getTransmissionType() {
        return transmissionType;
    }

    public CarType getCarType() {
        return carType;
    }

    public static class EngineBuilder {
        private EngineType engineType;
        private Integer engineDisplacement;
        private boolean isTurbo;
        private Integer numberOfCylinders;
        private Integer horsePower;
        private Integer torque;
        private TransmissionType transmissionType;
        private CarType carType;

        private EngineBuilder() {
        }

        public EngineBuilder withEngineType(EngineType engineType){
            this.engineType=engineType;
            return this;
        }

        public EngineBuilder withEngineDisplacement(Integer engineDisplacement){
            this.engineDisplacement=engineDisplacement;
            return this;
        }

        public EngineBuilder isTurbo(boolean isTurbo){
            this.isTurbo=isTurbo;
            return this;
        }

        public EngineBuilder withNumberOfCylinders(Integer numberOfCylinders){
            this.numberOfCylinders=numberOfCylinders;
            return this;
        }

        public EngineBuilder withHorsePower(Integer horsePower){
            this.horsePower=horsePower;
            return this;
        }

        public EngineBuilder withTorque(Integer torque){
            this.torque=torque;
            return this;
        }

        public EngineBuilder withTransmissionType(TransmissionType transmissionType){
            this.transmissionType=transmissionType;
            return this;
        }

        public EngineBuilder withCarType(CarType carType){
            this.carType=carType;
            return this;
        }

        public Engine build(){
            return new Engine(this);
        }

        @Override
        public String toString() {
            return "Engine{" +
                    "engineType=" + engineType +
                    ", engineDisplacement=" + engineDisplacement +
                    ", isTurbo=" + isTurbo +
                    ", numberOfCylinders=" + numberOfCylinders +
                    ", horsePower=" + horsePower +
                    ", torque=" + torque +
                    ", transmissionType=" + transmissionType +
                    ", carType=" + carType +
                    '}';
        }
    }
}
