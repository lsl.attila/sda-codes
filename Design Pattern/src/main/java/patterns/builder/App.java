package patterns.builder;

public class App {
    public static void main(String[] args) {

        Engine engine=Engine.builder()
                .withEngineType(EngineType.HYBRID)
                .withEngineDisplacement(2000)
                .withTransmissionType(TransmissionType.MANUAL)
                .withHorsePower(150)
                .withTorque(300)
                .build();
        System.out.println(engine);
    }
}
