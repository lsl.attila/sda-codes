package patterns.builder;

public enum CarType {
    SUV, CROSSOVER, COUPE, SEDAN, HATCHBACK
}
