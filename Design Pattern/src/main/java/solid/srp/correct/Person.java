package solid.srp.correct;

public class Person {
    public final String name;
    public final String surname;
    public final Email email;

    public Person(String name, String surname, Email email) {
        this.name = name;
        this.surname = surname;
        this.email = email;

    }

}
