package solid.srp.correct;

public interface Greeter {
    void greet();
}
