package solid.srp.correct;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {

    private static final Pattern EMAIL_PATTERN = Pattern.compile("[A-Za-z0-9_]+@(yahoo|outlook|gmail)\\.(ro|com|org)");

    private final String email;

    public Email(String email) {
        if (validateEmail(email)) {
            this.email = email;
        } else {
            throw new IllegalArgumentException(String.format("Invalid email address: %s", email));
        }
    }

    private static boolean validateEmail(String email) {
        Matcher matcher = EMAIL_PATTERN.matcher(email);
        return matcher.matches();
    }
}
