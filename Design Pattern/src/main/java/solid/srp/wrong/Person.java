package solid.srp.wrong;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Person {

    private static final Pattern EMAIL_PATTERN = Pattern.compile("[A-Za-z0-9_]+@(yahoo|outlook|gmail)\\.(ro|com|org)");
    public final String name;
    public final String surname;
    public final String email;

    public Person(String name, String surname, String email) {
        this.name = name;
        this.surname = surname;
        if (validateEmail(email)) {
            this.email = email;
        } else {
            throw new IllegalArgumentException(String.format("Invalid email address: %s", email));
        }
    }

    private static boolean validateEmail(String email) {
        Matcher matcher = EMAIL_PATTERN.matcher(email);
        return matcher.matches();
    }

    void greet() {
        System.out.println("Hi!");
    }
}
