package solid.ocp.correct;

public class App {
    static String uiSelectedPayment = "Cash";

    public static void main(String[] args) {
        PayApp payApp = new PayApp();
        Receipt receipt = new Receipt();

        PaymentMethod selectedPaymentMethod=PaymentMethod.mapToExistingPaymentMethod(uiSelectedPayment);
        payApp.checkout(receipt, selectedPaymentMethod.getPayable());
    }
}
