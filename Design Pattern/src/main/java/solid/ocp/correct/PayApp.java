package solid.ocp.correct;

import java.util.ArrayList;
import java.util.List;

public class PayApp {
    private List<Integer> orderedItems = new ArrayList<>();

    void checkout(Receipt receipt,Payable payable) {
        Integer toPay = 0;
        for (Integer item : orderedItems) {
            toPay += item;
            receipt.addReceiptItem(item);
        }
        payable.acceptPayment(toPay);
        receipt.addPayment(payable);
    }
}
