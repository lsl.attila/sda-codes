package solid.ocp.correct;

public class BitCoinPayment implements Payable {
    @Override
    public void acceptPayment(Integer totalAmount) {
        System.out.println("Payed " + totalAmount + " with bitcoin");
    }
}
