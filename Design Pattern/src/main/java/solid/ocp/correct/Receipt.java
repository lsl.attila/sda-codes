package solid.ocp.correct;


import java.util.ArrayList;
import java.util.List;

public class Receipt {
    private List<Integer> receiptItems = new ArrayList<>();
    private List<Payable> receiptPayments = new ArrayList<>();

    public void addReceiptItem(Integer receiptItem) {
        receiptItems.add(receiptItem);
    }

    public void addPayment(Payable payment) {
        receiptPayments.add(payment);
    }
}
