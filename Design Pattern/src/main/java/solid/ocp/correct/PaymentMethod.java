package solid.ocp.correct;

public enum PaymentMethod {
    CASH_PAYMENT("Cash", new CashPayment()),
    CARD_PAYMENT("Card", new CardPayment()),
    BITCOIN_PAYMENT("Bitcoin", new BitCoinPayment());

    private final String paymentMethod;
    private final Payable payable;

    PaymentMethod(String paymentMethod, Payable payable) {
        this.paymentMethod = paymentMethod;
        this.payable = payable;
    }

    public Payable getPayable() {
        return payable;
    }

    public static PaymentMethod mapToExistingPaymentMethod(String method) {
        for (PaymentMethod paymentMethod : PaymentMethod.values()) {
            if (paymentMethod.paymentMethod.equalsIgnoreCase(method)) {
                return paymentMethod;
            }
        }
        throw new IllegalArgumentException("Unknown payment method");
    }
}
