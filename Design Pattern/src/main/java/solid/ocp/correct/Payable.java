package solid.ocp.correct;

public interface Payable {
    void acceptPayment(Integer totalAmount);
}
