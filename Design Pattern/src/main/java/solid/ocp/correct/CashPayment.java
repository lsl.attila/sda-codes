package solid.ocp.correct;

public class CashPayment implements Payable {
    @Override
    public void acceptPayment(Integer totalAmount) {
        System.out.println("Payed " + totalAmount + " in cash");
    }
}
