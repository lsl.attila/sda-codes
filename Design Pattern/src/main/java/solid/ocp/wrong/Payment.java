package solid.ocp.wrong;

public class Payment {
    private Integer totalAmount;

    public Payment(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void acceptCash() {
        System.out.println("Payed " + totalAmount + " in cash");
    }

    public void acceptCard() {
        System.out.println("Payed " + totalAmount + " with card");
    }

    public void pay(String method) {
        switch (method) {
            case "cash":
                acceptCash();
                break;
            case "card":
                acceptCard();
                break;
            default:
                System.out.println("Unknown payment method!");

        }
    }
}
