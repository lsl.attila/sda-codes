package solid.ocp.wrong;

import solid.ocp.wrong.Payment;

import java.util.ArrayList;
import java.util.List;

public class Receipt {
    private List<Integer> receiptItems = new ArrayList<>();
    private List<Payment> receiptPayments = new ArrayList<>();

    public void addReceiptItem(Integer receiptItem) {
        receiptItems.add(receiptItem);
    }

    public void addPayment(Payment payment) {
        receiptPayments.add(payment);
    }
}
