package solid.ocp.wrong;

import java.util.ArrayList;
import java.util.List;

public class PayApp {
    private List<Integer> orderedItems=new ArrayList<>();

    void checkout(Receipt receipt, String paymentMethod){
        Integer toPay=0;
        for (Integer item:orderedItems){
            toPay+=item;
            receipt.addReceiptItem(item);
        }

        Payment payment=new Payment(toPay);
        payment.pay(paymentMethod);
        receipt.addPayment(payment);
    }


}
