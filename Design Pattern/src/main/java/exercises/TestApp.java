package exercises;



public class TestApp {

    private static final String[] WEEK_DAYS = {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"
    };

    public static void main(String[] args) {
        System.out.println(getWeekDay(7));
        System.out.println(getWeekDay(6));
        System.out.println(getWeekDay2(2));
    }

    public static String getWeekDay(int dayNo) {
        if (dayNo < 1 || dayNo > 7) {
            return null;
        }
        return WEEK_DAYS[dayNo - 1];
    }

    public static String getWeekDay2(int dayNo){
        switch (dayNo) {
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
            default:
                throw new IllegalArgumentException(String.format("Incorrect day number value %d ", dayNo));
        }
    }
}
