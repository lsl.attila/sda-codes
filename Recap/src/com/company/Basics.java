package com.company;
//1.Use System.out.print method to print the same statement in separate lines.

import java.text.DecimalFormat;

//        2.Enter any value with several digits after the decimal point and assign it to variable of type double. Display the given value rounded to two decimal places.
//        3.Display any three strings of characters on one line so that they are aligned to the right edge of the 15-character blocks. How to align strings to the left edge?
//        4.Enter two values of type int. Display their division casted to the double type and rounded to the third decimal point.
//        5.*Sum two integer variables initialized with maximal values for that type.
//        6.Create three variables, one for each type: float, byte and char. Enter values corresponding to those types using Scanner. What values are you able to enter for each type?
public class Basics {
    //1
    public static void Statement(String statement) {
        System.out.println(statement);
        System.out.println(statement);
    }

    //2
    public static double roundDouble(double no) {
        DecimalFormat decimalFormat=new DecimalFormat("#.##");
        return Double.valueOf(decimalFormat.format(no));
    }

    public static void main(String[] args) {
        System.out.println(roundDouble(789.123812));
    }
}
