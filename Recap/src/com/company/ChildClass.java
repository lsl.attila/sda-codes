package com.company;

public class ChildClass extends ParentClass{

    int y;
    int z;

    public ChildClass(int a, int b) {
        super(a);
        this.y = b;
    }
//    can't call this and super in the same constructor
//    public ChildClass(int a, int b, int c) {
//        super(a);
//        this(b,c);
//    }

}
