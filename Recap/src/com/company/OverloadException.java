package com.company;

public class OverloadException {

    void sum(int a, long b) {
        System.out.println("method a is invoked");
    }


    void sum(long a, int b) {
        System.out.println("method b is invoked");
    }

    public static void main(String[] args) {
        //just changing parameters order is not accepted as overloading
        OverloadException obj = new OverloadException();
//        obj.sum(20,20);

    }

}
