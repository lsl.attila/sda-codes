package com.company;

public class ConstantExampleClass {

    final static int constantVariable = 1;
    static final Point point = new Point(0, 1);

    public static void main(String[] args) {
        //can't change final variable
        //constantVariable = 2;
        point.x = 3;
        System.out.println(point.toString());

        Point point2 = new Point(10, 15);
        //can't change reference to final variable
        //point = point2;
    }

}
