package com.company.designPatterns;

public class FacebookStrategy implements MessageStrategy {

    @Override
    public void sendMessage(String message) {
        System.out.println("Message sent to Facebook "+message);
    }
}
