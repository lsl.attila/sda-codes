package com.company.designPatterns;

public class MotionSensor extends Sensor {
    private int distance;

    public MotionSensor(int distance) {
        this.distance = distance;
    }

    public int getDistance() {
        return distance;
    }
}
