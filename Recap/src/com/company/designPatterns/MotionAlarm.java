package com.company.designPatterns;

public class MotionAlarm {

    public void startAlarm(){
        System.out.println("The alarm has started");
    }

    public void stopAlarm(){
        System.out.println("The alarm has stopped");
    }
}
