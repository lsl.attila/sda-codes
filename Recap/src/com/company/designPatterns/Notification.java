package com.company.designPatterns;

public abstract class Notification {
    public abstract String getMessage();

    public void sendNotification(MessageStrategy messageStrategy) {
        messageStrategy.sendMessage(getMessage());

    }

}
