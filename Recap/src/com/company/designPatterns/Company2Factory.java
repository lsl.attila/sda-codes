package com.company.designPatterns;

public class Company2Factory implements SensorAbstractFactory{
    @Override
    public MotionSensor createMotionSensor(int distance) {
        System.out.println("created company2motionsensor");
        return new Company2MotionSensor(distance);
    }

    @Override
    public SmokeSensor createSmokeSensor(int volume) {
        System.out.println("created company2smokesensor");
        return new Company2SmokeSensor(volume);
    }
}
