package com.company.designPatterns;

public class SmsStrategy implements MessageStrategy{
    @Override
    public void sendMessage(String message) {
        System.out.println("Message sent by SMS "+message);
    }
}
