package com.company.designPatterns;

public class SmokeSensor extends Sensor {
    private int volume;

    public SmokeSensor(int volume) {
        this.volume = volume;
    }

    public int getVolume() {
        return volume;
    }
}
