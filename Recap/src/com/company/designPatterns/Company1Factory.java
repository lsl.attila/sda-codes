package com.company.designPatterns;

public class Company1Factory implements SensorAbstractFactory{
    @Override
    public MotionSensor createMotionSensor(int distance) {
        System.out.println("created company1motionsensor");
        return new Company1MotionSensor(distance);
    }

    @Override
    public SmokeSensor createSmokeSensor(int volume) {
        System.out.println("created company1smokesensor");
        return new Company1SmokeSensor(volume);
    }
}
