package com.company.designPatterns;

import java.util.ArrayList;
import java.util.List;

public class SensorSystem implements Subject {
    List<Observer>sensorList;

    public SensorSystem() {
        this.sensorList = new ArrayList<>();
    }

    @Override
    public void register(Observer sensorObserver) {
        sensorList.add(sensorObserver);
    }

    @Override
    public void notifyObservers() {
//        for (Observer observer:sensorList) {
//            observer.detect();
//        }
        sensorList.forEach(observer -> observer.detect());
    }

    @Override
    public void unregister(Observer sensorObserver) {
        sensorList.remove(sensorObserver);
    }
}
