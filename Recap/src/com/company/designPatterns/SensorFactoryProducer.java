package com.company.designPatterns;

public class SensorFactoryProducer {
    public static Sensor createSensor(String companyName, String sensorType, int parameter) {
        Sensor sensor;

        switch (companyName + sensorType) {
            case "Company1SmokeSensor":
                sensor = new Company1Factory().createSmokeSensor(parameter);
                break;
            case "Company2SmokeSensor":
                sensor = new Company2Factory().createSmokeSensor(parameter);
                break;
            case "Company1MotionSensor":
                sensor = new Company1Factory().createMotionSensor(parameter);
                break;
            case "Company2MotionSensor":
                sensor = new Company2Factory().createMotionSensor(parameter);
                break;
            default:
                sensor = null;
        }
        return sensor;
    }
}
