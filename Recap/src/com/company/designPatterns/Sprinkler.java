package com.company.designPatterns;

public class Sprinkler {

    public void turnOn(){
        System.out.println("The sprinkler has turned on");
    }

    public void turnOff(){
        System.out.println("The sprinkler has turned off");
    }
}
