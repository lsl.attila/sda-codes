package com.company.designPatterns;

public interface SensorAbstractFactory  {
    MotionSensor createMotionSensor(int distance);
    SmokeSensor createSmokeSensor(int volume);
}
