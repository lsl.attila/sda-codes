package oopGym;

import java.util.ArrayList;

public class Trainer {
    private String name;

    //todo choose just one
//    private Trainee[] traineesArr;
    private ArrayList<Trainee> traineeArrayList;

    public Trainer(String name, ArrayList<Trainee> trainees){
        this.name=name;
        this.traineeArrayList=trainees;
    }

    public void giveExercises() {
        for (Trainee t : traineeArrayList) {
            switch (t.getObjective()) {
                case "LOOSE_WEIGHT":
                    t.exercise("CARDIO");
                    break;
                case "MAINTAIN":
                    t.exercise("STRETCHING");
                    break;
                case "GAIN_STRENGHT":
                    //23 cu 24 echivalnete
                    t.exercise("STRENGHT");
                    break;
//                    return ExerciseType.STRENGHT.name();
                default:

            }

        }

    }
}
