package oopGym;

import java.util.TreeSet;

public class Trainee {
    //Create class Trainee, it should contain fields like:
    // name, stamina, strength, weight. You’ll simulate both
    // sides – Trainer and Trainee. Within a while loop you
    // will be asked for an exercise to be done. Every exercise
    // should add/reduce stamina/strength. Take into account
    // that stamina should not be reduced below 0. Consider
    // adding some supplements that will recover the stamina.
    // Supplement should be additional class.

    private String name;
    private int stamina;
    private double strength;
    private double weight;
    private String objective;//LOOSE_WEIGHT, MAINTAIN, GAIN_STRENGHT

    //todo add getters and setters
    public String getObjective() {
        return this.getObjective();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStamina() {
        return stamina;
    }


    public double getStrength() {
        return strength;
    }


    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    //todo add constuctor, having stamina value 100 for all trainees
    public Trainee(String name, double weight, String objective) {
        this.name = name;
        this.weight = weight;
        this.objective = objective;
        this.stamina = 50;
        this.strength = 50;
    }

    public void exercise(String exerciseType) {
        stamina--;
        switch (exerciseType) {
            case "CARDIO":
                weight--;
                break;
            case "STRENGHT":
                strength++;
               break;

        }
    }

}
