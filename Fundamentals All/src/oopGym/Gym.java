package oopGym;

import java.util.ArrayList;

public class Gym {
    public static void main(String[] args){
        Trainee client1=new Trainee("Vasile",79,"LOOSE_WEIGTH");
        Trainee client2=new Trainee("Izabela",55,"STRENGHT");
        ArrayList<Trainee>trainees=new ArrayList<>();
        trainees.add(client1);
        trainees.add(client2);
        Trainer trainer1=new Trainer("Gigi",trainees);

        trainer1.giveExercises();

        System.out.println(client1.getStamina());
        System.out.println(client2.getStamina());

    }
}
