package core;
import collection.StudentList;

import java.util.Scanner;

public class Person {
    protected String name;
    protected int age;


    public Person(String n, int a) {
        name = n;
        age = a;
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int newAge) {
        age=newAge;
    }
}


