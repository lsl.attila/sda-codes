package core;

//+public -private
public class Student extends Person {
    private String faculty;

    public Student(String n, int a, String f) {
        super(n, a);
        faculty = f;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String newFaculty) {
        faculty = newFaculty;
    }

    @Override
    public String toString() {
        return "Student{" +
                "faculty='" + faculty + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
