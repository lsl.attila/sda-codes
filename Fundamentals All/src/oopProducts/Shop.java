package oopProducts;

import org.w3c.dom.ls.LSOutput;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Shop {

    private Product[] allProducts;
    private Product[] cart;
    private int shopBalance;

    private ArrayList<Product>allProductsArr=new ArrayList<>();

    public int getShopBalance() {
        return this.shopBalance;
    }

    public void setShopBalance(int myShopBalance) {
        this.shopBalance = myShopBalance;
    }

    public Shop(Product[] products) {
        this.allProducts = products;
        this.cart = new Product[0];
        this.shopBalance = 0;

    }

    public String previewProduct(String productName) {
        for (int i = 0; i < allProducts.length; i++) {
            if (allProducts[i].getName().equalsIgnoreCase(productName)) {
                return "The product " + productName + " is at shelf " + i + " and it costs " + allProducts[i].getPrice() + " RON";
            }
        }
        return "Product was not found!";
    }

    public void addToCart(int productIndex) {
        if (cart.length == 5) {
            System.out.println("You cannot do any more shopping");
            return;
        }

        Product p = allProducts[productIndex];
        Product[] newCart = Arrays.copyOf(cart, cart.length + 1);
        newCart[newCart.length - 1] = p;
        cart = newCart;
        System.out.println("The product was added succesfully");
    }

    public void viewCart() {
        System.out.println("You have " + cart.length + " items in your cart " + Arrays.toString(cart) + "that cost " + getCartTotal() + " RON");
    }

    public void payProducts(int amountPaid) {
        //validate input and give change
        int amountExpected = getCartTotal();
        if (amountExpected == 0) {
            System.out.println("You don't have anything on your cart");
            return;
        }
        if (amountPaid < amountExpected) {
            System.out.println("Please introduce the right amount");
            return;
        } else {
            int change = amountPaid - amountExpected;
            System.out.println("Your change is: " + change + " RON");

            shopBalance += amountExpected;
            cart = new Product[0];
            System.out.println("Products paid successfully");
        }
        //increase shop balance
        //reset card
    }

    private int getCartTotal() {
        int total = 0;
        for (Product p : cart) {
            total += p.getPrice();
        }
        return total;
    }

    public void deleteFromCart(String productName) {
        Product[] newCart = new Product[cart.length - 1];
        boolean found = false;
        int i;
        int j = 0;
        for (i = 0; i < cart.length - 1; i++) {
            if (cart[i].getName().equalsIgnoreCase(productName)) {
                i++;
                found=true;
            } else {
                newCart[j] = cart[i];
            }j++;
        }
        if (found) {
            cart = newCart;
        } else {
            System.out.println("Element not found");
        }
        System.out.println(Arrays.toString(cart));
    }

    public void example(){
        Product newProduct=new Product();
        allProductsArr.isEmpty();
        allProductsArr.add(newProduct);
        allProductsArr.remove(newProduct);
        allProductsArr.add(3,newProduct);
        allProductsArr.remove(2);
    }
    //Queue - FIFO
    //Stack - LIFO


}
