package oopProducts;

public class Product {
    private String name;
    private int price;

    public Product(){

    }
    public Product(String nume,int pret){
        this.name=nume;
        this.price=pret;
    }

    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price;
    }

    public void setName(String name){
        this.name=name;
    }

    public void setPrice(int price){
        this.price=price;
    }

    public String toString(){
        return "Product name "+name+" and price "+price+"\n";
    }

}
