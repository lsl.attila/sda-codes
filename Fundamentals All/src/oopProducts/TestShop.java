package oopProducts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class TestShop {

    public static void main (String[] args){
        Product product1=new Product("Coca-Cola",4);
        Product product2=new Product("Lipton",3);
        Product product3=new Product("Pepsi",6);
        Product product4=new Product("Fanta",5);
        Product product5=new Product("Heineken",8);

        Product[] products={product1,product2,product3,product4,product5};

        Shop shop=new Shop(products);

        String menuString = "Hello, Customer! Choose an option below: " +
                "\n 1 - View all products in the shop " +
                "\n 2 - Preview product" +
                "\n 3 - Add product to cart " +
                "\n 4 - View cart" +
                "\n 5 - Pay products " +
                "\n 6 - View shop balance";
        System.out.println(menuString);

        Scanner scanner = new Scanner(System.in);

        while(scanner.hasNext()){
            if(scanner.hasNextInt()){
                switch (scanner.nextInt()){
                    case 1:
                        System.out.println(Arrays.toString(products));
                        break;
                    case 2:
                        System.out.println("Select the name of the product you want to see: ");
                        System.out.println(shop.previewProduct(scanner.next()));
                        break;
                    case 3:
                        System.out.println("Select the index of the product you want to add");
                        shop.addToCart(scanner.nextInt());
                        break;
                    case 4:
                        shop.viewCart();
                        break;
                    case 5:
                        System.out.println("Introduce the amount you want to pay: ");
                        if(scanner.hasNextInt()){
                            shop.payProducts(scanner.nextInt());
                        }else{
                            System.out.println("Please introduce the right amount");
                        }
                        break;
                    case 6:
                        System.out.println("Shop balance is : "+shop.getShopBalance());
                        break;
                    case 7:
                        System.out.println("Delete an item from cart by name");
                    shop.deleteFromCart(scanner.next());
                        default:
                        System.out.println("Choose another option");

                }
            }else{
                System.out.println("Input not valid! "+scanner.next());
                scanner.close();
            }
        }
    }
}
