import java.awt.Frame;
import java.awt.Button;
import java.awt.Label;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ExampleFrame extends Frame implements ActionListener {
    private Button buttons[];
    private Label label;

    public ExampleFrame() {
        this.setTitle("Example");
        //gombok letrehoza
        buttons = new Button[2];
//        buttons[0] = new Button("Button 1");
//        this.add(buttons[0], BorderLayout.NORTH);
//        buttons[1] = new Button("Button 2");
//        this.add(buttons[1], BorderLayout.SOUTH);
        buttons[0] = new Button("Button 1");
        buttons[0].addActionListener(this);
        add(buttons[0], BorderLayout.NORTH);
        buttons[1] = new Button("Button 2");
        buttons[1].addActionListener(this);
        add(buttons[1], BorderLayout.SOUTH);
//        buttons[0].addActionListener(this::actionPerformed);
//        buttons[1].addActionListener(this::actionPerformed);
//        buttons[0].addActionListener(new MyFirstListener());
//        buttons[1].addActionListener(new MySecondListener());
        //a cimke letrehozasa
        label = new Label("Bernike a legjobb");
//        this.add(label, BorderLayout.CENTER);
        add(label, BorderLayout.CENTER);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    public void actionPerformed(ActionEvent e){
        label.setText(e.getActionCommand());
    }


//    class MyFirstListener implements ActionListener {
//        public void actionPerformed(ActionEvent e) {
//            if (e.getSource() == buttons[0]) {
//                label.setText("elso");
//            }
//        }
//    }

//    class MySecondListener implements ActionListener {
//        public void actionPerformed(ActionEvent e) {
//            if (e.getSource() == buttons[1]) {
//                label.setText("madosik");
//            }
//        }
//
//    }


    public static void main(String[] args) {
        ExampleFrame f = new ExampleFrame();
        f.setBounds(50, 50, 200, 120);
        f.setVisible(true);
    }
}
