public class Control {
    public static void main(String[] args){
//Szarmaztatasra pelda
//        MyThread[] threads=new MyThread[2];
//        threads[0]=new MyThread("First thread");
//        threads[1]=new MyThread("Second thread");
//        threads[0].start();
//        threads[1].start();

//Runnable interfesz megvalositva
        MyRunnable[] objects=new MyRunnable[2];
        objects[0]=new MyRunnable();
        objects[1]=new MyRunnable();
        Thread[] threads=new Thread[2];
        threads[0]=new Thread(objects[0],"First");
        threads[1]=new Thread(objects[1],"Second");
        threads[0].start();
        threads[1].start();
    }
}
