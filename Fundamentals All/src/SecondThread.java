import java.io.PipedReader;
import java.io.BufferedReader;
import java.io.IOException;
public class SecondThread extends Thread{
    private PipedReader piperead;
    public SecondThread(String name, PipedReader pr){
        super(name);
        piperead=pr;
    }
    public void run(){
        String s=null;
        BufferedReader br=new BufferedReader(piperead);
        while(true){
            try {
                s=br.readLine();
            }catch (IOException e){
                System.out.println("Error reading");
            }
            System.out.println(getName() + " : " + s + " read from pipe");
            try {
                sleep(1000);
            }catch (InterruptedException e){}
        }
    }
}
