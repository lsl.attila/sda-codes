import collection.StudentIterator;
import collection.StudentList;
import core.Student;

import java.util.Scanner;

public class TestStudentList {
    public static void main(String[] args){
        Student s1=new Student("Bernike", 23,"Orvosi");
//        System.out.println(s1);
        Student s2=new Student("Attika",22,"Villammosmernoki");
//        System.out.println(s2);
        StudentList list= new StudentList(3);
        list.addStudent(s1);
        list.addStudent(s2);
        StudentIterator it=list.getReverseIterator();
        while(it.hasMoreElements()){
            System.out.println(it.nextElement());
        }
    }
}
