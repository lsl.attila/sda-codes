package JavaFundamentals;

import java.awt.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int milkPrice = 5;
        int winePrice = 15;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you want to buy milk? ");
        String answer = scanner.next();
        if (answer.equals("Yes")) {
            System.out.println("The milk will be 5 RON");
            int milk = scanner.nextInt();
            if (milk > milkPrice) {
                System.out.println("Here's your milk and your change sir: " + (milk - milkPrice));
            } else if (milk == milkPrice) {
                System.out.println("Here's your milk");
            } else {
                System.out.println("Sorry you don't have enough money");
            }
        } else {
            System.out.println("Do you want to buy a bottle of wine?");
            answer = scanner.next();
            if (answer.equals("Yes")) {
                System.out.println("Are you an adult?");
                int age = scanner.nextInt();
                if (age > 18) {
                    System.out.println("The wine will be 15 RON");
                    int wine = scanner.nextInt();
                    if (wine > winePrice) {
                        System.out.println("Here's your bottle of wine and here's your change: " + (wine - winePrice));
                    } else if (wine == winePrice) {
                        System.out.println("Here's your bottle of wine");

                    } else {
                        System.out.println("You don't have enough money for a bottle of wine");
                    }
                } else {
                    System.out.println("Sorry you can't buy a bottle of wine because you are underage");
                }
            }
        }

    }
}

