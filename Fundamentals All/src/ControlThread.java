public class ControlThread {
    public static void main(String[] args){
        FirstThread ft=new FirstThread("Writer");
        SecondThread st=new SecondThread("Reader",ft.getReader());
        ft.start();
        st.start();
    }
}
