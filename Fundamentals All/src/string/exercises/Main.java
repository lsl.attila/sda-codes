package string.exercises;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Exercitii Java Fundamentals Coding 1");
        String spacer = "\n=============\n";
        //1.Exercitiul unu
        {
            System.out.println("Hello");
            System.out.println("John");
        }
        //2.Exercitiul doi
        {
            System.out.println("     J");
            System.out.println("     J");
            System.out.println("J    J");
            System.out.println("  JJ  ");
            System.out.println(spacer);
        }
        //3.Exercitiul trei
        //randStr=randomString
        {
            String randStr = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG";
            randStr = randStr.toLowerCase();
            System.out.println(randStr);
        }
        //4.Exercitiul patru
        {
            String revStr = "avaJ";
            String rezStr = "";
            for (int i = revStr.length() - 1; i >= 0; i--) {
                rezStr = rezStr + revStr.charAt(i);
            }
            System.out.println(rezStr);
        }
        {
            String revStr = "avaJ";
            StringBuilder sb = new StringBuilder();
            for (int i = revStr.length() - 1; i >= 0; i--) {
                sb = sb.append(revStr.charAt(i));
            }
            System.out.println(sb.toString());
        }
        //Exercitiul 5
        {
            String progStr = "Programmer";
            System.out.println(progStr.substring(0, progStr.length() / 2));
        }
        //Exercitiul 6
        {
            String firstStr = "Java";
            String secondStr = "Fundamentals";
            System.out.println(firstStr.replace("J", "") + secondStr.replace("F", ""));
        }
        {
            String firstStr = "Java";
            String secondStr = "Fundamentals";
            firstStr = firstStr.substring(1, firstStr.length());
            secondStr = secondStr.substring(1, secondStr.length());
            System.out.println(firstStr + secondStr);
        }
        {
            String firstStr = "Java";
            String secondStr = "Fundamentals";
            String firstSubstr = firstStr.substring(1);
            String secondSubstr = secondStr.substring(1);
            StringBuilder sb = new StringBuilder();
            sb.append(firstSubstr);
            sb.append(secondSubstr);
            System.out.println(sb);
        }
        //Exercitiul 7
        {
            String rezStr = "This is Java!";
            System.out.println("Lenght: " + rezStr.length());
        }
        //Exercitiul 8
        {
            String firstStr = "This is a comparison";
            String secondStr = "THIS is a comparison";
            if (firstStr.equalsIgnoreCase(secondStr)) {
                System.out.println("The two strings are identical");
            } else {
                System.out.println("The two strings are not identical");
            }
        }

        //Exercitiul 9
        {
            String firstStr = "Java exercises";
            String secondStr = "ses";
            if (firstStr.endsWith(secondStr)) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }

        //Exercitiul 10
        //Sa fac si cu codul ascii
//        {
//            String firstString = "A akiu, I swd skieo 2397. GH kiu: sieo?? 25.33";
//            char[] countChar = firstString.toCharArray();
//            int letters = 0;
//            int spaces = 0;
//            int numbers = 0;
//            int others = 0;
//            for (int i = 0; i < firstString.length(); i++) {
//                if (Character.isLetter(countChar[i])) {
//                    letters++;
//                } else if (Character.isDigit(countChar[i])) {
//                    numbers++;
//                } else if (Character.isSpaceChar(countChar[i])) {
//                    spaces++;
//                } else {
//                    others++;
//                }
//            }
//            System.out.println("Number of letters: " + letters);
//            System.out.println("Number of spaces: " + spaces);
//            System.out.println("Number of numbers: " + numbers);
//            System.out.println("Number of others: " + others);
//
//        }
//
//        {
//            Scanner scanner = new Scanner(System.in);
//            System.out.print("Enter username: ");
//
//            String userName = scanner.nextLine();
//            System.out.print("Username is: " + userName);
//
//        }
//        System.out.println();
//        {
//            Scanner scanner = new Scanner(System.in);
//            System.out.println("Write input: ");
//            String sentence = scanner.nextLine();
//            int letters = 0;
//            int numbers = 0;
//            int spaces = 0;
//            int others = 0;
//            int asciiA = (int) 'A';
//            int asciiz = (int) 'z';
//            int ascii0 = (int) '0';
//            int ascii9 = (int) '9';
//            for (int i = 0; i < sentence.length(); i++) {
//                char elem = sentence.charAt(i);
//                if (elem >= asciiA && elem <= asciiz) {
//                    letters++;
//                } else if (elem > ascii0 && elem <= ascii9) {
//                    numbers++;
//                } else if (elem == ' ') {
//                    spaces++;
//                } else {
//                    others++;
//                }
//            }
//            System.out.println("Number of letters: " + letters);
//            System.out.println("Number of spaces: " + spaces);
//            System.out.println("Number of numbers: " + numbers);
//            System.out.println("Number of others: " + others);
//        }

//        System.out.println("Exercitii Java Fundamentals Coding 2");
//        Scanner scanner = new Scanner(System.in);
//        {
//        System.out.print("Introduceti prima variabila: ");
//        int a=scanner.nextInt();
//        System.out.print("Introduceti al doilea variabila: ");
//        int b=scanner.nextInt();
//        int sum=a+b;
//        System.out.println("Suma numerelor este: "+sum);}
//
//        {
//        System.out.print("Introduceti prima variabila: ");
//        float a=scanner.nextInt();
//        System.out.print("Introduceti al doilea variabila: ");
//        float b=scanner.nextInt();
//        System.out.println("Catul impartirii este: "+a/b);
//        System.out.println("Restul impartirii este: "+a%b);}
//
//        {
//        System.out.print("Introduceti raza cercului: ");
//        double raza=scanner.nextInt();
//        double perimetru=2*Math.PI*raza;
//        double aria=Math.PI*raza*raza;
//        System.out.println("Perimetrul cercului este: "+perimetru);
//        System.out.println("Aria cercului este: "+aria);}
//        {
//            System.out.print("Introduceti prima variabila: ");
//            int a = scanner.nextInt();
//            System.out.print("Introduceti al doilea variabila: ");
//            a += scanner.nextInt();
//            System.out.print("Introduceti al treilea variabila: ");
//            a += scanner.nextInt();
//            double media = a/3.0;
//            System.out.println("Media aritmetica este: "+media);
//        }
//
//        {
//        System.out.print("Introduceti lungimea dreptunghiului: ");
//        double lungime=scanner.nextDouble();
//        System.out.print("Introduceti latimea dreptunghiului: ");
//        double latime=scanner.nextDouble();
//        System.out.println("Aria dreptunghiului este: "+latime*lungime);
//        System.out.println("Perimetrul dreptunghiului este: "+2*(latime+lungime));}
//
//        {System.out.println("Introduceti primul numar: ");
//        int a = scanner.nextInt();
//        System.out.println("Introduceti al doilea numar: ");
//        int b = scanner.nextInt();
//        System.out.println("Primul numar inainte de inversare este: " + a);
//        System.out.println("Al doilea numar inainte de inversare este: " + b);
//        int c = a;
//        a = a + b; //se poate face si asa
//        b = a - b;
//        a = a - b;
//        a = b;
//        b = c;
//        System.out.println("Primul numar dupa de inversare este: " + a);
//        System.out.println("Al doilea numar dupa de inversare este: " + b);}
//
//        System.out.println("Introduceti primul numar: ");
//        int a = scanner.nextInt();
//        System.out.println("Introduceti al doilea numar: ");
//        int b = scanner.nextInt();
//        if (a > b) {
//            System.out.println("Prima variabila este mai mare");
//        }
//        else if (a < b) {
//            System.out.println("Al doilea variabila este mai mare");
//        }
//        else {
//            System.out.println("Variabilele sunt egale");
//        }
//
//        {System.out.println("Introduceti numarul: ");
//        int a = scanner.nextInt();
//        int sum=0;
//        while(a!=0){
//            sum+=a%10;
//            a=a/10;
//        }
//        System.out.println("Suma cifrelor este: "+sum);}
//
//        {
//            System.out.println("Introduceti numarul de multiplicat: ");
//            int a = scanner.nextInt();
//            System.out.println("Introduceti numarul de multiplii: ");
//            int b = scanner.nextInt();
//            for(int i=1;i<=b;i++){
//                System.out.print(a*i+" ");
//            }
//        }
//
//        {
//            System.out.println("Introduceti valoarea mininma: ");
//            int a = scanner.nextInt();
//            System.out.println("Introduceti valoarea maxima: ");
//            int b = scanner.nextInt();
//            System.out.println("Numerele pare intre "+a+" si "+b+" sunt: ");
//            while (b>=a) {
//                if (a % 2 == 0) {
//                    System.out.print(a + " ");
//                }
//                a++;
//            }
//        }
//
//        {
//            System.out.println("Introduceti prima variabila: ");
//            int a = scanner.nextInt();
//            for (int i = 0; i <a ; i++) {
//                for (int j = 0; j <=i ; j++) {
//                    System.out.print(a+" ");
//
//                }System.out.println();
//
//            }
//
//        }
//
//        {
//            System.out.println("Introduceti valoarea mininma: ");
//            int a = scanner.nextInt();
//            System.out.println("Introduceti valoarea maxima: ");
//            int b = scanner.nextInt();
//            System.out.println("Numerele divizibile cu 3 intre "+a+" si "+b+" sunt: ");
//            for (int i = a; i < b; i++) {
//                if (i % 3 == 0) {
//                    System.out.print(i + "\t");
//                }
//            }
//            System.out.println();
//            System.out.println("Numerele divizibile cu 5 intre " +a+" si "+b+" sunt: ");
//
//            for (int i = a; i < b; i++) {
//                if (i % 5 == 0) {
//                    System.out.print(i + "\t");
//                }
//            }
//            System.out.println();
//            System.out.println("Numerele divizibile cu 15 intre " +a+" si "+b+" sunt: ");
//
//            for (int i = a; i < b; i++) {
//                if (i % 3 == 0 && i % 5 == 0) {
//                    System.out.print(i + "\t");
//                }
//            }
//            System.out.println();
//        }
//
//        {
//            System.out.println("Introduceti valoarea mininma: ");
//            int a = scanner.nextInt();
//            System.out.println("Introduceti valoarea maxima: ");
//            int b = scanner.nextInt();
//            int rezultat=1;
//            for (int i = 0; i <b ; i++) {
//                rezultat*=a;
//            }
//            System.out.println("Rezultatul este: "+rezultat);
//        }
//
//        {
//            System.out.println("Exercitii Java Fundamentals Coding 3");
//            int[] a = {6, 7, 3, 10, 9};
//            double suma = 0;
//            for (int i = 0; i < a.length; i++) {
//                suma += a[i];
//            }
//            double sum = 0;
//            for (int s : a) {
//                sum += s;//cast (double)sum
//            }
//            System.out.println("Metoda 1: Suma numerelor din sir este: " + suma);
//            System.out.println("Metoda 2: Suma numerelor din sir este: " + sum);
//            System.out.println("Media aritmetica este: " + sum / a.length);
//            System.out.println("Numerele impare din sir sunt: ");
//            for (int i = 0; i < a.length; i++) {
//                if (a[i] % 2 != 0) {
//                    System.out.print(a[i] + "\t");
//                }
//            }
//            System.out.println();
//            int min = a[0];
//            int max = 0;
//            for (int i = 0; i < a.length; i++) {
//                if (a[i] < min) {
//                    min = a[i];
//                }
//                if (a[i] > max) {
//                    max = a[i];
//
//                }
//            }
//            System.out.println("Valoarea minima este: " + min);
//            System.out.println("Valoarea minima este: " + max);
//            System.out.println("Arrayul inversat");
//            int[] b = new int[a.length];
//            for (int i = a.length - 1; i >= 0; i--) {
//                b[a.length - i - 1] = a[i];
//            }
//            for (int i = 0; i < b.length; i++) {
//                System.out.print(b[i] + " ");
//            }
//            System.out.println();
////Se poate si cu un for
//
//
//            int odd = 0;
//            int even = 0;
//            for (int i = 0; i < a.length; i++) {
//                if (a[i] % 2 == 0) {
//                    even++;
//                } else {
//                    odd++;
//                }
//            }
//            System.out.println("Numarul numerelor pare este: " + even);
//            System.out.println("Numarul numerelor pare este: " + odd);
//
//            int[] c = new int[a.length + 1];
//            int poz = 2;
//            int val = 4;
//            for (int i = 0; i <= poz; i++) {
//                c[i] = a[i];
//            }
//            c[poz] = val;
//            poz++;
//            for (int i = poz; i < c.length; i++) {
//                c[i] = a[i - 1];
//            }
//            for (int i : c) {
//                System.out.print(i + " ");
//            }
//            System.out.println();
//
//            System.out.println("Numerele comune sunt: ");
//            int[] array = new int[]{1, 5, 6, 4, 4, 8, 9, 6};
//            boolean check = true;
//            for (int i = 0; i < array.length; i++) {
//                for (int j = i + 1; j < array.length; j++) {
//                    if (array[i] == array[j]) {
//                        System.out.print(array[i] + " ");
//                        break;
//                    }
//                }
//            }
//            System.out.println();
//
//            System.out.println("Valoarea maxima a sirului este: ");
//            int[] vector1 = new int[]{1, 7, 3, 10, 9};
//            int max1 = vector1[0];
//            int max2 = vector1[0];
//            for (int i = 0; i < vector1.length; i++) {
//                if (vector1[i] > max1) {
//                    max1 = vector1[i];
//                }
//            }
//            for (int i = 0; i < vector1.length; i++) {
//                if (vector1[i] > max2 && vector1[i] != max1) {
//                    max2 = vector1[i];
//                }
//            }
//            System.out.println(max1);
//            System.out.println(max2);
//
//            int[] array1 = new int[]{1, 2, 7, 3, 10, 2, 9};
//            int sumaVect = 4;
//            for (int i = 0; i < array1.length; i++) {
//                for (int j = i + 1; j < array1.length; j++) {
//                    if (array1[i] + array1[j] == sumaVect) {
//                        System.out.println(array1[i] + "+" + array1[j]);
//                    }
//                }
//            }
//
//        }

        {
            int[] arr = new int[]{1, 2, 7, 3, 10, 2, 9};
            System.out.println(maxArr(arr));
            int m=maxArr(arr);
            System.out.println(m);

        }

        int[] arr1=new int[]{2,4,5,8,6,7,4};
        numereDiv(2,arr1);


    }

    static int maxArr(int[] array){
        int maximum=array[0];
        for (int i = 0; i <array.length ; i++) {
            if(array[i]>maximum){
                maximum=array[i];
            }
        }
        return maximum;
    }

    static void numereDiv(int numar, int[] sirDiv){
        for (int i = 0; i <sirDiv.length ; i++) {
            if(sirDiv[i]%numar==0){
                System.out.println(sirDiv[i]);
            }
        }
    }


}