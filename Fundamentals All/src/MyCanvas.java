import java.awt.BorderLayout;
import java.awt.Canvas;//rajzvaszon letrehozasahoz szukseges
import java.awt.Color;//szinek beallitasahoz szukseges
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Graphics;//rajzolashoz szukseges
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Image;

public class MyCanvas extends Canvas {
    private Image img;
    private Graphics gr;
//    private int x = 0;
//    private int y = 0;

    public MyCanvas() {
        setBackground(new Color(50, 100, 100));
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                gr.fillOval(e.getX(),e.getY(),20,20);
//                x = e.getX();
//                y = e.getY();
                repaint();
            }
        });
    }

    public void paint(Graphics g) {
//        g.setColor(Color.red);
//        g.fillOval(x, y, 20, 20);
        if(img==null){
            img=createImage(getWidth(),getHeight());
            gr=img.getGraphics();
            gr.setColor(Color.red);
        }
        g.drawImage(img,0,0,null);
    }

    public void update(Graphics g){
        paint(g);
    }

    public static void main(String[] args) {
        Frame f = new Frame("Paint");
        f.setBounds(50, 50, 300, 200);
        f.add(new MyCanvas(), BorderLayout.CENTER);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        f.setVisible(true);
    }
}
