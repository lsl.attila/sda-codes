import org.w3c.dom.ls.LSOutput;

public class StringOnline {
    public static void main(String[] args) {
//        System.out.println("Exercitiul 1");
//        System.out.println("Enter your first name:");
//        Scanner user=new Scanner(System.in);
//        String s=user.nextLine();
//        hi(s);
//        System.out.println("Enter your second name:");
//        String lastName=user.nextLine();
//        hi(lastName);

        System.out.println("Exercitiul 3");
        String s1 = "aerisirea";
        String s2 = "Ana";
        cauta(s1, s2);
        cauta1(s1, s2);

        System.out.println("Exercitiul 4");
        numara(s1);

        System.out.println("Exercitiul 12");
        isPalindrom(s1);
    }


    public static void isPalindrom(String s) {
        String s1 = "";
        for (char c:s.toCharArray()) {
            s1+=c;
        }
        System.out.println(s1.equals(s));
    }

    public static void cauta(String s1, String s2) {
        if (s1.indexOf(s2) != -1) {
            System.out.println("FOUND");
        } else {
            System.out.println("NOT FOUND");
        }
    }

    public static void cauta1(String s1, String s2) {
        System.out.println(s1.contains(s2));
    }

    public static boolean isVocalaSwitch(char c) {
        switch (c) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                return true;
            default:
                return false;
        }
    }

    public static boolean isVocala(char c) {
        boolean rez = false;
        char[] vocale = {'a', 'e', 'i', 'o', 'u'};
        for (int i = 0; i < vocale.length; i++) {
            if (c == vocale[i]) {
                return true;
            }
        }
        return false;
    }


    public static void numara(String s) {
        int litere = 0;
        int cifre = 0;
        int caractereSpeciale = 0;
        int vocale = 0;
        int consoane = 0;
        s = s.toLowerCase();
        System.out.println("Caractere: " + s.length());
        for (int i = 0; i < s.length(); i++) {
            if (Character.isLetter(s.charAt(i))) {
                litere++;
                if (isVocalaSwitch(s.charAt(i))) {
                    vocale++;
                }
            }
            if (Character.isDigit(s.charAt(i))) {
                cifre++;
            }
        }
        System.out.println("Litere: " + litere);
        System.out.println("Cifre " + cifre);
        System.out.println("Vocale " + vocale);
        consoane = litere - vocale;
        System.out.println("Consoane " + consoane);
    }
}
