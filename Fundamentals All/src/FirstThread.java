import java.io.PipedWriter;
import java.io.PipedReader;
import java.io.PrintWriter;
import java.util.Date;
public class FirstThread extends Thread {
    private PipedWriter pipewrite;
    private PipedReader piperead;
    public FirstThread (String name){
        super(name);
        try{
            pipewrite=new PipedWriter();
            piperead=new PipedReader(pipewrite);
        }catch (java.io.IOException e){}
    }
    public PipedReader getReader(){
        return piperead;
    }
    public void run(){
        PrintWriter ptw=new PrintWriter(pipewrite);
        while(true){
            String s=(new Date()).toString();
            ptw.println(s);
            System.out.println(getName() + " : " + s + " has been written in pipe");
            try{
                sleep(1000);
            }catch (InterruptedException e){}
        }
    }

}
