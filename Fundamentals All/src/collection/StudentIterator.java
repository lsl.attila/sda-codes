package collection;

import core.Student;

public interface StudentIterator {
    boolean hasMoreElements();
    Student nextElement();
}

