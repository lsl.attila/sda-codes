package collection;

import core.Person;
import core.Student;

import java.util.Scanner;

public class StudentList {
    int currentSize;
    Student[] students;

    public StudentList(int size) {
        this.currentSize = 0;
        this.students = new Student[size];

    }

    public void addStudent(Student s) {
        this.students[currentSize] = s;
        currentSize++;
    }

    public StudentIterator getIterator() {
        return new StudentIteratorImpl();
    }

    public StudentIterator getReverseIterator(){
        return new ReverseStudentIteratorImpl();
    }

    public class StudentIteratorImpl implements StudentIterator {
        private int index = 0;

        @Override
        public boolean hasMoreElements() {
            return index == currentSize;
        }

        @Override
        public Student nextElement() {
            Student s = students[index];
            index++;

            return s;
        }
    }


    public class ReverseStudentIteratorImpl implements StudentIterator{
        int index=currentSize;
        @Override
        public boolean hasMoreElements() {
            return index>0;
        }

        @Override
        public Student nextElement() {
            Student s = students[index-1];
            index--;

            return s;

        }
    }


}

