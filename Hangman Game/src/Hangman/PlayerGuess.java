package Hangman;

import GameGenerics.PlayerAction;

public class PlayerGuess extends PlayerAction {
    private String guess;
    public PlayerGuess(String guess) {
        this.guess = guess;
    }
    public String toString() {
        return guess;
    }
    public boolean equals(Object word) {
        return word.equals(guess);
    }
}
