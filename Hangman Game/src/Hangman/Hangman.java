package Hangman;

import GameGenerics.GameEngine;
import GameGenerics.GameResponse;
import GameGenerics.Player;
import GameGenerics.PlayerAction;

import java.util.Random;

public class Hangman extends GameEngine {
    private boolean over = false;
    private String[] words;
    private HiddenWord word;
    private Player winner;
    public Hangman(String[] words) {
        super(new HangmanMap());
        this.words = words;
    }

    @Override
    public boolean gameOver() {
        return over;
    }

    private boolean guessWord(String guess) {
        if(guess.equals(word.toString())) {
            winner = players.get(0);
            over = true;
            word.reveal();
        }
        return over;
    }

    @Override
    public GameResponse performAction(PlayerAction a) {
        String guess = a.toString();
        //Enable the following line for cheats
        //System.out.println("Testing " + guess + "<>" + word);
        if(guessWord(guess)) {
            return new BooleanResponse(true);
        }

        if(!word.tryGuess(guess)) {
            if(!((HangmanMap)map).hangMore()) {
                winner = players.get(1);
                over = true;
                return new BooleanResponse(true);
            }
        }

        if(word.isDone()) {
            winner = players.get(0);
            over = true;
            return new BooleanResponse(true);
        }

        return null;
    }

    @Override
    public void setup() {
        Random generator = new Random();
        String generatedWord = words[generator.nextInt(words.length)];
        word = new HiddenWord(generatedWord);
        ((HangmanMap)map).setHidden(word);
        addPlayer(new HumanPlayer(this));
        addPlayer(new ComputerPlayer(this));
    }

    @Override
    public void finish() {
        map.draw();
        System.out.println("Winner is: " + winner);
    }
}
