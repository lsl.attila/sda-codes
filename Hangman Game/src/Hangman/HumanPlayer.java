package Hangman;

import GameGenerics.Player;
import GameGenerics.GameReference;
import java.util.Scanner;

public class HumanPlayer extends Player {

    private Scanner input;
    private String name;

    public HumanPlayer(GameReference game) {
        super(game);
        input = new Scanner(System.in);
        System.out.println("What's your name?");
        name = input.nextLine();
    }
    @Override
    public void act() {
        System.out.println("My guess is: ");
        game.performAction(new PlayerGuess(input.nextLine()));
    }

    public String toString() {
        return name;
    }
}
