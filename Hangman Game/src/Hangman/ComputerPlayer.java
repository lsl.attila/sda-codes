package Hangman;

import GameGenerics.Player;
import GameGenerics.GameReference;

import java.util.Scanner;

public class ComputerPlayer extends Player {

    public ComputerPlayer(GameReference game) {
        super(game);
    }

    @Override
    public void act() {}

    public String toString() {
        return "Hangman";
    }
}
