package Hangman;

import java.util.ArrayList;

public class HiddenWord {
    private ArrayList<Character> letters;
    private String word;
    private int remainingLetters;

    public HiddenWord(String word) {
        this.word = word;
        remainingLetters = word.length();
        letters = new ArrayList<Character>();
        for(int i = 0; i < word.length(); ++i) {
            letters.add('_');
        }
    }

    public void print() {
        System.out.print("\n\nHidden Word:   ");
        for(Character letter : letters) {
            System.out.print(letter);
        }
        System.out.println("");
    }

    public boolean tryGuess(String guess) {
        if(guess.length() != 1) {
            return false;
        }
        if(!word.contains(guess)) {
            return false;
        }
        for(int i = 0; i < letters.size(); ++i) {
            char letter = guess.charAt(0);
            if(letter == word.charAt(i) &&
            letters.get(i) == '_') {
                letters.remove(i);
                letters.add(i, letter);
                remainingLetters--;
            }
        }
        return true;
    }

    public boolean isDone() {
        return (remainingLetters == 0);
    }

    public void reveal() {
        for(int i = 0; i < word.length(); ++i) {
            letters.remove(i);
            letters.add(i, word.charAt(i));
        }
    }

    public String toString() {
        return word;
    }

}
