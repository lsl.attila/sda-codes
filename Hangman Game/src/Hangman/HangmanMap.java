package Hangman;

import GameGenerics.MapReference;

import java.util.ArrayList;

public class HangmanMap implements MapReference {
    private String hanger;
    private String empty;
    private String noose;
    private String head;
    private String dead_head;
    private String neck;
    private String body;
    private String legs;
    private ArrayList<String> output;
    private ArrayList<String> victim;
    private HiddenWord hidden;
    private int step;

    public HangmanMap() {
        step = 0;
        hidden = null;
        hanger =    "|||||||||\n" +
                    "=========\n";
        empty =     "        \n" +
                    "        \n";
        noose =     "   ||   \n" +
                    "   ||   \n";
        head =      "--------\n" +
                    "|.   . |\n" +
                    "|  _   |\n" +
                    "--------\n";
        dead_head = "--------\n" +
                    "|X   X |\n" +
                    "|  o   |\n" +
                    "--------\n";
        neck =      "   ||   \n";
        body =      "|------|\n" +
                    "| |  | |\n" +
                    "| |  | |\n" +
                    "  |  |  \n" +
                    "  |--|  \n";
        legs =      "   ||   \n" +
                    "   ||   \n" +
                    "   ||   \n";
        victim = new ArrayList<String>();
        victim.add(head);
        victim.add(neck);
        victim.add(body);
        victim.add(legs);
        output = new ArrayList<String>();
        output.add(hanger);
        output.add(empty);
    }

    public void draw() {
        System.out.println("================================");
        System.out.println("================================");
        System.out.println("================================");
        System.out.println("================================");
        for(String e : output) {
            System.out.print(e);
        }
        if(hidden != null) {
            hidden.print();
        }
        System.out.println("\n\n");
    }

    public boolean hangMore() {
        if(step < victim.size()) {
            output.add(victim.get(step));
            step++;
        } else if(output.contains(empty)) {
            output.remove(empty);
            output.add(1, noose);
        } else {
            if(output.contains(head)) {
                output.remove(head);
                output.add(2, dead_head);
            }
            return false;
        }
        return true;
    }
    public void setHidden(HiddenWord word) {
        hidden = word;
    }
}
