package Hangman;

import GameGenerics.GameResponse;

public class BooleanResponse extends GameResponse {
    public boolean value;
    public BooleanResponse(boolean value) {
        this.value = value;
    }
}
