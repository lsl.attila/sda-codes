import Hangman.Hangman;

public class Main {

    public static void main(String[] args) {
        Hangman game = new Hangman(new String[]{
                "temperament", "neologism", "spectru", "negru",
                "masina", "caine", "vanatoare"
        });
        game.play();
    }
}
