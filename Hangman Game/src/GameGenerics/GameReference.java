package GameGenerics;

public interface GameReference {
    public boolean gameOver();
    public GameResponse performAction(PlayerAction a);
    public void setup();
    public void finish();
}
