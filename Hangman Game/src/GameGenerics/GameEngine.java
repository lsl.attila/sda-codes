package GameGenerics;

import java.util.ArrayList;

public abstract class GameEngine implements GameReference {
    protected ArrayList<Player> players;
    protected MapReference map;

    public GameEngine(MapReference map) {
        players = new ArrayList<Player>();
        this.map = map;
    }

    public void play() {
        setup();
        while(!gameOver()) {
            map.draw();
            for(Player p : players) {
                p.act();
            }
        }
        finish();
    }

    public void addPlayer(Player p) {
        players.add(p);
    }

    public void removePlayer(Player p) {
        players.remove(p);
    }
}
