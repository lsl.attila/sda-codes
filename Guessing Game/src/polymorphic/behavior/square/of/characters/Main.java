package polymorphic.behavior.square.of.characters;

import java.io.PrintWriter;

interface PrintChar{
    public void print();
}

class QuestionMarkChar implements PrintChar{
    @Override
    public void print() {
        System.out.print("?");
    }
}

class ExclamationMarkChar implements PrintChar{
    @Override
    public void print() {
        System.out.print("!");
    }
}

public class Main {
    public static void main(String[] args) {
        PrintChar[][] map=new PrintChar[20][];
        for(int i=0;i<map.length;i++){
            map[i]=new PrintChar[20];
            for (int j = 0; j <map.length ; ++j) {
                if(i>10){
                    map[i][j]=new QuestionMarkChar();
                }
                else {
                    map[i][j]=new ExclamationMarkChar();
                }
            }
        }

        for (PrintChar[] line:map){
            for(PrintChar el:line){
                el.print();
            }
            System.out.println("");
        }

    }
}
