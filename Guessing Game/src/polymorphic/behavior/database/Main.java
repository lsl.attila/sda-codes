package polymorphic.behavior.database;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> inputs = new ArrayList<Integer>();
//        inputs.add(2);
//        inputs.add(12);
//        inputs.add(14);
//        System.out.println("List before adding numbers"+inputs);
        int sum = 0;
        System.out.println("Please enter the inputs");
        while (scanner.hasNextDouble()) {
            inputs.add(scanner.nextInt());
            if (scanner.equals("done")) {
                if (inputs.isEmpty()) {
                    System.out.println("Your list is empty");
                    break;
                }
            }
        }

        for (double i : inputs) {
            sum += i;
        }
        System.out.println(sum);
        System.out.println(inputs);
    }
}
