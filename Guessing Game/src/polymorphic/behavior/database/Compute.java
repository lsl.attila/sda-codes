package polymorphic.behavior.database;

import java.util.Scanner;

public interface Compute {
    public void compute();
}
