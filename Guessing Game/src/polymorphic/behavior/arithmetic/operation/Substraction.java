package polymorphic.behavior.arithmetic.operation;

public class Substraction implements Operation {
    @Override
    public void compute(int first, int second) {
        System.out.println(first + "-" + second + "=" + (first - second));
    }
}
