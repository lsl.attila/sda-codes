package polymorphic.behavior.arithmetic.operation;

import java.util.Scanner;

public interface Operation {
    public void compute(int first, int second);
}
