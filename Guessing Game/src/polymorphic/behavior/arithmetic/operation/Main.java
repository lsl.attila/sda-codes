package polymorphic.behavior.arithmetic.operation;

import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Exercitiul 1");
        System.out.println("Please enter your name and age!");
        String name = scanner.nextLine();
        int age = scanner.nextInt();
        System.out.println("Your name: " + name + " and age: " + age);
        System.out.println("Exercitiul 2");
        int operation;
        System.out.println("Please enter first number");
        int num1 = scanner.nextInt();
        System.out.println("Please enter second number");
        int num2 = scanner.nextInt();
        System.out.println("Please choose one operation: \n" +
                "1.Addition\n" +
                "2.Substraction\n" +
                "3.Multiplication\n" +
                "4.Divison\n"+
                "5.Modulo\n");
        try {
            operation = scanner.nextInt();
            switch (operation) {
                case 1:
                    System.out.println("Sum of the two numbers = "+(num1+num2));
                    break;
                case 2:
                    System.out.println("Substraction of the two numbers = "+(num1-num2));
                    break;
                case 3:
                    System.out.println("Multiplication of the two numbers = "+(num1*num2));
                    break;
                case 4:
                    System.out.println("The divison of the two numbers = "+(num1/num2));
                    break;
                case 5:
                    System.out.println("The modulo of the two numbers = "+(num1%num2));
                    break;
                default:
                    System.out.println("Please choose a number between [1-5]");
            }
        } catch (InvalidParameterException e) {
            System.out.println("Invalid number");
        } catch (Exception e) {
            System.out.println("Choose a number between [1-5]");
        }
        System.out.println("Exercitiul 3");
        System.out.println("Please enter circle radius");
        double radius=scanner.nextDouble();
        System.out.println("The circumference of the circle is: "+(2*radius*Math.PI));
        System.out.println("The area of the circle is: "+(radius*radius*Math.PI));
        System.out.println("Exercitiul 4");
        System.out.println("Enter width of rectangle: ");
        double width=scanner.nextDouble();
        System.out.println("Enter length of rectangle: ");
        double length=scanner.nextDouble();
        System.out.println("Perimeter of rectangle: "+(2*(width+length)));
        System.out.println("Area of rectangle: "+(width*length));
        System.out.println("Exercitiul 5");
        double distance=0;
        System.out.println("Please enter the coordinates of the first point");
        double x1=scanner.nextDouble();
        double y1=scanner.nextDouble();
        System.out.println("Please enter the coordinates of the second point");
        double x2=scanner.nextDouble();
        double y2=scanner.nextDouble();
        distance=Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
        System.out.println("The distance between the two planes is: "+distance);
        System.out.println("Exercitiul 6");
        int counter=0;
        double input=0;
        double sum=0;
        System.out.println("Please enter a series of values (enter string to quit): ");
        while(scanner.hasNextDouble()){
            input=scanner.nextDouble();
            sum+=input;
            counter++;
        }
        System.out.println("The sum of the entered numbers is: "+sum);
        System.out.println("The avarage of the entered numbers is: "+(sum/counter));
        System.out.println("A total of "+counter+"numbers have been entered");
        System.out.println("Exercitiul 7");
        System.out.println("Please enter the first time (ex: 03:15:22): ");
        String time1 = scanner.nextLine();
        System.out.println("Please enter the second time (ex: 03:15:22): ");
        String time2 = scanner.nextLine();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Date date1 = format.parse(time1);
        Date date2 = format.parse(time2);
        long difference = date2.getTime() - date1.getTime();
        System.out.println("The difference between the two times(in seconds) is: " + difference);
        System.out.println("The difference between the two times(in minutes) is: " + difference/60);
        System.out.println("The difference between the two times(in hours) is: " + difference/3600);
        System.out.println("Exercitiul 8");
        System.out.println("Please enter the temperature in Fahrenheit: ");
        double temperatureFahrenheit=scanner.nextDouble();
        System.out.println("The temperature in Celsius is: "+((temperatureFahrenheit-32)/1.8));

    }
}
