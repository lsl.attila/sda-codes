package polymorphic.behavior.arithmetic.operation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberOperations {
    private int firstNumber;
    private int secondNumber;
    private static final Pattern OPERATION_PATTERN = Pattern.compile("(([0-9][0-9]*)+([\\/\\+\\-\\*])+([0-9][0-9]*))");
    public static boolean validateOperation(String operation) {
        Matcher matcher = OPERATION_PATTERN.matcher(operation);
        return matcher.matches();
    }
    public void saveNumbers(String operation, int index) {
        String firstNumberString = operation.substring(0, index);
        String secondNumberString = operation.substring(index+1, operation.length());
        this.firstNumber = Integer.parseInt(firstNumberString);
        this.secondNumber = Integer.parseInt(secondNumberString);
    }
    public int getFirstNumber() {
        return firstNumber;
    }
    public int getSecondNumber() {
        return secondNumber;
    }
}
