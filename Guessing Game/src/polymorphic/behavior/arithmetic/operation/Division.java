package polymorphic.behavior.arithmetic.operation;

public class Division implements Operation {
    @Override
    public void compute(int first, int second) {
        if (second == 0) {
            System.out.println("Invalid operation");
        } else {
            System.out.println(first + "/" + second + "=" + (first / second) + " + " + (first % second));
        }
    }
}
