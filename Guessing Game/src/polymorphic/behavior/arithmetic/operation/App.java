package polymorphic.behavior.arithmetic.operation;

import java.util.Scanner;

import static polymorphic.behavior.arithmetic.operation.NumberOperations.validateOperation;

public class App {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (true) {
            String operation = scan.nextLine();
            if (operation.equals("exit")) {
                break;
            }
            if (validateOperation(operation)) {
                NumberOperations no = new NumberOperations();
                if (operation.contains("+")) {
                    int saveIndex = operation.indexOf("+");
                    no.saveNumbers(operation, saveIndex);
                    Addition add = new Addition();
                    add.compute(no.getFirstNumber(), no.getSecondNumber());
                }else if(operation.contains("-")){
                    int saveIndex = operation.indexOf("-");
                    no.saveNumbers(operation, saveIndex);
                    Substraction sub=new Substraction();
                    sub.compute(no.getFirstNumber(), no.getSecondNumber());
                }else if(operation.contains("*")){
                    int saveIndex = operation.indexOf("*");
                    no.saveNumbers(operation, saveIndex);
                    Multiplication mul=new Multiplication();
                    mul.compute(no.getFirstNumber(), no.getSecondNumber());
                }else if(operation.contains("/")){
                    no.saveNumbers(operation, operation.indexOf("/"));
                    Division div=new Division();
                    div.compute(no.getFirstNumber(), no.getSecondNumber());
                }
            }else{
                System.out.println("Invalid operation");
            }
        }
    }
}
