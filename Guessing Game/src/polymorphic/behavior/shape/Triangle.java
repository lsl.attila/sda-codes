package polymorphic.behavior.shape;

import java.security.InvalidParameterException;

public class Triangle implements Shape{

    private double a;
    private double b;
    private double c;

    @Override
    public double area() {
        double semiPerimeter=(a+b+c)/2;
        double area= semiPerimeter*(semiPerimeter-a)*(semiPerimeter-b)*(semiPerimeter-c);
        if(area<0){
            return Math.sqrt(area*(-1));
        }
        return Math.sqrt(area);
    }

    @Override
    public double perimeter() {
        return a+b+c;
    }

    @Override
    public void userInput() {
        try {
            System.out.println("The 3 lengths of the triangle are: \na=");
            a=scanner.nextDouble();
            System.out.println("b=");
            b=scanner.nextDouble();
            System.out.println("c=");
            c=scanner.nextDouble();
        }catch(InvalidParameterException e){
            System.out.println("Invalid input!");
        }catch(Exception e){
            System.out.println("Exception");
        }
    }
}
