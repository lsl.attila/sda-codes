package polymorphic.behavior.shape;

import java.security.InvalidParameterException;

public class Rectangle implements Shape {
    private double rectangleWidth;
    private double rectangleLength;

    @Override
    public double area() {
        return rectangleWidth * rectangleLength;
    }

    @Override
    public double perimeter() {
        return 2 * (rectangleWidth + rectangleLength);
    }

    @Override
    public void userInput() {
        System.out.println("Please enter the rectangle's width and length: ");
        try {
            rectangleWidth = scanner.nextDouble();
            rectangleLength = scanner.nextDouble();
        } catch (InvalidParameterException e) {
            System.out.println("Invalid input for width");
        } catch (Exception e) {
            System.out.println("Exception");
        }
    }
}
