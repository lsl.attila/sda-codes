package polymorphic.behavior.shape;

import java.security.InvalidParameterException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int shape;
        System.out.println("Please choose one shape: \n" +
                "1.Circle\n" +
                "2.Square\n" +
                "3.Rectangle\n" +
                "4.Triangle\n");

        try {
            shape = scanner.nextInt();
            switch (shape) {
                case 1:
                    Circle circle1 = new Circle();
                    circle1.userInput();
                    System.out.println("The area of the circle is: " + circle1.area());
                    System.out.println("The perimeter of the circle is: " + circle1.perimeter());
                    break;
                case 2:
                    Square square1 = new Square();
                    square1.userInput();
                    System.out.println("The area of the square is: " + square1.area());
                    System.out.println("The perimeter of the circle is: " + square1.perimeter());
                    break;
                case 3:
                    Rectangle rectangle1 = new Rectangle();
                    rectangle1.userInput();
                    System.out.println("The area of the rectangle is: " + rectangle1.area());
                    System.out.println("The perimeter of the rectangle is: " + rectangle1.perimeter());
                    break;
                case 4:
                    Triangle triangle1 = new Triangle();
                    triangle1.userInput();
                    System.out.println("The area of the rectangle is: " + triangle1.area());
                    System.out.println("The perimeter of the rectangle is: " + triangle1.perimeter());
                    break;
                default:
                    System.out.println("Please choose a number between [1-4]");
            }
        } catch (InvalidParameterException e) {
            System.out.println("Invalid number");
        } catch (Exception e) {
            System.out.println("Choose a number between [1-4]");

        }

    }
}
