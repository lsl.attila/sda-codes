package polymorphic.behavior.shape;

import java.security.InvalidParameterException;

public class Circle implements Shape {
    private double radius;

    @Override
    public double area() {
        return radius * radius * Math.PI;
    }

    @Override
    public double perimeter() {
        return 2 * radius * Math.PI;
    }

    @Override
    public void userInput() {
        System.out.println("Please enter the circle's radius: ");
        try {
            radius = scanner.nextDouble();
        } catch (InvalidParameterException e) {
            System.out.println("Invalid input!");
        } catch (Exception e) {
            System.out.println("Exception");

        }
    }
}
