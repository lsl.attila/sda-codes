package polymorphic.behavior.shape;

import java.security.InvalidParameterException;

public class Square implements Shape {
    private double squareSide;

    @Override
    public double area() {
        return squareSide*squareSide;
    }

    @Override
    public double perimeter() {
        return 4*squareSide;
    }

    @Override
    public void userInput() {
        System.out.println("Please enter the lenght of one side of the square");
        try{
            squareSide=scanner.nextDouble();
        }catch (InvalidParameterException e) {
            System.out.println("Invalid input!");
        } catch (Exception e) {
            System.out.println("Exception");

        }

    }
}
