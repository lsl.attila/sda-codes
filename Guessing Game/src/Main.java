import GuessingGame.GuessingGame;
import GuessingGame.GuessingMap;

public class Main {
    public static void main(String[] args) {
        GuessingGame joc =new GuessingGame(new GuessingMap());
        joc.play();
    }
}
