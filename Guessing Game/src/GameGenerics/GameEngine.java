package GameGenerics;

import java.util.ArrayList;

public abstract class GameEngine implements GameReference {
    protected ArrayList<Player> players;
    protected MapReference map;

    public GameEngine(MapReference map) {
        this.map = map;
        players = new ArrayList<Player>();
    }

    public void play() {
        setup();
        while(gameOver() == false) {
            map.draw();
            for(Player p : players) {
                p.act();
            }
        }
        finish();
    }
}
