package GameGenerics;

public interface GameReference {
    public void setup();
    public GameResponse performAction(PlayerAction a);
    public boolean gameOver();
    public void finish();
}
