package GuessingGame;

import GameGenerics.GameReference;
import GameGenerics.Player;

import java.util.Random;

public class ComputerPlayer extends Player {

    private int target;


    public ComputerPlayer(GameReference game) {
        super(game);
        Random generator = new Random();
        target = generator.nextInt(100);
    }

    @Override
    public void act() {
        int humanAttempt = ((PlayerGuess) game.performAction(new RequestGuess())).value;
        if (humanAttempt == target) {
            game.performAction(new GameFinishMessage());
        } else if (humanAttempt > target) {
            System.out.println("You missed. Shoot lower");
        } else {
            System.out.println("You missed. Shoot higher");
        }
    }
}
