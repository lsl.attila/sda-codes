package GuessingGame;

import GameGenerics.GameReference;
import GameGenerics.Player;

import java.util.Scanner;

public class HumanPlayer extends Player {
    private Scanner input;
    public HumanPlayer(GameReference game) {
        super(game);
        input = new Scanner(System.in);
    }

    @Override
    public void act() {
        game.performAction(new PlayerGuess(input.nextInt()));
    }
}