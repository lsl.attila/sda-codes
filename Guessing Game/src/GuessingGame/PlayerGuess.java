package GuessingGame;

import GameGenerics.GameResponse;
import GameGenerics.PlayerAction;

public class PlayerGuess implements GameResponse, PlayerAction {
    public int value;
    public PlayerGuess(int value){
        this.value=value;
    }
}
