package GuessingGame;

import GameGenerics.*;

public class GuessingGame extends GameEngine {

    int i = 3;
    private int playerGuess = -1;
    private boolean over = false;

    public GuessingGame(MapReference map) {
        super(map);
    }

    @Override
    public void setup() {
        players.add(new HumanPlayer(this));
        players.add(new ComputerPlayer(this));
    }

    @Override
    public GameResponse performAction(PlayerAction a) {
        if(a instanceof RequestGuess) {
            return new PlayerGuess(playerGuess);
        } else if(a instanceof GameFinishMessage) {
            over = true;
        } else if(a instanceof PlayerGuess) {
            playerGuess = ((PlayerGuess)a).value;
        }
        return null;
    }

    @Override
    public boolean gameOver() {
        return over;
    }

    @Override
    public void finish() {
        System.out.println("Bye!");
    }
}