import java.io.File;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

    Simulation simulation = new Simulation();

        File phase1File= new File("src/Phase1.txt");
        File phase2File= new File("src/Phase2.txt");

        List<Item> itemsForPhase1=simulation.loadItems(phase1File);
        List<Item> itemsForPhase2=simulation.loadItems(phase2File);

        List<Rocket> rocketsU1ForPhase1=simulation.loadU1Other(itemsForPhase1);
        List<Rocket> rocketsU1ForPhase2=simulation.loadU1Other(itemsForPhase2);

        System.out.println("The total cost for sending all U1 rockets for phase 1: "+simulation.runSimulation(rocketsU1ForPhase1));
        System.out.println("The total cost for sending all U1 rockets for phase 2: "+simulation.runSimulation(rocketsU1ForPhase2));

        List<Rocket> rocketsU2ForPhase1=simulation.loadU2(itemsForPhase1);
        List<Rocket> rocketsU2ForPhase2=simulation.loadU2(itemsForPhase2);

        System.out.println("The total cost for sending all U2 rockets for phase 1: "+simulation.runSimulation(rocketsU2ForPhase1));
        System.out.println("The total cost for sending all U2 rockets for phase 2: "+simulation.runSimulation(rocketsU2ForPhase2));




    }
}
