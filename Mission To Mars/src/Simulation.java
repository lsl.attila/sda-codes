import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Simulation {

    public List<Item> loadItems(File file) {
        List<Item> itemList = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line = bufferedReader.readLine();
            while (line != null) {
                itemList.add(createItem(line));
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return itemList;
    }

    private Item createItem(String line) {
        String[] elements = line.split("=");
        return new Item(elements[0], Integer.parseInt(elements[1]));
    }

    public List<Rocket> loadU1(List<Item> items) {
        List<Rocket> rockets = new ArrayList<>();

        Rocket u1 = new U1();
        for (Item item : items) {
            boolean added = false;
            while (!added) {
                if (u1.canCarry(item)) {
                    u1.carry(item);
                    added = true;
                } else {
                    rockets.add(u1);
                    u1 = new U1();
                }
            }
        }
        rockets.add(u1);
        return rockets;
    }

    public List<Rocket> loadU1Other(List<Item> items) {
        List<Rocket> rockets = new ArrayList<>();

        Rocket u1 = new U1();
        for (Item item : items) {
            if (u1.canCarry(item)) {
                u1.carry(item);
            } else {
                rockets.add(u1);
                u1 = new U1();
                u1.carry(item);
            }
        }

        return rockets;
    }

    public List<Rocket> loadU2(List<Item> items) {
        List<Rocket> rockets = new ArrayList<>();

        U2 u2 = new U2();
        for (Item item : items) {
            if (u2.canCarry(item)) {
                u2.carry(item);
            } else {
                rockets.add(u2);
                u2 = new U2();
                u2.carry(item);
            }
        }

        return rockets;
    }

    public long runSimulation(List<Rocket> rockets) {
        long simulationCost = 0;

        for (Rocket rocket : rockets) {
            simulationCost = simulationCost + sendRocket(rocket);
        }

        return simulationCost;
    }

    private long sendRocket(Rocket rocket){
        if (rocket.launch() && rocket.land()){
            return rocket.getCost();
        } else {
            System.out.println("crashed");
            return rocket.getCost() + sendRocket(rocket);
        }
    }



}
