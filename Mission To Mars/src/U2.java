public class U2 extends Rocket {

    public U2() {
        super(120, 18000, 29000);
    }

    @Override
    public boolean launch() {
        double chance = 0.04 * ((double) getWeight() / getMaxWeight());
        double random = Math.random() / 10;
        return random >= chance;
    }

    @Override
    public boolean land() {
        double chance = 0.08 * ((double) getWeight() / getMaxWeight());
        double number = Math.random() / 10;
        return number >= chance;
    }

}
