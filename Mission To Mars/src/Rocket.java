public class Rocket implements SpaceShip {

    private int cost;
    private int weight;
    private int maxWeight;

    public Rocket(int cost, int weight, int maxWeight) {
        this.cost = cost;
        this.weight = weight;
        this.maxWeight = maxWeight;
    }

    @Override
    public boolean launch() {
        return true;
    }

    @Override
    public boolean land() {
        return true;
    }

    @Override
    public boolean canCarry(Item item) {
        return (weight + item.getWeight()) < maxWeight;
    }

    @Override
    public void carry(Item item) {
        weight += item.getWeight();
    }

    public int getCost() {
        return cost;
    }

    public int getWeight() {
        return weight;
    }

    public int getMaxWeight() {
        return maxWeight;
    }
}
