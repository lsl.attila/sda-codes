public class U1 extends Rocket {

    public U1() {
        super(100, 10000, 18000);
    }

    @Override
    public boolean land() {
        double number = Math.random() / 10;
        return number >= chanceOfLand();
    }

    private double chanceOfLand() {
        return 0.01 * ((double) getWeight() / getMaxWeight());
    }

    @Override
    public boolean launch() {
        double chance = 0.05 * ((double) getWeight() / getMaxWeight());
        double random = Math.random() / 10;
        return random >= chance;
    }

}
