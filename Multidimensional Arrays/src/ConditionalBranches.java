public class ConditionalBranches {
    public static void main(String[] args) {
        //Ana are mere cu while
        System.out.println("Ana are 1 mar");
        int nrMere = 2;
        while (nrMere <= 10) {
            System.out.println("Ana are " + nrMere + " mere");
            nrMere++;
        }

        //Ana are mere cu for
        System.out.println("Ana are 1 mar");
        for (int i = 2; i <= 10; i++) {
            System.out.println("Ana are " + i + " mere");
        }

        int age = 81;
        if (age < 18) {
            System.out.println("You're a teenager!");
        } else if (age > 80) {
            System.out.println("You're an old person!");
        } else {
            System.out.println("You're an adult!");
        }

        //Sa calculam suma numerelor pare de la 1 la 10
        int sum = 0;
        for (int nr = 1; nr <= 10; nr++) {
            if (nr % 2 == 0) {
                sum = sum + nr;
            }
        }
        System.out.println("Suma numerelor pare este:" + sum);

        //Sa calculam suma numerelor impare de la 1 la 10
        int sum1 = 0;
        for (int nr1 = 1; nr1 <= 10; nr1++) {
            if (nr1 % 2 == 1) {
                sum1 = sum1 + nr1;
            }
        }
        System.out.println("Suma numerelor pare este:" + sum1);

        if (false || true && false) {
            System.out.println("Intra in if");
        }

        if (age > 40 || false && false) {
            System.out.println("Intra in if");
        }

        //Sa se afiseze numerele dintre 5 si 15 pare folosind un for

        for (int variabila = 1; variabila <= 20; variabila++) {
            if (variabila > 5 && variabila < 15 && variabila % 2 == 0) {
                System.out.println(variabila);
            }
        }

        //Intre 0-5 si 15-20 impare

        for (int variabila1 = 1; variabila1 <= 20; variabila1++) {
            if ((variabila1 > 0 && variabila1 < 5 || variabila1 > 15 && variabila1 < 20) && variabila1 % 2 == 1) {
                System.out.println(variabila1);
            }
        }

        //Sa se afiseze numerel intre 0-20 pare
        for (int ab = 1; ab <= 20; ab++) {
            if (ab % 2 == 1) {
                continue;
            }
            System.out.println(ab);
        }
        System.out.println("Mesaj incepe problema");
        //Folosind un singur while sa se afiseze numerele pare si suma numerelor impare de la 0-10
        int suma1 = 0;
        int abc = 1;
        while (abc <= 10) {
            if (abc % 2 == 0 && (abc != 4 && abc != 6)) {
                System.out.println(abc);
            }

            if (abc % 2 == 1 && abc > 5) {
                suma1 = suma1 + abc;
            }
            abc++;
        }
        System.out.println(suma1);


        //Tema1: Sa se calculeze produsul numerelor impare dintre 3 si 8 si suma numerelor pare dintre 5 si 15
        //Cerinte de mai sus sa le implementati folosind while
        System.out.println("Tema 1");
        int nrTema1 = 1;
        int prod = 1;
        int suma2 = 0;
        while (nrTema1 < 15) {
            if (nrTema1 % 2 == 1 && nrTema1 < 8) {
                prod = prod * nrTema1;
            } else if (nrTema1 % 2 == 0 && nrTema1 < 15) {
                suma2 = suma2 + nrTema1;
            }
            nrTema1++;

        }
        System.out.print("Produsul numerelor impare dintre 3 si 8 este: ");
        System.out.println(prod);
        System.out.print("Suma numerelor pare dintre 5 si 15 este: ");
        System.out.println(suma2);

        System.out.println("Tema 1 cu for ");//fara 3 in produs
        int prodFor = 1;
        int sumaFor = 0;
        for (int nrTema1For = 4; nrTema1For <= 15; nrTema1For++) {
            if (nrTema1For % 2 == 1 && nrTema1For < 8) {
                prodFor = prodFor * nrTema1For;
            } else if (nrTema1For % 2 == 0 && nrTema1For > 5) {
                sumaFor = sumaFor + nrTema1For;
            }
        }
        System.out.print("Prod cu for: ");
        System.out.println(prodFor);
        System.out.print("Suma cu for: ");
        System.out.println(sumaFor);

        System.out.println("Tema1 cu do while");
        prod = 1;
        suma1 = 0;
        nrTema1 = 4;
        do {
            if (nrTema1 % 2 == 1 && nrTema1 < 8) {
                prod = prod * nrTema1;
            } else if (nrTema1 % 2 == 0 && nrTema1 > 5) {
                suma1 = suma1 + nrTema1;
            }
            nrTema1++;

        } while (nrTema1 < 15);
        System.out.println("Prod cu do while: " + prod);
        System.out.println("Suma cu do while: " + suma1);


        //Tema2: Sa se afiseze numerele impare dintre 10 si -10 in ordine descrescatoare
        //Cerinte de mai sus sa le implementati folosind do while
        System.out.println("Tema 2 cu for ");
        for (int nrTema2For = 9; nrTema2For > -10; nrTema2For--) {
            if (nrTema2For % 2 == 1 || nrTema2For % 2 == -1) {
                System.out.println(nrTema2For);
            }
        }

        System.out.println("Tema 2 cu do while");
        int nrTema2 = 9;
        do {
            if (nrTema2 % 2 == 1 || nrTema2 % 2 == -1) {
                System.out.println(nrTema2);
            }
            nrTema2--;
        } while (nrTema2 > -10);

        System.out.println("Tema 2 cu while");
        nrTema2 = 9;
        while (nrTema2 > -10) {
            if (nrTema2 % 2 == 1 || nrTema2 % 2 == -1) {
                System.out.println(nrTema2);
            }
            nrTema2--;
        }


        //Tema3: Sa se calculeze suma numerelor divizibile cu 3 de la 10 pana la -8 ?in ordine descrescatoare? folosind o singura instructiune while
        //Cerinte de mai sus sa le implementati folosind for
        System.out.println("Tema 3");
        int suma3 = 0;
        for (int nrTema3 = -8; nrTema3 < 10; nrTema3++) {
            if (nrTema3 % 3 == 0) {
                suma3 = suma3 + nrTema3;
            }
        }
        System.out.print("Suma numerelor divizibile cu 3 intre 10 si -8 cu while este: ");
        System.out.println(suma3);

        suma3 = 0;
        int nrTema3While = -8;
        while (nrTema3While < 10) {
            if (nrTema3While % 3 == 0) {
                suma3 = suma3 + nrTema3While;
            }
            nrTema3While++;
        }
        System.out.println("Suma numerelor divizibile cu 3 intre 10 si -8 cu do while este: " + suma3);

        suma3 = 0;
        nrTema3While = -8;
        do {
            if (nrTema3While % 3 == 0) {
                suma3 = suma3 + nrTema3While;
            }
            nrTema3While++;
        } while (nrTema3While < 10);
        System.out.println("Suma numerelor divizibile cu 3 intre 10 si -8 cu do while este: " + suma3);

        int n = 21320;
        sum = 0;
        while (n != 0) {
            if (n % 2 == 0) {
                sum = sum + n % 10;
            }
            n = n / 10;
        }
        System.out.println("Suma cifrelor pare este: " + sum);


    }
}
