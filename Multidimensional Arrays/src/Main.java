import org.w3c.dom.ls.LSOutput;

public class Main {
    public static void main(String [] args){
        byte varsta = 23; // variabila de tip byte
                        //este pe 8 biti
                        //valoare minima-128
                        //valoare maxima 127
        short inaltime = 180;//variabila de tip short, este pe 16 biti
        int primulNumar = -841;//32 biti, cel mai folosit
        long numarIntregDeTipLong = 123456789;//64 biti, formatul camel case, este formatul oficial folosit in java
        float numarCuVirgula = 142.124f;//numar cu virgula pe 32 de biti
        //pentru caractere folosim apostroafe
        double numarPe64Biti=123456;
        char caracter1 = 'a';
        char caracter2 ='4';
        char caracterSpecial='?';
        char caracterUnicode='\u004E';
        boolean intrerupator=false;
        boolean intrerupator1=true;
        //Primitive: byte, short, int, long, double, float, char, boolean

        System.out.println("Alex");
        System.out.println("Alex");
        System.out.print("Se afiseaza un octet:");
        System.out.println(varsta);
        System.out.print("Se afiseaza inaltimea:");
        System.out.println(inaltime);
        System.out.print("Se afiseaza primul numar:");
        System.out.println(primulNumar);
        System.out.print("Se afiseaza numarul intreg de tip long:");
        System.out.println(numarIntregDeTipLong);
        System.out.print("Se afiseaza numarul cu virgula:");
        System.out.println(numarCuVirgula);
        System.out.print("Se afiseaza double:");
        System.out.println(numarPe64Biti);
        System.out.print("Primul caracter:");
        System.out.println(caracter1);
        System.out.print("Al doilea caracter:");
        System.out.println(caracter2);
        System.out.print("Caracterul special:");
        System.out.println(caracterSpecial);
        System.out.print("Caracterul Unicode:");
        System.out.println(caracterUnicode);
        System.out.print("Intrerupator:");
        System.out.println(intrerupator);
        System.out.print("Intrerupator1:");
        System.out.println(intrerupator1);
        //afisam un numar
        System.out.println(145);
        //afisam un numar cu virgula
        System.out.println(145.23);
        //afisam un caracter
        System.out.println('G');

        //Reasignare
        varsta=25;
        System.out.println(varsta);
        inaltime=199;
        System.out.println(inaltime);
        primulNumar=-333;
        System.out.println(primulNumar);
        numarIntregDeTipLong=123457896;
        System.out.println(numarIntregDeTipLong);
        numarCuVirgula=136.79f;
        System.out.println(numarCuVirgula);
        caracter1='b';
        System.out.println(caracter1);

        //Probleme cu operatori
        //a. -5+8*6
        //b. (-5+8)*6
        //c. 20+(-3*5)/8 float si int
        int primulOperand=-5;
        int alDoileOperand=8;
        int alTreileaOperand=6;
        System.out.print("Rezultatul pt a:");
        System.out.println(primulOperand+(alDoileOperand*alTreileaOperand));
        System.out.print("Rezultatul pt b:");
        System.out.println((primulOperand+alDoileOperand)*alTreileaOperand);

        float nr1=20f;
        float nr2=-3f;
        float nr3=5f;
        float nr4=8f;
        System.out.print("Rezultatul pt c:");
        System.out.println(nr1+(nr2*nr3)/nr4);

        //%-modulo afiseaza restul si %10 putem sa obtinem ultima cifra din variabila
        //%2 ne ajuta la testarea paritatii unui numar
        //nr.1/nr.2 ne returneaza restul impartirii lui nr.1 cu nr.2
        System.out.print("Rezultatul pentru modulo:");
        System.out.println(nr1%nr2);

        //k++ k=k+1 postfixat, prima data se asigneaza valoarea lui k la c, apoi se implementeaza k
        int c=1;
        int k=2;
        c=k++;
        System.out.print("Rezultatul pt k++:");
        System.out.println(c=k++);

        int o=5;
        System.out.println(10+o);//adunare
        System.out.println("10"+o);//concatenare
        System.out.println("Alex"+o);//concatenare

        int qq=(int)7.3f;

        float exemplu=(float)qq;

        //String a="abc"; cea mai simpla modalitate de a implementa un string


        String sirCaractere="Alexlex     ";
        String sirCaractere2="Alin";
        System.out.println(sirCaractere.charAt(2));
        System.out.println(sirCaractere.compareTo(sirCaractere2));
        System.out.println(sirCaractere.endsWith("ex"));
        System.out.println(sirCaractere.equalsIgnoreCase("ALEX"));
        System.out.println(sirCaractere.equals("ALEX"));
        System.out.println(sirCaractere.indexOf("an"));
        System.out.println(sirCaractere.lastIndexOf("an"));
        System.out.println(sirCaractere.replace("ex","in"));
        System.out.println(sirCaractere.substring(3));
        System.out.println(sirCaractere.toLowerCase());
        System.out.println(sirCaractere.toUpperCase());
        System.out.println(sirCaractere.trim());

        //in memoria heap vor fi 3 obiecte
        String sir="Bogdan ";//primul obiect este bogdan
        System.out.println(sir);
        sir=sir+"Niculescu ";//al doilea obiect este bogdan niculescu
        System.out.println(sir);
        sir=sir+"Cristian ";//al treilea este bogdan niculescu cristian
        System.out.println(sir);

        StringBuilder sb=new StringBuilder("Bogdan ");//bogdan niculescu(dupa prima suprascriere) cristian(dupa al doilea suprascriere)
        sb.append("Niculescu ");//bogdan niculescu dupa care suprascrie primul
                sb.append("Cristian ");//bogdan niculescu cristian


        //Loops
        int a=4;
        int b=14;
        System.out.print("Testarea egalitatii:");
        System.out.println(a==b);
        System.out.print("Testarea egalitatii:");
        System.out.println(a<b);
        System.out.print("Testarea egalitatii:");
        System.out.println(a<=b);

        while (a<b){
            System.out.println("Atti");
            a++;
        }
        System.out.println("Este gata bucla");

        a=4;//trebuie sa reasignam dupa while, daca avem si un do while
        do{
            System.out.println("Laszlo");
            a++;
        }
        while(a<b);
        System.out.println("Este gata bucla");

        int primulNumarWhile=1;
        int alDoileaNumarWhile=10;
        while(primulNumarWhile<=alDoileaNumarWhile){
            System.out.println(primulNumarWhile);
            primulNumarWhile++;
        }
        System.out.println("Este gata bucla");

        int alTreileaNumarWhile=10;
        int alPatruleaNumarWhile=1;
        while (alTreileaNumarWhile>=alPatruleaNumarWhile){
            System.out.println(alTreileaNumarWhile);
            alTreileaNumarWhile--;
        }
        System.out.println("Este gata bucla");
        //cu do while in loc de while punem do si conditia while punem la sfarsit

        a=1;
        b=10;
        while(a<=b){
            System.out.println(a);
            a=a+2;
        }
        System.out.println("Este gata bucla");

        //Exercitii cu while si cu do while
        //Sa se afiseze folosind do - while numerele intre -10 si 10
        int nr1DoWhile=-10;
        do{
            System.out.println(nr1DoWhile);
            nr1DoWhile++;
        }while(nr1DoWhile<=10);
        System.out.println("Este gata bucla cu do while");
        //acelasi lucru de mai sus folosind while
        int nrDoWhile1=-10;
        while(nrDoWhile1<=10) {
            System.out.println(nrDoWhile1);
            nrDoWhile1++;
        }
        System.out.println("Este gata bucla cu while");



        //sa se afiseze folosind while din 3 in 3 numerele intre -10 si 5
        int nrWhile2=-10;
        while(nrWhile2<=5){
            System.out.println(nrWhile2);
            nrWhile2=nrWhile2+3;
        }
        System.out.println("Este gata bucla cu while");
        //acelasi lucru cu do while
        int nr1While=-10;
        do{
            System.out.println(nr1While);
            nr1While=nr1While+3;
        }while (nr1While<=5);
        System.out.println("Este gata bucla cu do while");


        //TEMA DE CASA: Acelasi lucru de mai sus dar descrescator!!!
        //Sa se afiseze folosind do - while si while numerele intre 10 si -10, descrescator
        //Sa se afiseze folosind do - while si while din 3 in 3 numerele intre 5 si -10, descrescator

        //Tema1:Sa se afizese folosind do while numerele intre 10 si -10, descrescator
        System.out.println("Tema1:");
        int nrTema1=10;
        do{
            System.out.println(nrTema1);
            nrTema1--;
        }while (nrTema1>=-10);

        //Tema2:Sa se afiseze folosind while numerele intre 10 si -10, descrescator
        System.out.println("Tema2:");
        int nrTema2=10;
        while(nrTema2>=-10){
            System.out.println(nrTema2);
            nrTema2--;
        }

        //Tema3:Sa se afiseze folosind do while din 3 in 3 numerele intre 5 si -10, descrescator
        System.out.println("Tema3:");
        int nrTema3=5;
        do{
            System.out.println(nrTema3);
            nrTema3=nrTema3-3;
        }while(nrTema3>=-10);

        //Tema4:Sa se afiseze folosind while din 3 in 3 numerele intre 5 si -10, descrescator
        System.out.println("Tema4:");
        int nrTema4=5;
        while(nrTema4>=-10){
            System.out.println(nrTema4);
            nrTema4=nrTema4-3;
        }

        //Sa se afiseze pe ecran de 10 ori urmatoarele mesaje:"Ana are 1 mar"
        //                                                    "Ana are 2 mere" pana la 10 mere
        //Prima metoda
        int nrMere1=1;
        int nrMere2=2;
        while(nrMere1<2){
            System.out.println("Ana are " +nrMere1+ " mar");
            nrMere1++;
        }
        while (nrMere2<=10){
            System.out.println("Ana are " +nrMere2+ " mere");
            nrMere2++;
        }
        //A doua metoda
        int nrMere3=1;
        System.out.println("Ana are " +nrMere3+ " mar");
        nrMere3++;
        while(nrMere3<=10){
            System.out.println("Ana are " +nrMere3+ " mere");
            nrMere3++;
        }

        //Ana are mere cu for crescator
        int nrMere=1;
        System.out.println("Ana are " +nrMere+ " mar");//putem sa afisem direct cu sout fara variabila nrMere
        for(int nrMere4=2;nrMere4<11;nrMere4++){
            System.out.println("Ana are " +nrMere4+ " mere");
        }

        //Ana are mere, descrescator
        for(int nrMere5=10;nrMere5>=1;nrMere5--){
            System.out.println("Ana are " +nrMere5+ " mere");
        }
        int nrMere6=1;
        System.out.println("Ana are " +nrMere6+ " mar");//putem sa afisam direct cu sout fara variabila nrMere5

        //Suma numerelor
        int sum=0;
        for(int sumaNumerelor=1;sumaNumerelor<=10;sumaNumerelor++){
            sum=sum+sumaNumerelor;
             }
        System.out.println("Suma numerelor este:" +sum);

        //Produsul numerelor
        int produs=1;
        for(int produs1=1;produs1<=10;produs1++){//daca vrem sa ne afiseze in doua in doua produs1=produs1+2
            produs=produs*produs1;
        }
        System.out.println("Produsul numerelor este: "+produs);

        //Suma numerelor cu while
        int sumaNrWhile=1;
        int sumaWhile=0;
        while(sumaNrWhile<=10){
            sumaWhile=sumaWhile+sumaNrWhile;
            sumaNrWhile++;
        }
        System.out.println("Suma numerelor este: "+sumaWhile);















        }

    }



