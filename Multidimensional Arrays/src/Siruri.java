public class Siruri {
    public static void main(String[] args) {
        int[] altVector = new int[]{1, -12, 3435, -315, -3, 5, 3, 6};
        //Sa se afiseze numerele pare de pe pozitii impare
        int[] primulSirDeNumere = new int[]{1, -2, 6, 8, -3, 6};
        //primulSirDeNumere[i]-elementul de pe pozitia i
        //primulSirDeNumere[3]=8
        for (int i = 0; i < primulSirDeNumere.length; i++) {
            if (primulSirDeNumere[i] % 2 == 0 && i % 2 == 1) {
                System.out.println(primulSirDeNumere[i]);
            }
        }

        //TEMA
        System.out.println("TEMA!!!!");
        System.out.println("Afisarea numerelor din vector cu while: ");
        int i = 0;

        while (altVector.length > i) {
            System.out.println(altVector[i]);
            i++;
        }

        System.out.println("Sa se numara cate numere pare negative si pare pozitive avem in vector");
        i = 0;
        int nrPareNegativ = 0;
        int nrImparePozitive = 0;
        for (i = 0; i < altVector.length; i++) {
            if (altVector[i] % 2 == 0 && altVector[i] < 0) {
                nrPareNegativ++;
            } else if (altVector[i] % 2 == 0 && altVector[i] > 0) {
                nrImparePozitive++;
            }
        }
        System.out.println("Numarul numerelor pare negative: " + nrPareNegativ);
        System.out.println("Numarul numerelor pare pozitive: " + nrImparePozitive);

        System.out.println("Sa se calculeze media elementelor din array ");
        int sum = 0;
        i = 0;
        for (i = 0; i < altVector.length; i++) {
            sum = sum + altVector[i];
        }
        System.out.println("Media elementelor: " + sum / altVector.length);

        System.out.println("Sa se afiseze pe ecran DA daca vectorul contine valoarea 5");
        for (i = 0; i < altVector.length; i++) {
            if (altVector[i] == 5) {
                System.out.println("Da, vectorul contine 5");
            }
        }

        System.out.println("Sa se afiseze pe ecran indexul elementului 5 din vector");
        for (i = 0; i < altVector.length; i++) {
            if (altVector[i] == 5) {
                System.out.println("Indexul elementului 5 din vector este: " + i);
            }
        }

        System.out.println("Sa se afiseze valoarea maxima din vector");
        int valoareaMax = altVector[0];
        for (i = 0; i < altVector.length; i++) {
            if (altVector[i] > valoareaMax) {
                valoareaMax = altVector[i];
            }
        }
        System.out.println("Valoarea maxima din vector este: " + valoareaMax);

        int valoareaMinima = altVector[0];
        for (i = 0; i < altVector.length; i++) {
            if (altVector[i] < valoareaMinima) {
                valoareaMinima = altVector[i];
            }
        }
        System.out.println("Valoarea minima din vector este: " + valoareaMinima);

        int[] altVector1 = new int[]{-100, -12, 3435, -3, 5, 3, 6};
        System.out.println("Exercitii dupa tema");
        int nrmin1 = Integer.MAX_VALUE;
        int nrmin2 = Integer.MAX_VALUE;
        for (i = 0; i < altVector1.length; i++) {
            if (altVector1[i] < nrmin1) {
                nrmin2 = nrmin1;
                nrmin1 = altVector1[i];
            } else if (altVector1[i] < nrmin2 && altVector1[i] != nrmin1) {
                nrmin2 = altVector1[i];

            }

        }
        System.out.println("Cel mai mic numar: " + nrmin1);
        System.out.println("Al doilea cel mai mic numar: " + nrmin2);

        //Sa se declare un array si al doilea array care sa contina inversul primului array
        int[] vector = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] vector1 = new int[vector.length];
        for (i = 0; i < vector.length; i++) {
            vector1[i] = vector[vector.length - i - 1];
        }
        for (i = 0; i < vector.length; i++) {
            System.out.println(vector1[i]);
        }


        //Avem 2 vectori de lungimi diferite(sa contina fiecare vector cel putin 3 valori duplicate)
        //Sa se afiseze elementele comune o singura data

        System.out.println("Exercitiul 1 cu vectori");
        //Output: Sirul de cuvinte contine x(numarul lor) cuvinte cu prefixul al
        //alfabet, alin
        int contor = 0;
        String rezultat = "";
        String[] sirDeCuvinte = new String[]{"alfabet", "masa", "alin", "scaun"};
        for (int f = 0; f < sirDeCuvinte.length; f++) {
            if (sirDeCuvinte[f].startsWith("al")) {
                contor++;
                rezultat = rezultat + sirDeCuvinte[f] + ", ";
            }
        }
        System.out.println("Sirul de numere contine " + contor + " cuvinte cu prefixul al: " + rezultat);

        System.out.println("Exercitiul 2 cu vectori");
        int sumArray = 0;
        int[] array = new int[]{2, 6, 8, -10, 21, 18, -2};
        for (i = array.length - 1; i >= 0; i--) {
            if (array[i] % 2 == 0) {
                System.out.print(array[i] + ", ");
                sumArray = sumArray + array[i];
            }

        }
        System.out.println("Suma numerelor pare este: " + sumArray);

        System.out.println("TEMA");
        System.out.println("Numerele negative de pe pozitii impare in ordine inversa: ");
        System.out.print("");
        int[] vectorTema = new int[]{5, -3, 9, -2, 1, 4, 3, 7, 8, -12, 17, 16};
        for (i = vectorTema.length - 1; i >= 0; i--) {
            if (i % 2 == 1 && vectorTema[i] < 0) {
                System.out.print(vectorTema[i] + ", ");
            }
        }
        System.out.println();
        System.out.println("Numerele pozitive pare de pe pozitii impare in ordine inversa: ");
        for (i = vectorTema.length - 1; i >= 0; i--) {
            if (i % 2 == 1 && vectorTema[i] > 0 && vectorTema[i] % 2 == 0) {
                System.out.print(vectorTema[i] + ", ");
            }
        }
        System.out.println();
        System.out.println("Sa se calculeze suma elementelor pare dintr-o matrice de 4 pe 4: ");
        int[][] matriceTema = new int[][]{{6, 9, 7, 2}, {4, 6, 3, 1}, {8, 4, 6, 2}, {1, 9, 7, 5}};
        int sumaMatrice = 0;
        for (i = 0; i < matriceTema.length; i++) {
            for (int j = 0; j < matriceTema.length; j++) {
                if (matriceTema[i][j] % 2 == 0) {
                    sumaMatrice = sumaMatrice + matriceTema[i][j];
                }
            }
        }
        System.out.println(sumaMatrice);

        System.out.println("Sa se calculeze suma elementelor de pe linii pare si coloane impare");
        sumaMatrice = 0;
        int sumaLC = 0;
        for (i = 0; i < matriceTema.length; i++) {
            for (int j = 0; j < matriceTema.length; j++) {
                if (j % 2 == 1 || i % 2 == 0) {
                    sumaMatrice = sumaMatrice + matriceTema[i][j];
                }

            }


        }
        System.out.println(sumaMatrice);


    }

}
