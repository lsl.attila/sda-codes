public class Metode {
    public static void main(String[] args) {
        //Sa se implementeza o metoda care calculeaza dobanda unui imprumut in functie de salariul castigat
        //output:pentru suma de x lei vei avea o dobanda de y lei daca salariul tau este z lei
        //Daca salariul este mai mic de 5000 de lei dobanda va fi de 10%
        //Daca salariul este mai mic de 10000 de lei dobanda va fi de 15%
        //Daca salariul este peste 10000 de lei dobanda va fi de 20%

        //1.Sa se afiseze folosind do - while si while numerele intre 10 si -10, descrescator
        //Sa se afiseze folosind do - while si while din 3 in 3 numerele intre 5 si -10, descrescator

        System.out.println("Afisam exercitiul cu dobanda");
        System.out.println(calculeazaDobanda(7000, 10000));
        System.out.println("Sa se afiseze folosind while numerele intre 10 si -10, descrescator");
        System.out.println(numereIntervalWhile(10));
        System.out.println("Sa se afiseze folosind do - while numerele intre 10 si -10, descrescator");
        System.out.println(numereIntervalDoWhile(10));
        System.out.println("Sa se afiseze folosind while din 3 in 3 numerele intre 5 si -10, descrescator");
        System.out.println(numereDinTreiInTreiWhile(5));
        System.out.println("Sa se afiseze folosind do while din 3 in 3 numerele intre 5 si -10, descrescator");
        System.out.println(numereDinTreiInTreiDoWhile(5));
        System.out.println("Sa se calculeze produsul numerelor impare dintre 3 si 8 si suma numerelor pare dintre 5 si 15");
        System.out.println(prodNrImpSumNrPare(4, 1, 0));
        System.out.println("Sa se afiseze numerele impare dintre 10 si -10 in ordine descrescatoare");
        System.out.println(numarImpareDescrescator(10));
        System.out.println("Sa se calculeze suma numerelor divizibile cu 3 de la 10 pana la -8 in ordine descrescatoare folosind o singura instructiune while: ");
        System.out.println(numereDivizibileCuTrei(10, 0));
        System.out.println("Afisarea numerelor din vector cu while: ");
        int[] vector = new int[]{-7, 2, 1, 12, 27, -12, -11, 5, -20, -21};
        afisareaNrVectorWhile(vector);
        System.out.println("Sa se afiseze numarul numerele pare negative: " + afisareaNrPareNegParePozitive(vector));
        int nrPareNeg = afisareaNrPareNegParePozitive(vector);
        System.out.println("Sa se afiseze numarul numerele pare pozitive: " + afisareaNrParePoz(vector));
        int nrParePoz = afisareaNrParePoz(vector);
        System.out.println("Media elementelor din array: " + mediaElementelor(vector));
        System.out.print(daPeEcran(vector));
        System.out.println("Indexul elementului 5: " + indexulElementului(vector));
        System.out.println("Valoarea maxima: " + valoareaMaxima(vector));
        System.out.println("Valoarea minima: " + valoareMinima(vector));
        System.out.println("Numerele negative de pe pozitii impare in ordine inversa: ");
        numereleNegPozImpInv(vector);
        System.out.println("Numerele pozitive pare de pe pozitii impare in ordine inversa: ");
        numerelePozParInv(vector);
        int[][] matrice = new int[][]{{-2, 3, 5, -5}, {8, 6, 7, 5}, {0, 1, -3, 4}, {2, 6, 9, 12}};
        System.out.println("Sa se calculeze suma elementelor pare dintr-o matrice de 4 pe 4: "+sumaElPareMat(matrice));
        System.out.println("Sa se calculeze suma elementelor de pe linii pare si coloane impare: "+sumaLpareCimpare(matrice));


    }

    public static double calculeazaDobanda(double salar, double sumaImprumutata) {
        double dobanda;
        if (salar < 5000) {
            dobanda = sumaImprumutata * 0.1;
        } else if (salar < 10000) {
            dobanda = sumaImprumutata * 0.15;
        } else {
            dobanda = sumaImprumutata * 0.2;
        }
        return dobanda;
    }

    public static int numereIntervalWhile(int nrWhile) {
        while (nrWhile > -10) {
            System.out.println(nrWhile);
            nrWhile--;

        }
        return nrWhile;
    }

    public static int numereIntervalDoWhile(int nrDoWhile) {
        do {
            System.out.println(nrDoWhile);
            nrDoWhile--;
        } while (nrDoWhile > -10);
        return nrDoWhile;
    }

    public static int numereDinTreiInTreiWhile(int numarWhile) {
        while (numarWhile > -10) {
            System.out.println(numarWhile);
            numarWhile = numarWhile - 3;
        }
        return numarWhile;
    }

    public static int numereDinTreiInTreiDoWhile(int numarDoWhile) {
        do {
            System.out.println(numarDoWhile);
            numarDoWhile = numarDoWhile - 3;
        } while (numarDoWhile > -10);
        return numarDoWhile;
    }

    public static int prodNrImpSumNrPare(int numarPS, int produs, int suma) {
        while (numarPS < 15) {
            if (numarPS % 2 == 1 && numarPS < 8) {
                produs = produs * numarPS;
            } else if (numarPS % 2 == 0 && numarPS > 5) {
                suma = suma + numarPS;
            }
            numarPS++;
        }
        System.out.println("Produsul: " + produs);
        System.out.println("Suma: " + suma);
        return numarPS;
    }

    //    Sa se afiseze numerele impare dintre 10 si -10 in ordine descrescatoare
    public static int numarImpareDescrescator(int numarImpare) {
        while (numarImpare > -9) {
            if (numarImpare % 2 == 1 || numarImpare % 2 == -1) {
                System.out.println(numarImpare);
            }
            numarImpare--;
        }
        return numarImpare;
    }

    //    Sa se calculeze suma numerelor divizibile cu 3 de la 10 pana la -8 ?in ordine descrescatoare?     folosind o singura instructiune while
    public static int numereDivizibileCuTrei(int numarDivizibilCuTrei, int sumaDiv) {
        while (numarDivizibilCuTrei > -8) {
            if (numarDivizibilCuTrei % 3 == 0) {
                sumaDiv = sumaDiv + numarDivizibilCuTrei;
            }
            numarDivizibilCuTrei--;
        }
        System.out.println("Suma: " + sumaDiv);
        return numarDivizibilCuTrei;
    }

    //    Afisarea numerelor din vector cu while
    public static String afisareaNrVectorWhile(int[] vector) {
        String afisare = "";
        int i = 0;
        while (i < vector.length) {
            System.out.println(vector[i]);
            i++;
        }
        return afisare;
    }

    //Sa se numara cate numere pare negative si pare pozitive avem in vector
    public static int afisareaNrPareNegParePozitive(int[] vector) {
        int nrPareNeg = 0;
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] % 2 == 0 && vector[i] < 0) {
                nrPareNeg++;
            }

        }
        return nrPareNeg;

    }

    public static int afisareaNrParePoz(int[] vector) {
        int nrParePoz = 0;
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] % 2 == 1 && vector[i] > 0) {
                nrParePoz++;
            }
        }
        return nrParePoz;
    }

    //Sa se calculeze media elementelor din array
    public static int mediaElementelor(int[] vector) {
        int suma = 0;
        int media = 0;
        for (int i = 0; i < vector.length; i++) {
            suma = vector[i] + suma;
        }
        media = suma / vector.length;
        return media;
    }

    //Sa se afiseze pe ecran DA daca vectorul contine valoarea 5
    public static String daPeEcran(int[] vector) {
        String rezultat = "";
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] == 5) {
                System.out.println("DA");
            }
        }
        return rezultat;
    }

    //Sa se afiseze pe ecran indexul elementului 5 din vector
    public static int indexulElementului(int[] vector) {
        int rezultat = 0;
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] == 5) {
                rezultat = i;
            }
        }
        return rezultat;
    }

    //Sa se afiseze valoarea maxima din vector
    public static int valoareaMaxima(int[] vector) {
        int valMax = 0;
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] > valMax) {
                valMax = vector[i];
            }

        }
        return valMax;
    }

    //Sa se afiseze valoarea minima din vector
    public static int valoareMinima(int[] vector) {
        int valMin = 0;
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] < valMin) {
                valMin = vector[i];
            }
        }
        return valMin;
    }

    //Numerele negative de pe pozitii impare in ordine inversa
    public static String numereleNegPozImpInv(int[] vector) {
        String afisare = "";
        for (int i = vector.length - 1; i >= 0; i--) {
            if (vector[i] < 0 && i % 2 == 1) {
                System.out.println(vector[i]);

            }

        }

        return afisare;
    }

    //Numerele pozitive pare de pe pozitii impare in ordine inversa
    public static String numerelePozParInv(int[] vector){
        String afisare="";
        for(int i=vector.length-1;i>=0;i--){
            if(vector[i]>0&&i%2==0){
                System.out.println(vector[i]);
            }
        }
        return afisare;
    }

    //Sa se calculeze suma elementelor pare dintr-o matrice de 4 pe 4
    public static int sumaElPareMat(int[][] matrice){
        int suma=0;
        for (int i = 0; i <matrice.length ; i++) {
            for (int j = 0; j <matrice.length ; j++) {
                if(matrice[i][j]%2==0){
                    suma=suma+matrice[i][j];

                }
            }
        }return suma;
    }

    //Sa se calculeze suma elementelor de pe linii pare si coloane impare
    public static int sumaLpareCimpare(int[][] matrice){
        int suma=0;
        for (int i = 0; i <matrice.length ; i++) {
            for (int j = 0; j <matrice.length ; j++) {
                if(i%2==0||j%2==1){
                    suma=suma+matrice[i][j];
                }
            }
        }return suma;
    }


}
