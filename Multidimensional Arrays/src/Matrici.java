public class Matrici {
    public static void main(String[] args) {
        System.out.println("MATRICI");
        int[][] matrice = new int[][]{{-2, 3, 5, 10}, {8, 6, 7, 5}, {0, 1, -3, 4}, {2, 6, 9, 12}};
        for (int i = matrice.length - 1; i >= 0; i--) {
            for (int j = matrice.length - 1; j >= 0; j--) {
                System.out.print(matrice[i][j] + "  ");
            }
            System.out.println();
        }

        //Sa afisam matricea cu toate metode posibile
        System.out.println("Tema: Matricea1");
        for (int j = 0; j < matrice.length; j++) {
            for (int i = 0; i < matrice.length; i++) {
                System.out.print(matrice[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("Tema: Matricea2");
        for (int i = 0; i < matrice.length; i++) {
            for (int j = 0; j < matrice.length; j++) {
                System.out.print(matrice[j][i] + " ");
            }
            System.out.println();
        }

        System.out.println("Tema: Matricea3");
        for (int j = matrice.length - 1; j >= 0; j--) {
            for (int i = matrice.length - 1; i >= 0; i--) {
                System.out.print(matrice[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("Tema: Matricea4");
        for (int i = matrice.length - 1; i >= 0; i--) {
            for (int j = matrice.length - 1; j >= 0; j--) {
                System.out.print(matrice[j][i] + " ");
            }
            System.out.println();
        }

        System.out.println("Variabilele de pe diagonala");
        for (int i = 0; i < matrice.length; i++) {
            for (int j = 0; j < matrice.length; j++) {
                if (i == j) {
                    System.out.print(matrice[i][j] + " ");

                }
            }
        }
        System.out.println();

        System.out.println("Variabilele de pe al doilea diagonala");
        for (int i = 0; i < matrice.length; i++) {
            for (int j = 0; j < matrice.length; j++) {
                if ((i + j) == (matrice.length - 1)) {
                    System.out.print(matrice[i][j] + " ");
                }
            }

        }
        System.out.println();

        System.out.println("Sa se afiseze variabilele de de sub diagonalei ");
        for (int i = 0; i < matrice.length; i++) {
            for (int j = 0; j < matrice.length; j++) {
                if (i > j)
                    System.out.println(matrice[i][j]);
            }
        }

        System.out.println("Sa se afiseze variabilele deasupra diagonalei ");
        for (int i = 0; i < matrice.length; i++) {
            for (int j = 0; j < matrice.length; j++) {
                if (i < j) {
                    System.out.println(matrice[i][j]);
                }
            }

        }

        System.out.println("Sa se afiseze conturul si colturile sa se afiseze doar odata");
        for (int j = 0; j < matrice.length; j++){
            System.out.println(matrice[0][j]);
        }
        for(int i=1;i<matrice.length;i++){
            System.out.println(matrice[i][matrice.length-1]);
        }
        for (int j=matrice.length-2;j>=0;j--){
            System.out.println(matrice[matrice.length-1][j]);
        }
        for (int i=matrice.length-2;i>0;i--){
            System.out.println(matrice[i][0]);
        }

    }
}

