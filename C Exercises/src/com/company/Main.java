package com.company;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private static DecimalFormat df = new DecimalFormat("0.00");
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
// 1.) Use System.out.print method to print the same statement in separate lines.
//        System.out.print("Hello, World\nHello, World");
//        System.out.println();

//  2.) Enter any value with several digits after the decimal point and assign it to variable
//      of type double. Display the given value rounded to two decimal places.
//        double a = 122.46987;
//        String number = String.valueOf(a);
//        number = number.substring(number.indexOf(".")).substring(1);
//        double b = Double.parseDouble(number);
//        System.out.println("Digits after the decimal point: " + b);
//        System.out.print("Value rounded to two decimal places: ");
//        System.out.println(df.format(a));

//  3.) Display any three strings of characters on one line so that they are aligned to the right
//      edge of the 15-character blocks. How to align strings to the left edge?

//        String[] strings = {"one", "two", "three"};
//        String s1 = "one";
//        String s2 = "two";
//        String s3 = "three";
//        String line = String.format("%1$7s", s1);
//        System.out.print(line);
//        line = String.format(s2);
//        System.out.print(line);
//        line = String.format(s3);
//        System.out.print(line);

//  4.) Enter two values of type int. Display their division casted to the double type and rounded to
//  the third decimal point.
//        System.out.println();
//        int firstValue = 12;
//        int secondValue = 33;
//        double division = ((double) firstValue) / secondValue;
//        System.out.print(String.format("%.3g%n", division));

//  5.) Sum two integer variables initialized with maximal values for that type.
//        int a1 = Integer.MAX_VALUE;
//        int a2 = Integer.MAX_VALUE;
//        System.out.println(a1 + a2);

//  6.)Create three variables, one for each type: float, byte and char. Enter values corresponding to
//  those types using Scanner. What values are you able to enter for each type?
//        System.out.println("Please enter three variables(float, byte, char): ");
//        float f1=scanner.nextFloat();
//        byte b1=scanner.nextByte();
//        char c1=scanner.next().charAt(0);
//        System.out.println(f1+" "+b1+" "+c1);

//  7.) Write a method that receives 2 string parameters and checks if the second string is contained by the first one. The method will return a boolean. Example: returns true for “The Witcher” and “Witcher”.
//  Write a method that receives 2 parameters, a string and an int, and checks if the int variable is contained by the string. The method will return a boolean. Example: returns true for string “2 apples”  and int 2.
//  Write a method that receives 2 integer parameters and check if the first int contains the second int. The method will return a boolean. Example: returns true for 123 and 1.
//  Can the above methods(1,2,3) have the same name?

//        String one = "The Witcher";
//        String two = "Witcher";
//        System.out.println(containsStringString(one, two));
//        String three = "2 apples";
//        int four = 23;
//        System.out.println(containsStringInt(three, four));
//        int firstNumber = 123;
//        int secondNumber = 5;
//        System.out.println(containsIntInt(firstNumber, secondNumber));

//  8.) Write an application that will show if entered value is greater, equal or lower than 30.
//        System.out.println("Please enter a value: ");
//        int value=scanner.nextInt();
//        if(value<30){
//            System.out.println("Value is lower than 30");
//        }else if(value==30){
//            System.out.println("Value is equal to 30");
//        }else {
//            System.out.println("Value is greater than 30");
//        }

//  9.)   As above but compare two values at the same time.
//        Show if both values are greater, equal or lower than 30.
//        Otherwise show "<>".
//                Example:
//        input: 22, 25 output: lower
//        input: 30, 30 output: equal
//        input: 32, 33 output: greater
//        input: 22, 32 output: <>
//        input: 32, 22 output: <>
//        System.out.println("Please enter two values: ");
//        int valueOne=scanner.nextInt();
//        int valueTwo=scanner.nextInt();
//        if(valueOne<30&&valueTwo<30){
//            System.out.println("Both of the values are lower than 30");
//        }else if(valueOne==30&&valueTwo==30){
//            System.out.println("Both of the values are equal to 30");
//        }else if(valueOne>30&&valueTwo>30){
//            System.out.println("Both of the values are greater than 30");
//        }else {
//            System.out.println("<>");
//        }

//  10.)  As above but only one of the values has to be greater than 30.
//        Example:
//        input: 22, 25 output: lower
//        input: 30, 30 output: equal
//        input: 32, 33 output: greater
//        input: 22, 32 output: greater
//        input: 32, 22 output: greater
//        System.out.println("Please enter two values: ");
//        int valueThree=scanner.nextInt();
//        int valueFour=scanner.nextInt();
//        if(valueThree<30&&valueFour<30){
//            System.out.println("lower");
//        }else if(valueThree==30&&valueFour==30){
//            System.out.println("equal");
//        }else if(valueThree>30&&valueFour>30){
//            System.out.println("greater");
//        }else if(valueThree<30&&valueFour>30){
//            System.out.println("greater");
//        }else {
//            System.out.println("greater");
//        }

//  11.)  Write an application that for any entered number between 0 and 9 will provide it’s name. For
//        example for “3” program should print “three”.
//        System.out.println("Please enter a number between 0-9: ");
//        int condition=scanner.nextInt();
//        if(condition<0||condition>9){
//            System.out.println("The number you entered is either smaller than 0 or bigger than 9");
//        }else {
//            switch (condition){
//                case 0:
//                    System.out.println("zero");
//                    break;
//                case 1:
//                    System.out.println("one");
//                    break;
//                case 2:
//                    System.out.println("two");
//                    break;
//                case 3:
//                    System.out.println("three");
//                    break;
//                case 4:
//                    System.out.println("four");
//                    break;
//                case 5:
//                    System.out.println("five");
//                    break;
//                case 6:
//                    System.out.println("six");
//                    break;
//                case 7:
//                    System.out.println("seven");
//                    break;
//                case 8:
//                    System.out.println("eight");
//                    break;
//                case 9:
//                    System.out.println("nine");
//                    break;
//                default:
//                    System.out.println("Nice job");
//            }
//        }


//  12.)  Using nested for loops draw (parents loop iterator should be called “row”,
//        child – “column”):
//        triangle,
//        *rectangle with diagonals,
//        **Christmas tree
//        System.out.println("Triangle");
//        int rows = 7;
//        for (int i = 1; i <= rows; i++) {
//            for (int j = 1; j <= i; j++) {
//                System.out.print("* ");
//            }
//            System.out.println();
//        }
//
//        System.out.println("Pyramid/Christmas tree");
//        //rows are the same as in the triangle
//        for (int i = 0; i < rows; i++) {
//            for (int j = rows - i; j > 1; j--) {
//                System.out.print(" ");
//            }
//            for (int j = 0; j <= i; j++) {
//                System.out.print("* ");
//            }
//            System.out.println();
//        }
//
//        System.out.println("Rectangle with diagonals");
//        for (int i = 1; i <= rows; i++) {
//            for (int j = 1; j <= rows; j++) {
//                if (i == 1 || i == rows || j == 1 || j == rows || i == j || j == (rows - i + 1)) {
//                    System.out.print("*");
//                } else {
//                    System.out.print(" ");
//                }
//
//            }
//            System.out.println();
//        }

//  13.)  If you would like to buy a bottle of milk – cashier will ask you for a specific amount of
//        money. You have to enter that value and verify if it is same as the cashier asked.
//        If you would like to buy a bottle of wine – cashier will ask you if you are an adult and
//        for positive answer ask for a specific amount of money.
//        double milkPrice = 5;
//        int beerPrice = 6;
//        double myMoney = 45;
//        boolean quit = false;
//        String answer = "";
//        printMenu();
//        while (!quit) {
//            int switchToCase = scanner.nextInt();
//            scanner.nextLine();
//            switch (switchToCase) {
//                case 0:
//                    System.out.println("Thank you for your visit");
//                    break;
//                case 1:
//                    System.out.println("That will be 5 RON");
//                    double payingForMilk = scanner.nextDouble();
//                    if (payingForMilk < milkPrice) {
//                        myMoney-=payingForMilk;
//                        double milkRest = milkPrice - payingForMilk;
//                        System.out.println("Sorry, that's not enough! I need " + (milkRest) + " more!");
//                        payingForMilk = scanner.nextDouble();
//                        if (payingForMilk == milkRest) {
//                            myMoney-=payingForMilk;
//                            System.out.println("Thank you very much! Would you like anything else?");
//                            answer=scanner.next();
//                            if (answer.equalsIgnoreCase("Yes")) {
//                                printMenu();
//                            } else {
//                                System.out.println("Thank you for your visit, have a nice day!");
//                            }
//                        } else {
//                            System.out.println("Are you kidding me? Get out of here!");
//                        }
//                    }
//                    if (payingForMilk == milkPrice) {
//                        myMoney -= payingForMilk;
//                        System.out.println("Thank you very much. Would you like anything else?");
//                        answer = scanner.next();
//                        if (answer.equalsIgnoreCase("Yes")) {
//                            printMenu();
//                        } else {
//                            System.out.println("Thank you for your visit, have a nice day!");
//                        }
//                    }
//                    if (payingForMilk > milkPrice) {
//                        myMoney -= payingForMilk;
//                        double changeMilk = payingForMilk - milkPrice;
//                        System.out.println("Here's your change " + changeMilk);
//                        answer = scanner.next();
//                        if (answer.equalsIgnoreCase("Keep")) {
//                            System.out.println("Thank you very much. Would you like anything else?");
//                            answer = scanner.next();
//                            if (answer.equalsIgnoreCase("No")) {
//                                System.out.println("Have a nice day!");
//                            } else if (answer.equalsIgnoreCase("Yes")) {
//                                printMenu();
//
//                            }
//                        }
//                        if (answer.equalsIgnoreCase("Thanks")) {
//                            myMoney += changeMilk;
//                            System.out.println("Thank you very much. Would you like anything else?");
//                            answer = scanner.next();
//                            if (answer.equalsIgnoreCase("No")) {
//                                System.out.println("Have a nice day!");
//                            } else if (answer.equalsIgnoreCase("Yes")) {
//                                printMenu();
//
//                            }
//                        }
//
//                    }
//                    System.out.println("Your current balance: " + myMoney);
//                    break;
//
//                case 2:
//                    System.out.println("How old are you?");
//                    int age = scanner.nextInt();
//                    if (age >= 18 && age <= 100) {
//                        System.out.println("That will be 6 RON");
//                        double payingForBeer = scanner.nextDouble();
//                        if (payingForBeer < beerPrice) {
//                            myMoney-=payingForBeer;
//                            double beerRest = beerPrice - payingForBeer;
//                            System.out.println("Sorry, that's not enough! I need " + (beerRest) + " more!");
//                            payingForBeer = scanner.nextDouble();
//                            if (payingForBeer == beerRest) {
//                                myMoney-=payingForBeer;
//                                System.out.println("Thank you very much! Would you like anything else?");
//                                answer=scanner.next();
//                                if (answer.equalsIgnoreCase("Yes")) {
//                                    printMenu();
//                                } else {
//                                    System.out.println("Thank you for your visit, have a nice day!");
//                                }
//
//                            } else {
//                                System.out.println("Are you kidding me? Get out of here!");
//                            }
//                        }
//                        if (payingForBeer == beerPrice) {
//                            myMoney -= payingForBeer;
//                            System.out.println("Thank you very much. Would you like anything else?");
//                            answer = scanner.next();
//                            if (answer.equalsIgnoreCase("Yes")) {
//                                printMenu();
//                            } else {
//                                System.out.println("Thank you for your visit, have a nice day!");
//                            }
//                        }
//                        if (payingForBeer > beerPrice) {
//                            myMoney -= payingForBeer;
//                            double changeBeer = payingForBeer - beerPrice;
//                            System.out.println("Here's your change " + changeBeer);
//                            answer = scanner.next();
//                            if (answer.equalsIgnoreCase("Keep")) {
//                                System.out.println("Thank you very much. Would you like anything else?");
//                                answer = scanner.next();
//                                if (answer.equalsIgnoreCase("No")) {
//                                    System.out.println("Have a nice day!");
//                                } else if (answer.equalsIgnoreCase("Yes")) {
//                                    printMenu();
//
//                                }
//                            }
//                            if (answer.equalsIgnoreCase("Thanks")) {
//                                myMoney += changeBeer;
//                                System.out.println("Would you like anything else?");
//                                answer = scanner.next();
//                                if (answer.equalsIgnoreCase("No")) {
//                                    System.out.println("Have a nice day!");
//                                } else if (answer.equalsIgnoreCase("Yes")) {
//                                    printMenu();
//
//                                }
//                            }
//
//                        }
//                        System.out.println("Your current balance: " + myMoney);
//                    }
//                    if (age < 18) {
//                        System.out.println("Sorry but I can't serve alcohol for underage people! Would you like to buy something else?");
//                        answer = scanner.next();
//                        if (answer.equalsIgnoreCase("Yes")) {
//                            printMenu();
//                        } else {
//                            System.out.println("Have a nice day!");
//                        }
//                    }
//                    if (age > 100) {
//                        System.out.println("Sorry but you are too old! Would you like to buy something else?");
//                        answer = scanner.next();
//                        if (answer.equalsIgnoreCase("Yes")) {
//                            printMenu();
//                        } else {
//                            System.out.println("Have a nice day!");
//                        }
//                    }
//                    break;
//
//                case 3:
//                    System.out.println("Sorry but the coffee machine is broken. Would you like to buy something else?");
//                    answer = scanner.next();
//                    if (answer.equalsIgnoreCase("Yes")) {
//                        printMenu();
//                    } else {
//                        System.out.println("Have a nice day!");
//                    }
//                    break;
//
//                case 4:
//                    System.out.println("We are sold out of newspapers. Would you like to buy something else?");
//                    answer = scanner.next();
//                    if (answer.equalsIgnoreCase("Yes")) {
//                        printMenu();
//                    } else {
//                        System.out.println("Have a nice day!");
//                    }
//                    break;
//            }
//        }

        //14.)Write an application that will find biggest value within array of int variables.
//        int[] array = new int[]{2, 4, 6, 3, 8, 1, 10, 11, 54, 12, 32, 99};
//        int min = array[0];
//        int max = array[0];
//        for (int i = 0; i < array.length; i++) {
//            if (array[i] < min) {
//                min = array[i];
//            }
//            if(array[i]>max){
//                max=array[i];
//            }
//        }
//        System.out.println("Min value: "+min);
//        System.out.println("Max value: "+max);

        //15.)Same as 14 but check your application using randomly generated array (use Random class),
//        Random random = new Random();
//        int[] array = new int[10];
//        int min = array[0];
//        int max = array[0];
//        for (int i = 0; i < array.length; i++) {
//            array[i] = random.nextInt();
//            System.out.println(array[i]);
//            if (array[i] < min) {
//                min = array[i];
//            }
//            if (array[i] > max) {
//                max = array[i];
//
//            }
//        }
//        System.out.println("Min randomly generated value: "+min);
//        System.out.println("Max randomly generated value: "+max);

        //16.)Write an application that will find the longest char sequence within an array of type String.
        //Test it in the same way as you have done in the previous exercise. How will you generate
        //random char sequences?
//        String[] arrayOfStrings=new String[]{"people","animal","car","butterfly","party"};
//        int maxLength=0;
//        String longestString=null;
//        for (String s : arrayOfStrings){
//            if(s.length()>arrayOfStrings.length){
//                maxLength=s.length();
//                longestString=s;
//            }
//        }
//        System.out.println("longest string: "+longestString);

//        String alphabet = "ABCDEFGHIJKLMNOPQRSTUWVXYZ";
//        StringBuilder stringBuilder = new StringBuilder();
//        Random random = new Random();
//        int length = 10;
//        for (int i = 0; i <length ; i++) {
//            int index=random.nextInt(alphabet.length());
//            char randomChar = alphabet.charAt(index);
//            stringBuilder.append(randomChar);
//        }
//        String randomString = stringBuilder.toString();
//        System.out.println("Random String is: "+randomString);

        //17.) Given 2 arrays with integer values check if the arrays are the same(same length and same elements in that order).
        int[] firstArray = new int[]{2, 4, 6, 7, 4, 3, 2};
        int[] secondArray = new int[]{2, 4, 6, 5, 4, 3, 2};
        checkArrays(firstArray,secondArray);



    }


//    public static void printMenu() {
//        System.out.println("What would you like to buy?");
//        System.out.println("0 - Quit\n" +
//                "1 - Milk\n" +
//                "2 - Beer\n" +
//                "3 - Coffee\n" +
//                "4 - Newspaper");
//    }

//    public static boolean containsStringString(String one, String two) {
//        if (one.contains(two)) {
//            System.out.println("String one contains string two");
//            return true;
//        }
//        return false;
//    }
//
//    public static boolean containsStringInt(String stringOne, int intOne) {
//        if (stringOne.isEmpty()) {
//            return false;
//        }
//        StringBuilder stringBuilder = new StringBuilder();
//        char[] chars = stringOne.toCharArray();
//        for (char c : chars) {
//            if (Character.isDigit(c)) {
//                stringBuilder.append(c);
//                System.out.println("Digit found in the string: " + stringBuilder);
//            }
//            if (Integer.parseInt(stringBuilder.toString()) == intOne) {
//                System.out.println("The digit in the string is equal to the given int");
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public static boolean containsIntInt(int one, int two) {
//        if (Integer.toString(one).contains(Integer.toString(two))) {
//            return true;
//        }
//        return false;
//    }

    public static boolean checkArrays(int[] firstArray, int[] secondArray) {
        boolean isInTheSameOrder=false;
        if (firstArray.length == secondArray.length) {
            System.out.println("The two arrays have the same length!");
            for (int i = 0; i < firstArray.length; i++) {
                if (firstArray[i] == secondArray[i]) {
                    isInTheSameOrder=true;
                } else {
                    isInTheSameOrder=false;
                }
            }
        } else {
            System.out.println("The two arrays differ in length!");
        }
        System.out.println("The two arrays are in the same order: "+isInTheSameOrder);
      return isInTheSameOrder;

    }

}
