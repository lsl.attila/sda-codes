public class ToManyNoOfHoursException extends Exception {
    public ToManyNoOfHoursException(int n){
        super(n+" Too many no. of hours exception");
    }
}
