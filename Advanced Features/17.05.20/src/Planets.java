public enum Planets {
    MERCURY("Mercury", "Small", 2),
    VENUS("Venus","Small",1),
    EARTH("Earth","Small",0),
    MARS("Mars","Small",1);

    String name;
    String size;
    int distance;

    Planets(String name, String size, int distance){
        this.name=name;
        this.size=size;
        this.distance=distance;
    }

    public double distanceFromEarth(){
        return distance;
    }


    @Override
    public String toString() {
        return size+" "+name+" "+distance;
    }
}
