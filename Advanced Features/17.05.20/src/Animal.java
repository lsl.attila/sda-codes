public class Animal {
    private String name;
    private String colour;
    private String gender;
    private double weight;
    private double age;
    private String yieldVoice="quiet";

    public Animal(String name, String colour, String gender, double weight,double age){
        this.name=name;
        this.colour=colour;
        this.gender=gender;
        this.weight=weight;
        this.age=age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getYieldVoice(){
        return yieldVoice;
    }


}
