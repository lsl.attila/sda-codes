public enum LenghtUnit {
    METER(1,"Metri..."),
    CENTIMETER(0.1,"Centi..."),
    FOOT(0.5,"Foot..."),
    INCH(0.3,"Inch...");

    double value;
    String name;

    LenghtUnit(double value,String name){
        this.value=value;
        this.name=name;
    }

    public double getValue(){
        return value;
    }

}
