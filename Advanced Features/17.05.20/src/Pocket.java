public class Pocket {

//    2.
//    Create class Pocket.
//    a)
//    Add field „ money ”, create constructor , getter and setter
//    b)
//    Add verification for both getter and setter . Getter should result in returning as much
//    money , as the user asked for. It should return 0 if money <=
//    c)
//    Setter should not accept values below 0 and greater than 3000. It may print a message
//    like „I don’t have enough space in my pocket for as much money

    private int money;

    public Pocket(int money){
        this.money=money;
    }

    public int getMoney() {
        if(this.money<=10){
            return 0;
        }
        return money;
    }

    public void setMoney(int money)throws IllegalArgumentException{
        if(money<0){
            throw new IllegalArgumentException("Your pocket is empty");
        }
        if(money>3000){
            throw new IllegalArgumentException("You don't have enough space in your pocket for that kind of money");
        }
        this.money=money;
    }

    public void myMoney(){
        System.out.println("You have "+getMoney()+ " RON "+"in your pocket");
    }
}
