public class Rectangle extends Shape {
    private double lungime;
    private double latime;

    public Rectangle(String nume, String culoare,double lungime, double latime){
        super(nume,culoare);
        this.lungime=lungime;
        this.latime=latime;
    }

    @Override
    public double aria() {
        double aria=lungime*latime;
        System.out.println(getNume()+" de culoare "+getCuloare()+" care are lungimea de "+lungime+" si latimea de "+latime+" si arie de "+aria);
        return aria;
    }
}
