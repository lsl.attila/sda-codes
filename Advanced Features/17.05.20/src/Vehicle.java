public class Vehicle {
    private int maxSpeed;

    public Vehicle(int maxSpeed){
        this.maxSpeed=maxSpeed;
    }


    public int getMaxSpeed(){
        return this.maxSpeed;
    }

    @Override
    public String toString() {
        System.out.println("Vehicle max speed is:"+maxSpeed);
        return "";
    }

    public void show(){
        System.out.println("Show");
    }

    public void repair(){
        System.out.println("Vehicle is repaired");
    }
}
