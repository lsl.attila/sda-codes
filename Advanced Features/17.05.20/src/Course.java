public class Course implements Activity {
//1.Create Activity interface with method doActivity();
//2.Create class Course (fileds: name, noOfHours) which implements Activity interface
//3.Add getter and setter
//4.Add conditions in noOfHours getter:
//            4.1 If noOfHours < 0 throw InvalidNoOfHoursException
//            4.2 If noOfHours > 100 throw ToManyNoOfHoursException
//5.In main method create 3 Course objects (one object shoud be valid, one object shoud throw InvalidNoOfHoursException when the getter is called,
//                                          one object shoud throw ToManyNoOfHoursException when the getter is called )
//6. Add all objects in Courses array
//7. Go through Courses array and show noOfHours and activity for each course (write the code to handle all possible exception and show proper messages)

    private String name;
    private int noOfHours;
    private String activity;

    public Course(String name, int noOfHours,String activity) {
        this.name = name;
        this.noOfHours = noOfHours;
        this.activity=activity;
    }

    public String getActivity(){
        return activity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getNoOfHours()throws Exception{
        if(noOfHours<=0){
            throw new InvalidNoOfHoursException(noOfHours);
        } else if(noOfHours>100){
           throw new ToManyNoOfHoursException(noOfHours);
        }else {
            return noOfHours;
        }

    }


    @Override
    public void doActivity() {
        System.out.println("No. of hours: "+noOfHours+" Activity "+activity);
    }
}
