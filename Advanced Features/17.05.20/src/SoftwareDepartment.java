public class SoftwareDepartment extends Department {
    private String languageCode;

    public SoftwareDepartment(String name, int noOfEmployees,String languageCode) {
        super(name, noOfEmployees);
        this.languageCode=languageCode;
    }

    @Override
    public String getActivity() {
        return ",the department's language code is: "+languageCode;
    }
}
