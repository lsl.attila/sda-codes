public class Car implements Comparable<Car> {

//    private boolean con;
//    public Car(int maxSpeed, boolean con){
//        super(maxSpeed);
//        this.con=con;
//    }
//
//    public boolean isCon(){
//        return con;
//    }
//
//    @Override
//    public void show() {
//        System.out.println(super.toString()+" Show");
//    }

    private int maxSpeed;

    public Car(int maxSpeed){
        this.maxSpeed=maxSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public int comparoTo(Car otherCar) {
        return this.maxSpeed-otherCar.maxSpeed;
    }
}
