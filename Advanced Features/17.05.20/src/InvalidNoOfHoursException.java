public class InvalidNoOfHoursException extends Exception {
    public InvalidNoOfHoursException(int n){
        super(n+" Invalid no. of hours");
    }
}
