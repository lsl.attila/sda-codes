public class Circle extends Shape{
    private double raza;

    public Circle(String nume, String culoare,double raza){
        super(nume,culoare);
        this.raza=raza;
    }

    @Override
    public double aria() {
        double aria=raza*raza*Math.PI;
        System.out.println(getNume()+" de culoare "+getCuloare()+" care are raza de "+raza+" si arie de "+aria);
        return aria;
    }
}
