public class HrDepartment extends Department {
    private String nextEvent;

    public HrDepartment(String name, int noOfEmployees, String nextEvent) {
        super(name, noOfEmployees);
        this.nextEvent = nextEvent;
    }

    @Override
    public String getActivity() {
        return ",the next event will be: " + nextEvent;
    }

    public void check(int n) throws Exception {
        if (n < 1) {
            throw new Exception("n este mai mic ca 1");
        }
        System.out.println(10/n);
    }
}
