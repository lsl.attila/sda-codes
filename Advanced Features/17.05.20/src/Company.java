public class Company {

    private String name;
    private Size size;
    private Department[] arrayOfDepartments;

    public Company(String name, Size size, Department[] arrayOfDepartments) {
        this.name = name;
        this.size = size;
        this.arrayOfDepartments = arrayOfDepartments;
    }

    public String getName() {
        return name;
    }

    public Size getSize() {
        return size;
    }
}
