public class Catty extends Animal {
    private String yieldvoice;

    public Catty(String name, String colour, String gender, double weight, double age,String yieldvoice) {
        super(name, colour, gender, weight, age);
        this.yieldvoice=yieldvoice;
    }

    @Override
    public String getYieldVoice() {
        return "(the cat) voice is: "+yieldvoice;
    }

}
