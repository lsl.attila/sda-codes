public class Doggy extends Animal {
    private String yieldVoice;

    public Doggy(String name, String colour, String gender, double weight, double age,String yieldVoice) {
        super(name, colour, gender, weight, age);
        this.yieldVoice=yieldVoice;
    }

    @Override
    public String getYieldVoice() {
        return "(the dog) voice is: "+yieldVoice;
    }
}
