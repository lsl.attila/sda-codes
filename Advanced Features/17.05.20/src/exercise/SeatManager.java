package exercise;

public class SeatManager {

    public int noOfSeats;

    public SeatManager(int noOfSeats) {
        this.noOfSeats = noOfSeats;
    }

    public int getNoOfSeats() {
        return noOfSeats;
    }

    public void takeASeat(){
        System.out.println("Try and take a seat");
        if(noOfSeats>0){
            System.out.println("Success, seats "+noOfSeats);
            noOfSeats--;
        }else {
            System.out.println("No available seats");
        }
    }
}
