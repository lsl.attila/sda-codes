package exercise;

public abstract class Person{

//        1.Create Scheduler interface with method getNoOfHours();
//        2.Create Person class (fileds: firstName, lastName, age, uid (unique code like CNP))
//        3.Create Employee class which extends Person and implements Scheduler (fileds: salary )
//        4.Create Module class (fields: name, type)
//        5.Create Student class which extends Person and implements Scheduler ( list of modules )
//        6.Create School class (fileds: list of employees and list ofstudents )
//        7.Create a method in School class which return a map with employees and students (uid shoud be the key map)
//        8.Using the new method obtain the map and show the following information for each object:
//        uid
//        name
//        age
//        noOfNours
//        9*.If the object is an instance of Student go deeper and show the folowing information for each module: (tips: instanceof)
//        name
//                type
//
//        If the object is an instance of Employee show the salary
//        10.Make Person class abstract
//        11.Add a method in Person class which retrun firstName + lastName
//        12.Add an abstract method which return object type message (this object is an employee for Employee /this object is a student for Student)
//        13.Create City class which will accept a generic type (fields: name, noOfPopulation, list of generic objects )
//        14.Use the school object created and add it into generic list of city, also create a new class University and add it to generic list
//        15.For each object check if the object is instance of School and call the method which retun the map with employee and studens and show the firstName + lastName (*11) and
//        object type(*12)

    private String firstName;
    private String lastName;
    private int age;
    private String uid; //unique code: CNP

//    public Person(String firstName, String lastName, int age, String uid) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.age = age;
//        this.uid = uid;
//    }

    public String getFullName(){
        return firstName+" "+lastName;
    }
    public abstract String getTypeMsg();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getNoOfHours() {
        return 10;
    }

}
