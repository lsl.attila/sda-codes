package exercise;

import java.util.ArrayList;
import java.util.List;

public class Student extends Person implements Scheduler {
    private List<Module> moduleList = new ArrayList<>();

//    public Student(String firstName, String lastName, int age, String uid){
//        super(firstName, lastName, age, uid);
//    }

    public List<Module> getModules() {
        return moduleList;
    }

    public void setModules(List<Module> modules) {
        this.moduleList = modules;
    }

    @Override
    public int getNoOfHours() {
        return 4;
    }

    @Override
    public String getTypeMsg() {
        return "This object is a student";
    }
}
