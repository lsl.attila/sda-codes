package exercise;

public class StopWatchThread implements Runnable {

    private String customName;

    @Override
    public void run(){
        for (int i = 0; i <10 ; i++) {
            System.out.println("Stop watch: "+i+" "+customName);
            try {
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }
}
