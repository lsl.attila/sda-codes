package exercise;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainExercises {
    public static void main(String[] args) throws InterruptedException {

//        Employee employee1 = new Employee();
//        employee1.setUid("1");
//        employee1.setFirstName("FirstName1");
//        employee1.setLastName("LastName1");
//        employee1.setAge(35);
//        employee1.setSalary(2345);
//
//
//        Employee employee2 = new Employee();
//        employee2.setUid("2");
//        employee2.setFirstName("FirstName2");
//        employee2.setLastName("LastName2");
//        employee2.setAge(40);
//        employee2.setSalary(2555);
//
//
//        Module module1 = new Module();
//        module1.setName("Module1");
//        module1.setType("Type 1");
//
//        Module module2 = new Module();
//        module2.setName("Module2");
//        module2.setType("Type 2");
//
//        List<Module> moduleList = new ArrayList<>();
//        moduleList.add(module1);
//        moduleList.add(module2);
//
//
//        Student student1 = new Student();
//        student1.setFirstName("FirstNameStudent");
//        student1.setLastName("LastNameStudent");
//        student1.setAge(12);
//        student1.setUid("Stud1");
//        student1.setModules(moduleList);
//
//
//        List<Employee> employees = new ArrayList<>();
//        employees.add(employee1);
//        employees.add(employee2);
//
//        List<Student> students = new ArrayList<>();
//        students.add(student1);
//
//        School school = new School();
//        school.setEmployeesList(employees);
//        school.setStudentsList(students);
//
//
//        Map<String, Person> map = school.getMap();

//        for (String key : map.keySet()) {
//            Person person = map.get(key);
//
//            System.out.println(key + " " + person.getFirstName() + " " + person.getLastName() + " " + person.getAge());
//
//            if(person instanceof Employee){
//                System.out.println(((Employee)person).getSalary());
//            }
//
//            if(person instanceof Student){
//                Student item=(Student) person;
//                for(Module modul1 : item.getModules()){
//                    System.out.println(modul1.getName()+" "+modul1.getType());
//                }
//
//            }
//
//
//        }
//
//        University university = new University();
//        List<Institution> institutions = new ArrayList<Institution>();
//        institutions.add(school);
//        institutions.add(university);
//        City<Institution> city = new City<>();
//        city.setList(institutions);
//
//        for (Institution institution : city.getList()) {
//            if (institution instanceof School) {
//                Map<String, Person> map1 = ((School) institution).getMap();
//                for (String key : map1.keySet()) {
//                    Person person = map1.get(key);
//                    System.out.println(person.getFullName() + " " + person.getTypeMsg());
//                }
//            }
//        }

//        StopWatchThread stopWatchThread=new StopWatchThread();
//        stopWatchThread.run();
//        stopWatchThread.setCustomName("Thread 1");
//
//        StopWatchThread stopWatchThread1=new StopWatchThread();
//        stopWatchThread1.run();
//        stopWatchThread1.setCustomName("Thread 2");

//        Thread stopWatchThread1=new Thread(new StopWatchThread());
//        stopWatchThread1.start();
//
//
//        System.out.println("Main thread starts");
//        Thread.sleep(5000);
//        System.out.println("Main thread is still running");
//        Thread.sleep(5000);
//        System.out.println("Main thread ends");

//        SeatManager seatManager=new SeatManager(1);
//        seatManager.takeASeat();
//        seatManager.takeASeat();

        //Trebuie sa fac exercitiile cu threaduri!!!

//        Optional<String> optional = Optional.of("Java Advanced Features");
//
//        if(optional.isPresent()){
//            System.out.println(optional.filter(item -> item.endsWith("tures")).orElse("abc"));
//        }

//        List<String>names= Arrays.asList("Andrew", "Brandon","Michael");
//        Stream<String> namesStream= names.stream();
//
//        List<String>names1= Arrays.asList("Andrew", "Brandon","Michael");
//        List<String>namesCopy=names1.stream().collect(Collectors.toList());
//
//        Optional<String>firstName=names.stream().findFirst();


//        List<String>names3= Arrays.asList("Andrew", "Brandon","Michael");
//        List<String>namesStartingWithA= names3.stream().filter(n->n.startsWith("A")).collect(Collectors.toList());
//
//        List<String> names4=Arrays.asList("Andrew","Brandon","Michael");
//        List namesLengths=names4.stream().map(String::length).collect(Collectors.toList());
//
//        List<String>names5= Arrays.asList("Andrew", "Brandon","Michael");
//        OptionalDouble avarageNameLengthOptional=names5.stream().mapToInt(String::length).average();
//        avarageNameLengthOptional.ifPresent(System.out::println);
//
//
//        boolean allNamesLenghtIsGtThan3=names5.stream().allMatch(n->n.length()>3);
//
//        boolean thereIsANameWhichLenghtIsGtThan3=names5.stream().anyMatch(n->n.length()>3);
//
//
//
//        List<Children> people=Arrays.asList(new Children("John","Smith",12),new Children("Sarah","Connor",10));
//
//        people.stream().sorted((p1,p2)->p1.getLastName().compareTo(p2.getLastName()))
//                       .forEach(p-> System.out.println(p.getLastName()));


        List<String> names = Arrays.asList("John", "Sarah", "Mark", "Tyla", "Elisha", "Eamonn");
        List<Integer> integers = Arrays.asList(1, 4, 2346, 123, 76, 11, 0, 0, 62, 23, 50);
        names.stream().sorted().forEach(System.out::println);
        integers.stream().sorted().forEach(System.out::println);
        names.stream().filter(n -> n.startsWith("E")).collect(Collectors.toList()).forEach(System.out::println);
        integers.stream().filter((n->n>30&&n<200)).forEach(System.out::println);
        names.stream().forEach(n->System.out.println(n.toUpperCase()));


    }
}
