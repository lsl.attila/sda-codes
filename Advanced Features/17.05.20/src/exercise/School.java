package exercise;

import java.util.*;

public class School implements Institution{
    private List<Student> studentList;
    private List<Employee> employeeList;

//    public School(List<Person> studentsList, List<Person> employeesList){
//        this.studentsList=studentsList;
//        this.employeesList=employeesList;
//    }


    public List<Student> getStudentsList() {
        return studentList;
    }

    public void setStudentsList(List<Student> studentsList) {
        this.studentList = studentsList;
    }

    public List<Employee> getEmployeesList() {
        return employeeList;
    }

    public void setEmployeesList(List<Employee> employeesList) {
        this.employeeList = employeesList;
    }

    public Map<String, Person> getMap() {
        Map<String, Person> map = new HashMap<String, Person>();
        for(Employee employee: employeeList){
            map.put(employee.getUid(),employee);
        }
        for (Student student: studentList){
            map.put(student.getUid(),student);
        }

        return map;
    }
}
