package exercise;

import java.util.List;

public class City<T> {

    private String name;
    private int noOfPopulation;
    private List<T>list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNoOfPopulation() {
        return noOfPopulation;
    }

    public void setNoOfPopulation(int noOfPopulation) {
        this.noOfPopulation = noOfPopulation;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
