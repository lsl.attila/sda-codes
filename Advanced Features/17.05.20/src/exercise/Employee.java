package exercise;

public class Employee extends Person implements Scheduler {

    private int salary;
//    public Employee(String firstName, String lastName, int age, String uid,int salary) {
//        super(firstName, lastName, age, uid);
//        this.salary=salary;
//    }


    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public int getNoOfHours() {
        return 8;
    }

    @Override
    public String getTypeMsg() {
        return "This object is an employee";
    }
}
