import java.text.DecimalFormat;

public class Department {


//            1. Create base class Departament (name, noOfEmployees , activity method which print the activity)
//            2. Create class HrDepartamnet which extend Departament class (nextEvent, override activity method)
//            3. Create class SoftwareDepartamnet which extend Departament class (languageCode activity method)
//            4. Create enum Size (SMALL, MEDIUM, BIG)
//            5.Create Company class (name, size, array of departaments)
//            6.In MAIN method show company name, size, all name departments and the department activity



    private String name;
    private int noOfEmployees;
    private String activity;

    public Department(String name,int noOfEmployees){
        this.name=name;
        this.noOfEmployees=noOfEmployees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNoOfEmployees() {
        return noOfEmployees;
    }

    public void setNoOfEmployees(int noOfEmployees) {
        this.noOfEmployees = noOfEmployees;
    }

    public String getActivity(){
        return activity;
    }
}
