public class GenericBox<T>{

    private T name;

    public GenericBox(T s){
        this.name=s;
    }

    public T getName() {
        return name;
    }

    public void setName(T name) {
        this.name = name;
    }

//    public void doSomething{
//        item().show;
//    }
}
