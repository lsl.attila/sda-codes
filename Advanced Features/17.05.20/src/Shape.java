public abstract class Shape {
    private String nume;
    private String culoare;

    public Shape(String nume,String culoare){
        this.nume=nume;
        this.culoare=culoare;
    }

    public String getNume(){
        return nume;
    }

    public String getCuloare(){
        return culoare;
    }

    public abstract double aria();


}
