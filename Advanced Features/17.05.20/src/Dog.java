public class Dog {

//    1.
//    Create class Dog.
//    a)
//    Add private fields to the class , like name , age , gender , race, weigth
//    b)
//    Create constructor that accepts all of the class fields
//    c)
//    Create additional constructor , that will accept only gender and race. It should call main
//    constructor with default values
//    d)
//    Create getters and setters for age and weigth
//    e)
//    Create object of class Dog. Verify if everything works as expected
//    f)
//    Add verification for all arguments passed to the setters . E.g . setWeigth method should
//    not accept values below or equal to 0.

    private String name;
    private int age;
    private String gender;
    private String race;
    private int weight;

    public Dog(String name,int age, String gender, String race, int weight) throws IllegalArgumentException{
        if(name.equalsIgnoreCase("")){
            throw new IllegalArgumentException("Please enter your dog's name!");
        }
        this.name = name;
        this.age = age;
        if(gender.equalsIgnoreCase("")){
            throw new IllegalArgumentException("Please enter your dog's gender!");
        }
        this.gender = gender;
        if(race.equalsIgnoreCase("")){
            throw new IllegalArgumentException("Please enter your dog's race!");
        }
        this.race = race;
        this.weight = weight;
    }

    public Dog(String gender, String race){
        this.gender=gender;
        this.race=race;
    }

    public int getAge(){
        return age;
    }

    public int getWeight(){
        return weight;
    }

    public void setAge(int age) throws IllegalArgumentException{
        if(age<0||age==0){
            throw new IllegalArgumentException("Your dog wasn't born yet, you don't have a dog!");
        }
        this.age=age;
    }

    public void setWeight(int weight)throws IllegalArgumentException{
        if(weight<0||weight==0){
            throw new IllegalArgumentException("Your dog is too light, please enter a valid weight");
        }
        this.weight=weight;
    }

    public void myDog(){
        System.out.println("Dog name: "+name+"\n"+"Dog age: "+getAge()+"\n"+"Dog gender: "+gender+"\n"+"Dog race: "+race+"\n"+"Dog weight: "+getWeight()+"\n");
    }

}
