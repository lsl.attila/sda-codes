package home.practice.inheritance;

public class Cat extends Animal {

    //  move common fields and methods to the Class Animal


    public Cat(String name, int age, String color) {
        super(name, age, color);
    }


    //  create method "act" ("yieldVoice")
    @Override
    public void act() {
        System.out.println("meow");
    }

}
