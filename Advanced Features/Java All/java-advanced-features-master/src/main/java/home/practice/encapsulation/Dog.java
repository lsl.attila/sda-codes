package home.practice.encapsulation;

//  encapsulation - exercise

public class Dog {

    //  add private fields
    private String name;
    private int age;
    private String gender;
    private String race;
    private int weight;


       //  create constructor that accepts all of the class fields
    public Dog(String name, int age, String gender, String race, int weight) {
        this.name = name;
        if (age > 0) {
            this.age = age;
        } else {
            this.age = 1;
        }
        this.gender = gender;
        this.race = race;
        this.weight = weight > 0 ? weight : 1;

    }

    //  create additional constructor that will accept only gender and race
    //  It should call main constructor with default values
    public Dog(String gender, String race) {
        this("default name", 1, gender, race, 2);
    }

    //  create getters and setters for age and weight
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age > 0)
            this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        if (weight > 0)
            this.weight = weight;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", race='" + race + '\'' +
                ", weight=" + weight +
                '}';
    }

    public static void main(String[] args) {

        //  create object of class Dog. Verify if everything works as expected
        //  add verification for all arguments passed to the setters
        //  e.g. setWeight method should not accept values below or equal to 0
        Dog dog = new Dog("Mike", -5, "Male", "Husky", -20);
        Dog dog2 = new Dog("male", "race");

        System.out.println(dog.toString() + dog2.toString());
        //  previous one uses the overridden toString method, the following one uses "String.valueOf" method
        System.out.println(String.valueOf(dog.getAge()));

    }

}
