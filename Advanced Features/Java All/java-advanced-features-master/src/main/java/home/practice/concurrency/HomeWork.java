package home.practice.concurrency;

public class HomeWork {

//    1. Create a class implementing the Runnable interface (Implementing the run method):
//      a) Inside the run method display "Hello!"
//      b) Create a class object.
//      c) Start the thread receiving the created object parameter
//         (new Thread (<object>).start())
//      d) Create several objects, run a separate thread for each of them.
//      e) Add the constructor to the class, that accepts the int value.
//      f) For the displayed data inside the run method, add the received value (Hello + value).
//      g) Add a method to the class that will modify the int value.
//      h) Add a while loop to the run method, inside which it will print the modified int
//          value every few seconds.
//      i) Add the ability to disable (gracefully shutdown) the thread. Why shouldn't we
//          just kill the thread?
//
//    2. *You are the manager. You have 5 employees. Simulate the situation in which each
//      of them comes at a different time to work.
//      a) Every employee, after getting to work, displays the information
//          "<name: I came to work at <time HH:MM>."
//      b) Every 10 seconds, the employee displays "name: I'm still working!"
//      c) Every 30 seconds, we release one of the employees to home (remember about
//          stopping the thread!) and remove the employee from the "active employees list"
//      d) When you release your employee to home, print
//          "<name: <time HH:MM>, it's time to go home!"
//      e) *When you release a given employee, all of the others speed up. From that moment,
//          display the information about work 2 seconds faster.
//      f) **The manager decides in which order they release employees
//          (e.g. through an earlier defined list)

}
