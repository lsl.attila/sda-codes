package home.practice.composition;

//  COMPOSITION and AGGREGATION means usage of objects as other objects' fields
//  A Company is an aggregation of People. A Company is a composition of Accounts.
//  When a Company ceases to do business its Accounts cease to exist but its People continue to exist.

public class Monitor {

    private int displaySize;

}
