package home.practice.abstractclass;

//  abstract class cannot be initialized

public abstract class Vehicle {

    private int maxSpeed;

    public Vehicle (int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getMaxSpeed () {
        return maxSpeed;
    }

    public abstract void move ();
}
