package home.practice.generictypes;

public abstract class Vehicle {

    public abstract void repair();
}
