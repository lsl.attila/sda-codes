package home.practice.enums;

public class MainEnums {

    public static void main(String[] args) {

        LengthUnit meterUnit = LengthUnit.METER;
        System.out.println(LengthUnit.METER);

        LengthUnit lengthUnit = LengthUnit.CENTIMETER;

        switch(lengthUnit) {

            case FOOT:
                System.out.println("Foot unit is selected");
                break;
            case METER:
                System.out.println("Meter unit is selected");
                break;
            case INCH:
                System.out.println("Inch unit is selected");
                break;
            case CENTIMETER:
                System.out.println("Centimeter unit is selected");
                break;
            default:
                System.out.println("None of them");
        }

        System.out.println(LengthUnit.FOOT.convertToMeters());

        for (LengthUnit lengthUnit1 : LengthUnit.values()) {
            System.out.println(lengthUnit1);
        }

        System.out.println(LengthUnit.CENTIMETER);


        //  verify both methods for multiple planets
        System.out.println(Planets.JUPITER);
        System.out.println(Planets.PLUTO);

        System.out.println(Planets.JUPITER.distanceFromEarth());
        System.out.println(Planets.PLUTO.distanceFromEarth());

    }
}
