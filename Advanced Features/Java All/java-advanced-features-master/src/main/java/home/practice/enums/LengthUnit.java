package home.practice.enums;

//  an ENUM is a type that has a predefined set of values.
//  Enum types implicitly extend the Enum class

public enum LengthUnit {

    METER(1, "Meter unit"),
    CENTIMETER(0.01, "Centimeter unit"),
    FOOT(0.3, "Foot unit"),
    INCH(0.025, "Inch unit");

    double value;
    String prettyName;

    LengthUnit (double value, String prettyName) {
        this.value = value;
        this.prettyName = prettyName;
    }

    double convertToMeters() {
        return value;
    }

    @Override
    public String toString() {
        return prettyName;
    }
}
