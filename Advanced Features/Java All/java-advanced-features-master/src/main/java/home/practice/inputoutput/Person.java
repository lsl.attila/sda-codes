package home.practice.inputoutput;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

//  SERIALIZABLE

public class Person implements Serializable {

    private String firstName;
    private String lastName;

    //  ... constructors and other methods
    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

//    public static void main(String[] args) {
//
//        Person person = new Person("Michael", "Dudikoff");
//        try (FileOutputStream fileOutputStream = new FileOutputStream(file);
//        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
//            objectOutputStream.writeObject(person);
//        }
//
//        Person person;
//        try (FileInputStream fileInputStream = new FileInputStream(file));
//        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStram) {
//            person = (Person) objectInputStream.readObject();
//        }
//    }
//
//    //  Java 8 introduced new classes for reading / writing files.
//    //  They are put in the java.nio package
//    Path absolutePath = Paths.get("C:/myDirectory/myFile.txt");
//    Path relativePath = Paths.get("myFile.txt");
//
//
//    //  Reading and writing files
//    List<String> fileLines = Files.readAllLines(path);
//    List<String> fileLines = Files.readAllLines(absolutePath, Charset.forName("UTF-8"));
//
//    List<String> fileLines = Arrays.asList("first line", "second line");
//    Files.write(path, fileLines);
//
//    Files.writes(absolutePath, fileLines, StandardOpenOption.APPEND);
//
//    //  Directories
//    Files.createDirectory(path);
//    Files.isDirectory(relativePath);
//
//    //  Create a file with a "lorem ipsum" paragraph within - it can be done by copy-pasting
//    //  existing paragraph or generating it dynamically using Java library. Read that file.
//    //  a) Count words.
//    //  b) *Count special signs (like comma, dot, spaces).
//    //  c) *Select one word and print it's number of occurrences.
//
//
}
