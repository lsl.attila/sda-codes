package home.practice.collections;

//  Set is an interface that extends Collection interface
//  one element cannot be present in a Set multiple times
//  If we want to get a Set instance, we need it's implementation.
//  We will use the HashSetClass
//  since elements are brought into hashset, the place of each is unknown

import java.util.*;

public class MainSet {

    public static void main(String[] args) {

        Set<String> travelRoute = new HashSet<>();
        travelRoute.add("Berlin");
        travelRoute.add("Paris");
        travelRoute.add("Madrid");
        travelRoute.add("Paris");
        travelRoute.add("Berlin");
        travelRoute.add("Andalusia");
        travelRoute.add("Amsterdam");
        travelRoute.add("Alamo");
//        travelRoute.remove("Paris");

        System.out.println("before sorting" + travelRoute.toString());

//        for (String country : travelRoute) {
//            System.out.println(country + " ");
//        }

        // sort exercise
        List<String> travelList = new ArrayList<String>(travelRoute);
        Collections.sort(travelList);
        System.out.println("after sorting" + travelList.toString());

    }
}

//  Create a set consisting of colors - given from the user

//  Present the removal of individual elements from the set

//  Display the collection before and after sorting