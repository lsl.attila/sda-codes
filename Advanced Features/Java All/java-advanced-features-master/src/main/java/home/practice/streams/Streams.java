package home.practice.streams;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//  Streams can be used to perform operations on collections with the
//  usage of lambda expressions.
//  Let's assume we have a list of people. We would like to get the
//  average age of people named "Thomas". STREAMS make such
//  operations very easy to implement

public class Streams {

    public static void main(String[] args) {

        //  collect, findFirst, findAny

        List<String> names = Arrays.asList("Andrew", "Brandon", "Michael");
        Stream<String> namesString = names.stream();

        List<String> names2 = Arrays.asList("Andrew", "Brandon", "Michael");
        List<String> namesCopy = names2.stream()
                .collect(Collectors.toList());

        Optional<String> firstName = names.stream()
                .findFirst();

        Optional<String> firstName2 = names2.stream()
                .findAny();


        //  filter, map

        List<String> names3 = Arrays.asList("Andrew", "Brandon", "Michael");
        List<String> namesStartingWithA = names3.stream()
                .filter(n -> n.startsWith("A"))
                .collect(Collectors.toList());

        List<String> names4 = Arrays.asList("Andrew", "Brandon", "Michael");
        List namesLengths = names4.stream()
                .map(String::length)
                .collect(Collectors.toList());

        List<String> names5 = Arrays.asList("Andrew", "Brandon", "Michael");
        OptionalDouble averageNameLengthOptional = names5.stream()
                .mapToInt(String::length)
                .average();
        averageNameLengthOptional.ifPresent(System.out::println);


        //  allMatch and anyMatch

        boolean allNamesLengthIsGtThan3 = names.stream()
                .allMatch(n -> n.length() > 3);

        boolean thereIsANameWhichLengthIsGtThan3 = names.stream()
                .anyMatch(n -> n.length() > 3);


        //  reduce method

        //  Initial value is empty string. If current value is an empty string we simply add
        //  element to an empty string, otherwise we concatenate current value, ", " and element.
        String namesConcatenation = names.stream()
                .reduce("",
                        (currValue, element) -> (currValue.equals("") ? "" : currValue + ", " + element));


        //  forEach, sorted

        //  The same as names.forEach(System.out::println);
        names.stream()
                .forEach(System.out::println);


        List<Person> people = Arrays.asList(
                new Person("John", "Smith", 20),
                new Person("Sarah", "Connor", 30)
        );
        people.stream()
                .sorted((p1, p2) -> p1.getLastName().compareTo(p2.getLastName()))
                .forEach(p -> System.out.println(p.getLastName()));
        //  alternative way:
        people.stream().sorted(Comparator.comparing(Person::getLastName));


        //  EXERCISES

        //  1. Using streams, for a given list:
        //      - ["John", "Sarah", "Mark", "Tyla", "Ellisha", "Eamonn"]
        //      - [1, 4, 2346, 123, 11, 0, 0, 62, 23, 50]
        //      a) Sort the list.
        //      b) Print only those names that start with "E".
        //      c) Print values greater than 30 and lower than 200.
        //      d) Print names uppercase.
        //      e) Remove first and last letter, sort and print names.
        //      f) *Sort backwards by implementing reverse Comparator and
        //         using lambda expression

    }

}
