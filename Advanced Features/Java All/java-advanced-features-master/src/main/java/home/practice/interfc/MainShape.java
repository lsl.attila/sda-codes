package home.practice.interfc;

public class MainShape {

    public static void main(String[] args) {

        Rectangle myRectangle = new Rectangle(4, 5);
        System.out.println(myRectangle.getArea());

        Shape myShape = myRectangle;
        //  calling Rectangle#getPerimeter implementation
        System.out.println(myShape.getPerimeter());

        myRectangle.print();
    }
}
