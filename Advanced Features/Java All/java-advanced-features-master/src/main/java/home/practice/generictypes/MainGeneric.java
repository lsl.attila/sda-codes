package home.practice.generictypes;

public class MainGeneric {

    public static void main(String[] args) {

        Car car = new Car();
        Garage<Car> garage = new Garage(car);
        garage.repairVehicle();

        Auto auto1 = new Auto(300);
        Auto auto2 = new Auto(200);

        if (auto1.compareTo(auto2) > 0) {
            System.out.println("auto1 is faster!");
        } else if (auto1.compareTo(auto2) == 0) {
            System.out.println("cars have the same speed");
        } else
            System.out.println("auto2 is faster!");


        Person p1 = new Person();
        p1.setHeight(181);
        Person p2 = new Person();
        p2.setHeight(181);

        if (p1.compareTo(p2) > 0) {
            System.out.println("person1 is taller");
        } else if (p1.compareTo(p2) == 0) {
            System.out.println("people have the same height");
        } else
            System.out.println("p2 is taller");


//        GenericBox<String> genericString = new GenericBox("thing");
//        GenericBox<Integer> genericInteger = new GenericBox(12);
//        GenericBox<Double> genericDouble = new GenericBox(12.0);

    }

}
