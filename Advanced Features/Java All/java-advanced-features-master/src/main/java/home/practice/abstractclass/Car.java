package home.practice.abstractclass;

public class Car extends Vehicle {

    public Car (int maxSpeed) {
        super (maxSpeed);
    }

    @Override
    public void move() {
        System.out.println("Car moved " + this.getMaxSpeed());
    }


}
