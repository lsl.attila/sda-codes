package home.practice.collections;

import java.util.*;

//  A List is an interface that extends Collection interface

public class MainList {

    public static void main(String[] args) {

        List<String> visitedCountries = new ArrayList<>();
        visitedCountries.add("Germany");
        visitedCountries.add("France");
        visitedCountries.add("Spain");
        visitedCountries.remove("France");
//        List<String> newList = new ArrayList<>(visitedCountries);
        System.out.println(visitedCountries.toString());
        for (String country : visitedCountries) {
            System.out.print(country + " ");
        }

        //  List using iterator
        Iterator<String> iterator = visitedCountries.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next() + " ");
        }
        if (iterator.hasNext())
            System.out.println(iterator.next() + " ");

        //  1. Create a List and display its result (data should be provided by the user - console):
        //  a) Purchases to be made. *If an element already exists on the list, then it should
        //  not be added
        //  b) *Add to the example above the possibility of "deleting" purchased elements
        //  c) Display only those purchases that start with "m" (e.g. milk)
        //  d) *View only purchases whose next product on the list starts with "m" (e.g. eggs,
        //  if milk was next on the list)
        List<String> groceries = new ArrayList<>();
        Scanner shoppingCart = new Scanner(System.in);
        System.out.println("1 - add \n 2 - delete \n 3 - list of products starting with \n 4 - next product starts with \n 5 - display items in basket \n 6 - exit");
        String product = shoppingCart.nextLine();
        while (!product.equals("6")) {
            switch (product) {
                case "1":
                    System.out.println("input product to add");
                    String newProduct = shoppingCart.nextLine();
                    if (!groceries.contains(newProduct)) {
                        groceries.add(newProduct);
                    } else {
                        System.out.println("item is already in the basket");
                    }
                    break;
                case "2":
                    System.out.println("input product to delete");
                    String productToDelete = shoppingCart.nextLine();
                    groceries.remove(productToDelete);
                    break;
                case "3":
                    System.out.println("input letter");
                    char letter = shoppingCart.next().charAt(0);
                    // ezt a fort atirni iteratorra
                    for (String grocery : groceries) {
                        if (grocery.startsWith(String.valueOf(letter)))
                            System.out.println(grocery);
                    }
                    break;
                case "4":
                    System.out.println("input letter");
                    // ezt az iteratort atirni forra
                    Iterator<String> groceriesIterator = groceries.iterator();
                    letter = shoppingCart.next().charAt(0);
                    String element = "";
                    if (groceriesIterator.hasNext()) {
                        element = groceriesIterator.next();
                    }
                    while (groceriesIterator.hasNext()) {
                        String oldElement = element;
                        element = groceriesIterator.next();
                        if (element.startsWith(String.valueOf(letter))) {
                            System.out.println(oldElement);
                        }
                    }
                    break;
                case "5":
                    System.out.println(groceries.toString());
                    break;
                default:
                    System.out.println("Press 6 to exit");

            }
            System.out.println("please enter next operation");
            product = shoppingCart.nextLine();
        }

//        //  2. Ratings received. Display their average. The numbers cannot be less than 1 and
//        //  greater than 6.
        List<Integer> ratings = new ArrayList<>();
        ratings.add(2);
        ratings.add(2);
        ratings.add(3);
        ratings.add(5);
        int sumOfRatings = 0;
        for (int i = 0; i < ratings.size(); i++){
            sumOfRatings += ratings.get(i);
        }
        System.out.println("Average rating is: " + sumOfRatings / ratings.size());
//
//
//        //  3. * List of lists - multiplication table
        List<List<Integer>> multiTable = new ArrayList<List<Integer>>();
        List<Integer> one = new ArrayList<>();
        one.add(1);
        one.add(2);
        one.add(3);
        List<Integer> two = new ArrayList<>();
        two.add(2);
        two.add(4);
        two.add(6);
        List<Integer> three = new ArrayList<>();
        three.add(3);
        three.add(6);
        three.add(9);
        multiTable.add(one);
        multiTable.add(two);
        multiTable.add(three);
//        System.out.println(multiTable.toString());
        for(List<Integer> list : multiTable){
            System.out.println(list.toString());
        }
    }
}



