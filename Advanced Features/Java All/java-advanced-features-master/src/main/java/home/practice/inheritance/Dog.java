package home.practice.inheritance;

public class Dog extends Animal {

    //  move common fields and methods to the Class Animal


    public Dog(String name, int age, String color) {
        super(name, age, color);

    }

    //  create method "act" ("yieldVoice")
    public void act() {
        System.out.println("bark");
    }

}
