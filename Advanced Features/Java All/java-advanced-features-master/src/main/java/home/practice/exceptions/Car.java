package home.practice.exceptions;

public class Car {

    private int speed;

    public void increaseSpeed() throws CarCrashedException {
        speed += 70;
        if (speed > 200) {
            throw new CarCrashedException(this);
        }
    }
}
