package home.practice.collections;

import java.util.*;

//  A MAP is an object that maps KEYS to VALUES. A map cannot contain duplicate keys:
//  each key can map at most to one value

public class MainMap {

    public static void main(String[] args) {

        Map<String, String> countries = new HashMap<>();
        countries.put("Poland", "Warsaw");
        countries.put("Germany", "Berlin");
        for (Map.Entry<String, String> dictionary : countries.entrySet()) {
            String country = dictionary.getKey();
            String capital = dictionary.getValue();
            System.out.printf("%s : %s\n", country, capital);
        }

        //  a
        System.out.println("enter name");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        System.out.println("enter surname");
        String surname = scanner.next();
        Map<String, String> names = new HashMap<>();
        names.put(name, surname);
        System.out.println("enter name");
        String name2 = scanner.next();
        System.out.println("enter surname");
        String surname2 = scanner.next();
        names.put(name2, surname2);

        names.put("Joe", "Spencer");
        names.put("Jen", "McSpencer");
        for (Map.Entry<String, String> person : names.entrySet()) {
            System.out.println(person.getKey() + " " + person.getValue());
        }

        //  b
        Map<String, Integer> nameToAgeMap = new HashMap<>();
        nameToAgeMap.put("Joe", 20);
        nameToAgeMap.put("Jen", 21);
        for (Map.Entry<String, Integer> pWithAge : nameToAgeMap.entrySet()) {
            System.out.println(pWithAge.getKey() + " " + pWithAge.getValue());
        }

        // c
        Map<String, List<String>> nameAndFriends = new HashMap<>();
        List<String> friends = new ArrayList<>();
        List<String> friends2 = new ArrayList<>();
        friends.add("Jennifer");
        friends.add("Aniston");
        nameAndFriends.put("Joe", friends);
        friends2.add("Matthew");
        nameAndFriends.put("Jen", friends2);
        for (Map.Entry<String, List<String>> pAndFriends : nameAndFriends.entrySet()) {
            System.out.println(pAndFriends.getKey() + " " + pAndFriends.getValue());
        }

        Map<String, Map<Integer, String>> details = new HashMap<>();
        Map<Integer, String> idToPlaceMap = new HashMap<>();
        idToPlaceMap.put(1, "tree");
        idToPlaceMap.put(2, "fruit");
        details.put("Mike", idToPlaceMap);
        for (Map.Entry<String, Map<Integer, String>> entry : details.entrySet()) {
            System.out.println(entry.getKey());
            for (Map.Entry<Integer, String> insideEntry : entry.getValue().entrySet()) {
                System.out.println("  " + insideEntry.getKey() + insideEntry.getValue());
            }
        }
    }
}

// Create a map and display its result (data should be provided by the user - console):
//  a) Names and Surnames
//  b) Names and ages
//  c) Names and lists of friends (other names)
//  d) *Names and details (map of maps), e.g.
//          "Mike:"
//              "ID": "...",
//              "birthPlace": "..."
//              ...