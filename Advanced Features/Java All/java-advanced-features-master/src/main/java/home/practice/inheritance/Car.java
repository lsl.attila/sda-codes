package home.practice.inheritance;

public class Car extends Vehicle {

    private boolean convertible;


    public Car(int maxSpeed, boolean convertible) {
        super(maxSpeed);
        this.convertible = convertible;
    }

    public boolean isConvertible() {
        return convertible;
    }


    //  calling parent's implementation
    @Override
    public String toString () {
        return super.toString()
                + ", convertible = " + convertible;
    }


    // hiding parent's field
    private double maxSpeed;

    public int getParentsHiddenField () {
        return super.maxSpeed;
    }
}
