package home.practice.inheritance;

public class Circle extends Shape {

    //  which fields and methods are common?
    int radius;


    public Circle(String color, int radius) {
        super(color);
        this.radius = radius;
    }

    public double calculateArea () {
        return Math.PI * (radius * radius);
    }

    public double calculateCirc () {
        return 2 * Math.PI * radius;
    }

}
