package home.practice.concurrency;


//  CONCURRENCY is the ability to perform multiple operations simultaneously
//  if we run an application with the MAIN method, a THREAD is created
//  We will refer to this thread as the main thread

public class Concurrency {

    public static void main(String[] args) throws InterruptedException {

        StopWatchThread stopWatchThread1 = new StopWatchThread("SW1");
        StopWatchThread stopWatchThread2 = new StopWatchThread("SW2");
        Thread sWThread = new Thread(new SWThread());
        Bench bench = new Bench(1); //  creating bench with one free seat
        SeatTakerThread seatTaker1 = new SeatTakerThread(bench);
        SeatTakerThread seatTaker2 = new SeatTakerThread(bench);

        stopWatchThread1.start();
        stopWatchThread2.start();
        sWThread.start();
        seatTaker1.start();
        seatTaker2.start();

        //  Concurrency - sleep method
        System.out.println("Main thread starts");
        Thread.sleep(5000);
        System.out.println("Main thread is still running");
        Thread.sleep(5000);
        System.out.println("Main thread ends");


    }

    //  WHAT IS THIS?
    public void methodWithSyncedCodeBlock() {
        System.out.println("Unsynced part");
        synchronized (this) {
            System.out.println("Synced part");
        }
    }
}
