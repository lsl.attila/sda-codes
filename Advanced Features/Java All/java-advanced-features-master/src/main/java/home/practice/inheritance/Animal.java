package home.practice.inheritance;

// ABSTRACT
// Change Animal class to be abstract. Which methods should/may be abstract?
public abstract class Animal {

    String name;
    int age;
    String color;


    public Animal(String name, int age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    //  create method "act" ("yieldVoice")
    //  when turning regular method into an abstract one, its body {} must be erased.
    public abstract void act();


    public static void main(String[] args) {

        //  create simple array of Animal, that will contain one object of type Dog and one object of type Cat
        Animal[] animals = {new Dog("Joe", 6, "brown"), new Cat("Jill", 5, "purple")};

        //  using for-each loop show which animal gives what kind of voice.
        //  ( read as: for each animal in animals)
        for (Animal animal : animals) {
            // How to print a name of an object?
            if (animal instanceof Dog) {
                System.out.print("dog says: ");
            } else {
                System.out.print("Cat say: ");
            }
            animal.act();
        }

    }
}
