package home.practice.composition;

public class Muzzle {

    // Create a muzzle object
    int size;


    public Muzzle(int size) {
        this.size = size;
    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Muzzle{" +
                "size=" + size +
                '}';
    }

}
