package home.practice.generictypes;

public interface Comparable<T> {

    public int compareTo(T o);


    //  There is a convention that the parameterized name is a single uppercase letter.
    //  Also there is a convention specifying which letter should be used:
    //  E, T - element type
    //  K - key type
    //  V - value type
    //  T - type
    //  N - number type
    //  S, U, V if there are more parameterized types
}
