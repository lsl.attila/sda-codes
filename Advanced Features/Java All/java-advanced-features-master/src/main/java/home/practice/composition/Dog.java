package home.practice.composition;

public class Dog {

    private String name;
    private Muzzle muzzle;

    public Dog(String name, Muzzle muzzle) {
        this.name = name;
        this.muzzle = muzzle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Muzzle getMuzzle() {
        return muzzle;
    }

    public void setMuzzle(Muzzle muzzle) {
        this.muzzle = muzzle;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", muzzle=" + muzzle +
                '}';
    }

    public static void main(String[] args) {

        //  Assign created object to a Dog object
        Dog dog = new Dog("James", null);
        System.out.println(dog);
        Muzzle muzzle = new Muzzle(1);
        dog.setMuzzle(muzzle);
        //  How to use a Dog object to show all attributes of a Muzzle object?
        System.out.println(dog.getMuzzle().getSize());
        System.out.println(dog);
    }
}
