package home.practice.nestedclasses;

public class MainNest {

    public static void main(String[] args) {

        Bicycle bicycle = new Bicycle();
        System.out.println(bicycle.getMaxSped());
        Bicycle.Wheel wheel = bicycle.new Wheel();
        wheel.damage();
        System.out.println(bicycle.getMaxSped());


        Bicycle bicycle1 = new Bicycle();
        System.out.println(bicycle1.getMaxSped());
        Bicycle.Mechanic mechanic = new Bicycle.Mechanic();
        mechanic.repair(bicycle1);
        System.out.println(bicycle1.getMaxSped());

    }
}
