package home.practice.enums;

public enum Planets {

    JUPITER("Jupiter", "Huge", 30),
    PLUTO("Pluto", "Small", 100),
    EARTH("Earth", "Medium", 50);

    String name;
    String relativeSize;
    int distanceFromSun;

    Planets(String name, String relativeSize, int distanceFromSun) {
        this.name = name;
        this.relativeSize = relativeSize;
        this.distanceFromSun = distanceFromSun;
    }

    //  Override toString method. It should print relative size of a planet and it's name
    //  e.g. "Huge Jupiter", "Small Pluto"
    @Override
    public String toString() {
        return relativeSize + " " + name;
    }

    //  create distanceFromEarth method
    public int distanceFromEarth(){
        if (this.name.equals(EARTH.name)){
            return 0;
        }
        if (this.name.equals(PLUTO.name)){
            return PLUTO.distanceFromSun - EARTH.distanceFromSun;
        }
        return EARTH.distanceFromSun - JUPITER.distanceFromSun;
    }
}
