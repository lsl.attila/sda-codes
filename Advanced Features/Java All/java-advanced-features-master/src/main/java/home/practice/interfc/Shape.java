package home.practice.interfc;

//  is public and abstract by default
//  might include static methods and fields
//  can be extended
//  a class can implement many interfaces
public interface Shape {

    double getArea();
    double getPerimeter();

    default void print() {
        System.out.println("Shape: " + this);
    }
}
