package home.practice.inheritance;

public class Rectangle extends Shape {

    //  which fields and methods are common?
    int length;
    int width;

    public Rectangle(String color, int length, int width) {
        super(color);
        this.length = length;
        this.width = width;
    }


    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }


    public double calculateArea () {
        return length * width;
    }

    public double calculateCirc () {
        return 2 * length + 2 * width;
    }


}

