package home.practice.generictypes;


//  a generic type is a generic class or interface that is parameterized over types

public class GenericBox<T> {

    private T item;

    public GenericBox(T item) {
        this.item = item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }


}
