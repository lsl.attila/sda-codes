package home.practice.inheritance;

public class Vehicle {

    // hiding parent's field
    protected int maxSpeed;

    public Vehicle (int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }


    public int getMaxSpeed () {
        return maxSpeed;
    }

    //  overriding methods
    @Override
    public String toString () {
        return "Fields values: maxSpeed = " + maxSpeed;
    }



}
