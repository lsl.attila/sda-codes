package home.practice.encapsulation;

//  encapsulation - wraps fields and methods that work together as a single unit

public class Person {

    private String firstName;
    private String lastName;
    private boolean male;
    private int age;


    //  getters and setters
    String getFirstName(){
        return firstName;
    }

    public boolean isMale() {
        return male;
    }

    //  we do not want to assign null or empty value to firstName field
    public void setFirstName (String firstName) {
        if(firstName != null && !"".equals((firstName)))
        this.firstName = firstName;
    }

    public void setMale (boolean male) {
        this.male = male;
    }

    // constructor
    public Person (String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getLastName(){
        return lastName;
    }

    //  we only allow increasing age value by one at a time
    public void growOlder() {
        age++;
    }

}
