package home.practice.abstractclass;

public class MainVehicle {

    public static void main(String[] args) {

        Car myCar = new Car(120);
        myCar.move();   //  prints "Car moved"

//      Vehicle myVehicle = myCar;
        Vehicle myVehicle = new Car(150);
        //  calling Car's implementation
        //  myVehicle refers to a Car object
        myVehicle.move();   //  prints "Car moved"
    }
}
