package home.practice.lambdaexpression;

import java.util.function.Predicate;

//  Lambda expression - PREDICATE (functional interface)

//  example WITHOUT Lambda
public class AdultPersonTest implements Predicate<Person> {

    @Override
    public boolean test (Person person) {
        return person.getAge() >= 18;
    }
}
