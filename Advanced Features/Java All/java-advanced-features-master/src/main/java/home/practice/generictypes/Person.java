package home.practice.generictypes;

public class Person implements Comparable<Person> {

    //  Person class should implement compareTo method,
    //  that will verify if one person is taller than another

    private int height;

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int compareTo(Person otherPerson) {
        return this.height - otherPerson.getHeight();
    }

}
