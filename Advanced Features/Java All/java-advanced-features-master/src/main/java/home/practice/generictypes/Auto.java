package home.practice.generictypes;

public class Auto extends Vehicle implements Comparable<Auto>{

    private int maxSpeed;

    public Auto(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public int compareTo(Auto otherAuto) {
        return this.maxSpeed - otherAuto.maxSpeed;
    }

    @Override
    public void repair () {
        System.out.println("Auto is repaired");
    }

}
