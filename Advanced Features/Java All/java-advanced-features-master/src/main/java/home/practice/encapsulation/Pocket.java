package home.practice.encapsulation;

public class Pocket {

    //  add field "money", create constructor, getter and setter
    private int money;

    public Pocket(int money) {
        this.money = money;
    }


    //  add verification for both getter and setter. Getter should result in
    //  returning as much money as the user asked for. It should return 0 if money <= 10.
    public int getMoney(int amount) {
        if (money < amount) {
            return -1;
        }
        if (money <= 10) {
            return 0;
        }
        this.money -= amount;
        return amount;
    }


    //  Setter should not accept values below 0 and greater than 3000. It may print a
    //  message like "I don't have enough space in my pocket for as much money!"
    public void setMoney(int amount) {
        if (this.money + amount > 3000) {
            System.out.println("I don't have enough space in my pocket for as much money!");
//            return;
        } else if (amount > 0) {
            this.money += amount;
        }
    }

    @Override
    public String toString() {
        return "Pocket{" +
                "money=" + money +
                '}';
    }

    public static void main(String[] args) {
        Pocket pocket = new Pocket(15);
        System.out.println(pocket.toString());
        pocket.setMoney(5);
        System.out.println(pocket.toString());
        System.out.println(pocket.toString() + pocket.getMoney(18));
        System.out.println(pocket.toString() + pocket.getMoney(5));
        pocket.setMoney(2999);
        System.out.println(pocket.toString());
        System.out.println(pocket.toString() + pocket.getMoney(1));

    }
}
