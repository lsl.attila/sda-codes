package home.practice.exceptions;

import java.util.List;
import java.util.Scanner;

public class ExceptionsHomework {

    //  write an application that will read the input and print back value that
    //  user provided, use try-catch statements to parse the input, e.g. I/O:
    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter value");

        String userInput = myObj.nextLine();  // Read user input
//  INPUT: 10
//  OUTPUT: int -> 10
//  INPUT: 10.0
//  Output: double -> 10.0
//  INPUT: "Hello!"
//  OUTPUT: "Hey! That's not a value! Try once more."
        try {
            int inputAsInt = Integer.parseInt(userInput);
            System.out.println("OUTPUT: int -> " + inputAsInt);
        } catch (NumberFormatException e) {
            try {
                double inputAsDouble = Double.parseDouble(userInput);
                System.out.println("OUTPUT: double -> " + inputAsDouble);
            } catch (NumberFormatException exception) {
                System.out.println("Hey! That's not a value! Try once more.");
            }

        }

    }

//  What do you think about raising an exception: when should we raise
//  an exception and when return Null on method "failure"? Try to
//  implement one method for each situation.
    public List<String> testMethod(boolean isException) throws Exception {
        if (isException) {
            throw new Exception();
        }
        return null;
    }
}