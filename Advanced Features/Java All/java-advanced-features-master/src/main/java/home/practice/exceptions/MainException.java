package home.practice.exceptions;

import java.util.Scanner;

//  base for all EXCEPTIONS is the THROWABLE Class
//  if a program's execution is disrupted an exception is thrown
//  exceptions can be caught and handled
//  exceptions are represented as objects
public class MainException {

    public static void main(String[] args) throws Exception {

        //  try-catch
        try {
            int x = 5 / 0;
        } catch(Exception e) {
            System.out.println("Exception is caught and handled!");
        }

        // try-catch-finally
        int x;
        int y = 4;
        
        try {
            x = y / 0;
        } catch (Exception e) {
            System.out.println("Exception is caught and handled!");
        } finally {
            System.out.println("This will be printed no matter what the value of y is");
        }
        
        //  multiple catch
        int[] intArray = {1,2};

        try {
            //  arithmetic exception
//            x = 0 / 0;
            //  ArrayIndexOutOfBoundsException
            x = intArray[10] / y;
        } catch(ArithmeticException e) {
            System.out.println("ArithmeticException caught!");
        } catch(ArrayIndexOutOfBoundsException e) {
            System.out.println("ArrayIndex(...)Exception caught!");
        } catch(Exception e) {
            System.out.println("Another exception caught!");
        }


        //  catch with multiple parameter types
        try {
            x = intArray[10] / y;
        } catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {
            System.out.println("ArithmeticException or a " +
                    "ArrayIndexOutOfBoundsException caught!");
        }

        //  autoclosing
        try(Scanner scanner = new Scanner(System.in)){
            //  code
        }

        //  throwing an exception
//      throw new AnExceptionType(arguments);
//      throw new Exception("Some program failure");

        //  "throws" in method signature
        int[] someIntArray = {3, 4, 2};
        //  invalid code - printArrayElement throws Exception, so it needs to be
        //  handled (putting it into try-catch or adding throws Exception to
        //  main method declaration will fix it)

        printArrayElement(new int[]{},0);
        printArrayElement(new int[]{1},0);
    }

    private static void printArrayElement(int[] intArray, int index) throws Exception {
        if (index < 0 || index >= intArray.length) {
            throw new Exception("Incorrect argument!");
        }
        System.out.println(intArray[index]);
    }
    // trace of exception: lines that show the location of exception in code. Lines start with at...
}
