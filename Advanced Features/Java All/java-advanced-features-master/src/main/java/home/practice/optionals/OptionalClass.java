package home.practice.optionals;

import java.util.Optional;

//  Optional class is a wrapper that can be used when we are not
//  sure if a value is present. It contains a useful set of methods.

public class OptionalClass {

    private static String stringVariable = "my value";


    public static void main(String[] args) {

        //  of method does not allow null value
        Optional<String> stringOptional1 = Optional.of(stringVariable);
        //  ofNullable method does not allow null value
        Optional<String> stringOptional2 = Optional.ofNullable(stringVariable);
        //  returns true if value is present
        stringOptional1.isPresent();

        if(stringOptional2.isPresent()) {
            String value = stringOptional2.get();
        }

        stringOptional1.ifPresent(System.out::println);

        String value = stringOptional1.orElse("default value");
    }
}
