package home.practice.inheritance;

// ABSTRACT
// Change Shape class to be abstract. Which methods should/may be abstract?
public abstract class Shape {

    //  add fields, create constructor, getters and setters

    String color;

    public Shape(String color) {
        this.color = color;

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    //  this is a declared method. If it has a body its called "an implemented method"
    //  Add calculateCirc (getPerimeter) and (calculateArea) getArea methods declaration to the Shape abstract class.
    //  Implement and verify those methods for both Circle and Rectangle classes
    public abstract double calculateArea();

    public abstract double calculateCirc();


}
