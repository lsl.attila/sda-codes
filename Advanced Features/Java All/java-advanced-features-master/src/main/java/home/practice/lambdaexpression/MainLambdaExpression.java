package home.practice.lambdaexpression;

import java.util.Random;
import java.util.function.*;

//  A lambda expression is a representation of an implementation of
//  an interface which consists of only one method
//  (parameter names) -> expression to evaluate

public class MainLambdaExpression {

    public static void main(String[] args) {

        //  example WITHOUT Lambda
        Person andyMurray = new Person("Andy", "Murray", 30);
        Predicate<Person> adultPersonTest = new AdultPersonTest();
        System.out.println(adultPersonTest.test(andyMurray));

        //  example WITH Lambda
        Person jackMurray = new Person("Jack", "Murray", 33);
        Predicate<Person> adultPersonTest2 = p -> p.getAge() > 30;
        System.out.println(adultPersonTest2.test(jackMurray));


        Runnable myRunnable = () -> System.out.println("Running a runnable");
        myRunnable.run();
        Predicate<String> startsWithABCTest = s -> s.startsWith("ABC");
        System.out.println(startsWithABCTest.test("ABCDEF"));


        //  FUNCTION INTERFACE

        //  Method apply returns Integer and the type of the parameter is String
        Function<String, Integer> stringLengthFunction = s -> s.length();
        System.out.println(stringLengthFunction.apply("ABCDE"));

        Function<String, String> replaceCommaWithDotsFunction = s -> s.replace(',', '.');
        System.out.println(replaceCommaWithDotsFunction.apply("a,b,c"));


        //  METHOD REFERENCE

        Person johnSmith = new Person("John", "Smith", 19);
        Predicate<Person> adultPersonTest3 = Person::isAdult;
        System.out.println(adultPersonTest3.test(johnSmith));


        //  SUPPLIER, CONSUMER

        Supplier<Integer> randomNumberSupplier = () -> new Random().nextInt();
        int randomNumber = randomNumberSupplier.get();

        Consumer<Double> printWithPrefixConsumer = d -> System.out.println("Value: " + d);
        printWithPrefixConsumer.accept(10.5);


        //  OPERATOR

        UnaryOperator<Integer> toSquareUnaryOperator = i -> i * i;
        System.out.println(toSquareUnaryOperator.apply(5));


        //  BLOCK OF CODE

        UnaryOperator<Integer> toSquareUnaryOperator2 = i -> {
            int result = i * i;
            System.out.println("Result: " + result);
            return result;
        };
        toSquareUnaryOperator2.apply(4);
    }


}
