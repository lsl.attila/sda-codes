package home.practice.annotations;

//  Annotations inform the compiler about the code structure
//  Tools can process annotations to generate code, documentation
//  Annotations can be processed during runtime,
//  e.g. classes can be found by their annotations

import java.util.List;

public class Annotations {


//    @Override
//    public String toString(){
//        ...
//    }
        //  declared in either parent class or implemented interface
        //  helps to avoid mistakes when overriding methods

    @SuppressWarnings("unchecked")
    public static void addToList (List list, String string) {
        list.add(string);
    }
        //  In the example annotation will make the compiler assume that
        //  the code is correct (it will not result in showing warnings).
}
