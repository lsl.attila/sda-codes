package home.practice.generictypes;

public class Generic {

    public static void main(String[] args) {

        // CHECK ABOUT ACTUAL GENERIC TYPE WAY TO SOLVE THIS
        //  Create a simple Generic class, that will give a possibility to store any kind of
        //  value within. Add object of type String, Integer and Double to array of that
        //  Generic type. Print all values of the array within.

        Object[] myArray = {"i", 10, 10.0};
        for (Object o : myArray)
            System.out.println(o.toString());
    }
}
