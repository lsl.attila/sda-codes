package home.practice.inheritance;

public class MainInheritance {

    public static void main(String[] args) {

        //  assigning to parent type variable
        Car myCar0 = new Car(130, false);
        Vehicle myCarVehicle0 = new Car(210, true);
        //  Car myCar2 = new Vehicle(); -- you can't do such thing


        Vehicle vehicle = new Vehicle(85);
        System.out.println(vehicle);
        //  System.out.println(); prints object's toString value


        //  polymorphism
        Vehicle vehicle1 = new Vehicle(85);
        Vehicle myCarVehicle = new Car(210, true);
        System.out.println("New vehicle: " + vehicle1.toString());
        System.out.println("New car: " + myCarVehicle.toString());


        //  passing parameters
        Car myCar = new Car(120, true);
        //  passing a Car as a parameter - it is a Vehicle as well
        printMaxSpeed(myCar);
        //  again - this time passing a Car as an Object
        //  (a Car is also an Object)
        printWithPrefix("My car: ", myCar);



        //  Exercises

        Shape shape1 = new Circle("red", 3);
        Shape shape2 = new Rectangle("green", 10, 5);
        System.out.println("Circumference of circle : " + shape1.calculateCirc() + " Area of circle: " + shape1.calculateArea());
        System.out.println("Circumference of rectangle : " + shape2.calculateCirc() + " Area of rectangle: " + shape2.calculateArea());


    }

    //  passing parameters
    public static void printMaxSpeed(Vehicle vehicle) {
        System.out.println(vehicle.getMaxSpeed());
    }

    public static void printWithPrefix(String prefix, Object object) {
        System.out.println(prefix + object);
    }

}
