package home.practice.nestedclasses;

//  A NESTED CLASS can be defined in body of another class.
//  We will call them inner and outer classes.
//  Object of non-static inner type is bound to an object of outer type.

public class Bicycle {

    private String name = "YT Industries";
    private boolean tyre;
    private int maxSped = 40;

    public int getMaxSped() {
        return maxSped;
    }


    //  NON-STATIC nested class
    public class Wheel {

        public void damage() {
            //  we can refer to outer class's field
            //  (Wheel type object will be created for a Bicycle instance)
            maxSped *= 0.5;
        }
    }


    //  STATIC NESTED CLASSES work similarly, the difference is that an
    //  instance of inner static class in not bound to an instance of outer class
    public static class Mechanic {

        public void repair(Bicycle bicycle){
            //  nested static class can refer to private field of outer class
            bicycle.maxSped += 15;
        }
    }

}
