package home.practice.exceptions;

public class CarCrashedException extends Exception {

    //  parameter is the argument
    public CarCrashedException(Car car) {
        //  calling Exception(String message) constructor
        super("Car " + car + "has crashed!");
    }

}

