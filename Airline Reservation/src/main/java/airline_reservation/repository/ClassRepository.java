package airline_reservation.repository;

import airline_reservation.ConnectionManager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ClassRepository {
        public static void findAll() {
        Connection connection = ConnectionManager.getConnection();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from classes");

            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String type = resultSet.getString("type");
                System.out.println(id + " " + type);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
