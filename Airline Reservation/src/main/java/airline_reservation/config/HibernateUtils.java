package airline_reservation.config;

import airline_reservation.datasource.*;
import airline_reservation.datasource.Class;
import airline_reservation.Constants;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtils {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {

            Configuration configuration = new Configuration();
            Properties properties = new Properties();
            properties.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
            properties.put(Environment.URL, Constants.URL);
            properties.put(Environment.USER, Constants.USERNAME);
            properties.put(Environment.PASS, Constants.PASSWORD);
            properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
            properties.put(Environment.SHOW_SQL, true);

            configuration.addProperties(properties);
            configuration.addAnnotatedClass(Class.class);
            configuration.addAnnotatedClass(Flight.class);
            configuration.addAnnotatedClass(Plane.class);
            configuration.addAnnotatedClass(Reservation.class);
            configuration.addAnnotatedClass(User.class);
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        }
        return sessionFactory;
    }
}
