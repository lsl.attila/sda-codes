package airline_reservation;

import airline_reservation.datasource.Class;
import airline_reservation.repository.ClassRepository;
import org.w3c.dom.ls.LSOutput;

import java.sql.Connection;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        Connection connection=ConnectionManager.getConnection();
        System.out.println("Classes");
        ClassRepository.findAll();
    }
}
