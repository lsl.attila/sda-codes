package airline_reservation.datasource;

import javax.persistence.*;
import java.sql.ResultSet;
import java.util.Date;

@Entity
@Table(name= "reservation")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "id")
    private Integer id;

    @Column(name="date")
    private Date date;

    @Column(name="price")
    private Integer price;

    @ManyToOne
    @JoinColumn(name = "flight_id")
    private ResultSet reservation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
