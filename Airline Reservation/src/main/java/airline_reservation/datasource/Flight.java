package airline_reservation.datasource;

import javax.persistence.*;

@Entity
@Table(name= "flights")
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "id")
    private Integer id;

    @Column(name="from")
    private String from;

    @Column(name="to")
    private String to;

    @ManyToOne
    @JoinColumn(name = "plane_id")
    private Flight flights;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
