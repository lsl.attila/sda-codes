package hr.repository;

import hr.config.HibernateUtils;
import hr.datasource.Department;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentRepository {

//    public static void findAll() {
//        Connection connection = ConnectionManager.getConnection();
//
//        try {
//            Statement statement = connection.createStatement();
//            ResultSet resultSet = statement.executeQuery("select * from departments");
//
//            while (resultSet.next()) {
//                Integer id = resultSet.getInt("departmentId");
//                String name = resultSet.getString("name");
//                System.out.println(id + " " + name);
//            }
//            resultSet.close();
//            statement.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        try {
//            connection.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    public static void findAllPreparedStatement(String letter) {
//        Connection connection = ConnectionManager.getConnection();
//
//        try {
//            PreparedStatement statement = connection.prepareStatement("select * from departments where name like ?");
//            statement.setString(1, letter);
//            ResultSet resultSet = statement.executeQuery();
//            while (resultSet.next()) {
//                Integer id = resultSet.getInt("departmentId");
//                String name = resultSet.getString("name");
//                System.out.println(id + " " + name);
//            }
//            resultSet.close();
//            statement.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        try {
//            connection.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void findLetter(String letter) {
//        Connection connection = ConnectionManager.getConnection();
//        try {
//            Statement statement = connection.createStatement();
//            ResultSet resultLetter = statement.executeQuery("select * from departments where name like '" + letter + "%'");
//
//            while (resultLetter.next()) {
//                Integer id = resultLetter.getInt("departmentId");
//                String name = resultLetter.getString("name");
//                System.out.println(id + " " + name);
//            }
//            resultLetter.close();
//            statement.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        try {
//            connection.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    public static void updateName(String newName, Integer chosenDepartment) {
//        Connection connection = ConnectionManager.getConnection();
//        try {
//            Statement statement = connection.createStatement();
//            Integer affectedRows = statement.executeUpdate("UPDATE departments SET name='" + newName + "'WHERE departmentId=" + chosenDepartment);
//            System.out.println(affectedRows);
//            statement.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        try {
//            connection.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    public static List<Department> findAllList() {
//        List<Department> departments = new ArrayList<Department>();
//        Connection connection = ConnectionManager.getConnection();
//
//        try {
//            PreparedStatement statement = connection.prepareStatement("select * from departments");
//            ResultSet resultSet = statement.executeQuery();
//
//            while (resultSet.next()) {
//                Integer id = resultSet.getInt("departmentId");
//                String name = resultSet.getString("name");
//                Department department = new Department();
//                department.setId(id);
//                department.setName(name);
//                departments.add(department);
//            }
//            resultSet.close();
//            statement.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        try {
//            connection.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return departments;
//    }

    public static Department findByld(Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Department department = session.find(Department.class, id);
        session.close();
        return department;
    }

    public static List<Department> findAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        List<Department> departments = session.createQuery("select dep from Department dep", Department.class).getResultList();
        session.close();
        return departments;
    }

    public static void save(Department department) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        session.save(department);
        transaction.commit();
        session.close();
    }

    public static void update(Department department) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        session.saveOrUpdate(department);
        transaction.commit();
        session.close();
    }

    public static List<Department> findAllByName(String name) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Query<Department> query = session.createQuery("select dep from Department dep where dep.name = :name", Department.class);
        query.setParameter("name", name);
        List<Department> departments = query.getResultList();
        session.close();
        return departments;
    }
}

