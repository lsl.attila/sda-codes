package hr.repository;

import hr.config.HibernateUtils;
import hr.datasource.Department;
import hr.datasource.Employee;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class EmployeeRepository {

    public static Employee findById(Integer id){
        Session session= HibernateUtils.getSessionFactory().openSession();
        Employee employee=session.find(Employee.class,id);
        session.close();
        return employee;
    }

    public static List<Employee> findAll(){
        Session session=HibernateUtils.getSessionFactory().openSession();
        List<Employee> employees = session.createQuery("select emp from Employee emp",Employee.class).getResultList();
        session.close();
        return employees;
    }

    public static void save(Employee employee){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction=session.getTransaction();
        transaction.begin();
        session.save(employee);
        transaction.commit();
        session.close();
    }

    public static void update(Employee employee){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction=session.getTransaction();
        transaction.begin();
        session.saveOrUpdate(employee);
        transaction.commit();
        session.close();
    }

}
