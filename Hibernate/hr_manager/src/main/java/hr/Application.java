package hr;

import hr.datasource.Department;
import hr.datasource.Employee;
import hr.repository.DepartmentRepository;
import hr.repository.EmployeeRepository;

import java.util.List;

public class Application {
    public static void main(String[] args) {
//        Connection connection=ConnectionManager.getConnection();
//        System.out.println("Departments");
//        DepartmentRepository.findAll();
//        System.out.println("PreparedStatement");
//        DepartmentRepository.findAllPreparedStatement("HR");
//        System.out.println("Find letter");
//        DepartmentRepository.findLetter("H");
//        System.out.println("UpdateName");
//        DepartmentRepository.updateName("HumanResources",1);


//        List<Department> departments = DepartmentRepository.findAllList();
//        for (Department department : departments) {
//            System.out.println(department.getId() + " " + department.getName());
//        }
//
//        Department department = DepartmentRepository.findByld(1);
//        System.out.println(department.getName());

//       SessionFactory sessionFactory= HibernateUtils.getSessionFactory();
//        System.out.println(sessionFactory);

//        Department dep=DepartmentRepository.findByld(1);
//        System.out.println(dep.getName()+" "+dep.getId());

//        List<Department> departments= DepartmentRepository.findAll();
//        for (Department department:departments){
//            System.out.println(department.getName());
//        }

//        System.out.println("Employees findById");
//        Employee employee = EmployeeRepository.findById(1);
//        System.out.println(employee.getFirstName() + " " + employee.getLastName());
//
//        System.out.println("Employees findAll");
//        List<Employee> employees = EmployeeRepository.findAll();
//        for (Employee item : employees) {
//            System.out.println(item.getFirstName() + " " + item.getLastName());
//        }

//        List<Department> departments = DepartmentRepository.findAll();
//        for (Department department : departments) {
//            System.out.println(department.getName());
//        }
//
//        Department department = new Department();
//        department.setName("New Department1");
//
//        DepartmentRepository.save(department);
//
//        List<Department> departmentList = DepartmentRepository.findAll();
//        for (Department dep : departmentList) {
//            System.out.println(dep.getName());
//        }

//        Department dep=DepartmentRepository.findByld(1);
//        System.out.println("Before update: "+dep.getName());
//
//        dep.setName("Update with hibernate");
//        DepartmentRepository.update(dep);
//
//        Department updateDep=DepartmentRepository.findByld(1);
//        System.out.println("After update: "+dep.getName());


//                List<Employee> employees = EmployeeRepository.findAll();
//        for (Employee employee:employees) {
//            System.out.println(employee.getFirstName()+" "+employee.getLastName());
//        }
//
//        Employee employee = new Employee();
//        employee.setFirstName("John");
//        employee.setLastName("Wick");
//
//        EmployeeRepository.save(employee);
//
//        List<Employee> employeeList = EmployeeRepository.findAll();
//        for (Employee emp:employeeList) {
//            System.out.println(emp.getFirstName()+" "+emp.getLastName());
//        }
//
//       Employee emp=EmployeeRepository.findById(1);
//        System.out.println("Before update: "+emp.getFirstName()+" "+emp.getLastName());
//
//        emp.setLastName("Adam");
//        emp.setFirstName("Majlak");
//        EmployeeRepository.update(emp);
//
//        Employee updateEmp=EmployeeRepository.findById(1);
//        System.out.println("After update: "+emp.getFirstName()+" "+emp.getLastName());


//        Employee employee=EmployeeRepository.findById(1);
//        System.out.println(employee.getFirstName()+" "
//                +employee.getLastName()+" "
//                +employee.getDepartment().getName());
//
//        System.out.println("Find all");
//        List<Employee> employees=EmployeeRepository.findAll();
//        for(Employee item:employees){
//            System.out.println(item.getFirstName()+" "+item.getLastName()+" "+item.getDepartment().getName());
//        }

        List<Department> departments=DepartmentRepository.findAllByName("Finance");
                for(Department department:departments){
                    System.out.println(department.getName());
                }

    }
}
